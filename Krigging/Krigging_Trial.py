from pykrige.ok import OrdinaryKriging
from pykrige.uk import UniversalKriging
from pykrige.rk import Krige
from pykrige.compat import GridSearchCV
from scipy.interpolate import griddata
from matplotlib import pyplot as plt
from matplotlib import tri as tri
import pandas as pd
import numpy as np

#import the .csv data into a pandas DataFrame
df = pd.read_csv('/Users/andres/Documents/Documents - Andres Rocha\'s MacBook Pro/03_ETHZ/Project/Data/_stats.csv')

#we extract the Z values from each column (here we only take the Median for sake of an example)
#y = df['Median / nm']
X = df[['Temperature', 'Base:acid']]
X = X.to_numpy()

x = df['Temperature'].to_numpy()
y = df['Base:acid'].to_numpy()
z = df['Intensity / a.u.'].to_numpy()

#plotting via irregular interpolation

xi = np.linspace(90,190,100)
yi = np.linspace(0.7,1.2,100)
zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')


CS = plt.contour(xi, yi, zi, 200, linewidths=0.5)
CS = plt.contour(xi, yi, zi, 200, cmap=plt.cm.jet)
plt.colorbar()
plt.scatter(x , y, marker='o', c='k', s=9)

plt.xlim(min(x), max(x))
plt.ylim(min(y), max(y))
plt.show()
