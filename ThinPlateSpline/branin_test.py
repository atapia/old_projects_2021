"""
deMello Group

Author: Andres Rocha Tapia
Date: 26.03.2020

In this script, we use the RBF model to interpolate using thin plate spline (TPS), a data space and derive a reaction surface.
The employed data if from Perovskite research provided by Shangkun/Phil

"""

# Data import packages
import pandas as pd

# Plotting packages
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.pyplot import Axes

# Interpolation Package
from rbf.interpolate import RBFInterpolant
from TPS_own import TPS
from Krige import blup

# miscellaneous
from datetime import datetime
import numpy as np
from numpy import random


def branin(x):
    a, b, c = [1, 5.1 / (4 * (np.pi) ** 2), 5 / np.pi]
    r, s, t = [6, 10, 1 / (8 * np.pi)]
    return a * (x[:, 1] - b * (x[:, 0] ** 2) + c * x[:, 0] - r) ** 2 + s * (1 - t) * np.cos(x[:, 0]) + s


# The brannin function is usually defined in the interval
n = 100
x1 = np.linspace(-5, 10, n)
x2 = np.linspace(0, 15, n)
X_true = np.reshape(np.meshgrid(x1, x2), (2, n * n)).T
Y_true = branin(X_true)

# Define some coarse data points
random.seed(99)
m = 8
x1c = np.linspace(-4.8, 9.8, m)
x2c = np.linspace(0.2, 14.8, m)
X_coarse = np.reshape(np.meshgrid(x1c, x2c), (2, m * m)).T
Y_coarse = branin(X_coarse) + 0.1 * random.randn(len(X_coarse))

###############################################################################################
# Running Multiple Interpolators

# Krige
krige_pred, _ = blup(X_coarse, Y_coarse.reshape((-1, 1)), X_true)
print(krige_pred.shape)

# TPS (RBF)
rbf_inty = RBFInterpolant(X_coarse, Y_coarse, sigma=0.0, phi='phs2', order=1)
rbf_pred = rbf_inty(X_true)
print(rbf_pred.shape)

# TPS (Self Implemented)
tps_pred = TPS(X_coarse, Y_coarse.reshape((-1, 1)), X_true, factor=0.7)

######################################################################################################
# Rearranging arrays to fit into contour plots

xx, yy = np.meshgrid(x1, x2)

K_grid = np.zeros((n, n))
true_grid = np.zeros((n, n))
RBF_grid = np.zeros((n, n))
TPS_grid = np.zeros((n, n))

# Assigning values ot grid
k = 0
for i in np.arange(n):
    for j in np.arange(n):
        K_grid[i, j] = krige_pred[k]
        RBF_grid[i, j] = rbf_pred[k]
        TPS_grid[i, j] = tps_pred[k]
        true_grid[i, j] = Y_true[k]
        k += 1

######################################################################################################
# Plotting interpolations
count = 30
fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(12, 6))
fig.subplots_adjust(left=0.02, bottom=0.06, right=0.95, top=0.94, wspace=0.05)

# True Function

#Minimum
xmin = [-np.pi, np.pi, 9.42478]
ymin = [12.275, 2.275, 2.475]
im1 = ax1.contourf(xx, yy, true_grid, count)
ax1.scatter(xmin, ymin, s=20, color='white', marker='.')
fig.colorbar(im1, ax=ax1)
ax1.set_title('True Function')
ax1.annotate('minimum: \n f(x) = 0.397887', xy=(xmin[0], ymin[0]), xytext=(1, 5), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))
ax1.annotate('minimum: \n f(x) = 0.397887', xy=(xmin[1], ymin[1]), xytext=(1, 5), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))
ax1.annotate('minimum: \n f(x) = 0.397887', xy=(xmin[2], ymin[2]), xytext=(1, 5), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))

# Krige

xmin = np.array([-np.pi, np.pi, 9.42478]).reshape((-1,1))
ymin = np.array([12.275, 2.275, 2.475]).reshape((-1,1))
krig_fmin, _ = blup(X_coarse, Y_coarse.reshape((-1, 1)), np.hstack([xmin, ymin]))

im2 = ax2.contourf(xx, yy, K_grid, count)
ax2.scatter(xmin, ymin, s=20, color='white', marker='.')
ax2.scatter(X_coarse[:, 0], X_coarse[:, 1], color='k', s=20, marker='.')
fig.colorbar(im2, ax=ax2)
ax2.set_title('Krigging')
ax2.annotate('minimum: \n f(x) = {:.5f}'.format(krig_fmin[0]), xy=(xmin[0], ymin[0]), xytext=(1, 9), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))
ax2.annotate('minimum: \n f(x) = {:.5f}'.format(krig_fmin[1]), xy=(xmin[1], ymin[1]), xytext=(1, 5), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))
ax2.annotate('minimum: \n f(x) ={:.5f}'.format(krig_fmin[2]), xy=(xmin[2], ymin[2]), xytext=(1, 0.5), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))

# RBF
rbf_fmin = rbf_inty(np.hstack([xmin, ymin]))

im3 = ax3.contourf(xx, yy, RBF_grid, count)
ax3.scatter(xmin, ymin, s=20, color='white', marker='.')
ax3.scatter(X_coarse[:, 0], X_coarse[:, 1], color='k', s=20, marker='.')
fig.colorbar(im3, ax=ax3)
ax3.set_title('TPS')
ax3.annotate('minimum: \n f(x) = {:.5f}'.format(rbf_fmin[0]), xy=(xmin[0], ymin[0]), xytext=(1, 9), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))
ax3.annotate('minimum: \n f(x) = {:.5f}'.format(rbf_fmin[1]), xy=(xmin[1], ymin[1]), xytext=(1, 5), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))
ax3.annotate('minimum: \n f(x) ={:.5f}'.format(rbf_fmin[2]), xy=(xmin[2], ymin[2]), xytext=(1, 0.5), color='white',
             arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='white'))

fig.tight_layout()

######################################################################################################
# Testing performance

def mse(Z, Y):
    """

    :param Z: predicted values
    :param Y: actual values
    :return: mean squared error
    """

    n = len(Z)
    sub = Z[:, ] - Y[:, ]
    sub = sub ** 2
    return sub.sum()/n

# Krige
krige_per = mse(krige_pred, Y_true)
rbf_per = mse(rbf_pred, Y_true)
tps_per = mse(tps_pred[:,], Y_true)
print(tps_per)

fig2 = plt.figure(2)
methods = ('Krigging', 'TPS')
performance = [krige_per, rbf_per]
x_pos = np.arange(len(methods))
plt.bar(x_pos, performance, align='center', alpha=0.5)
plt.xticks(x_pos, methods)
plt.ylabel('MSE')
plt.title('Performance of methods')


plt.show()
