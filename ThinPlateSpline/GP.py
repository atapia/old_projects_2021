"""
deMello Group

Author: Andres Rocha Tapia
Date: 26.03.2020

In this script, we use the RBF model to interpolate using thin plate spline (TPS), a data space and derive a reaction surface.
The employed data if from Perovskite research provided by Shangkun/Phil

"""

# Data import packages
import pandas as pd

# Plotting packages
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt

# Interpolation Package
from scipy.optimize import minimize
from rbf.gauss import GaussianProcess
from rbf.gauss import gpse


# miscellaneous
from datetime import datetime
import numpy as np

# Here we read the data using a pandas data frame
data = pd.read_csv('./Data/_stats.csv')
S = data[['Temperature', 'Base:acid']]
S = S.to_numpy()
column = 'Median / nm'
Y = data[column].to_numpy()

# We create the linear space to predict
# Mind that if we use a vale of n, we effectively are probing a surface with n^2 points
# Think of speed of calculation, n=100 can take a few minutes!!!!
n = 50
T_pred = np.linspace(90, 190, n).reshape((-1, 1))
X_pred = np.linspace(.7, 1.2, n).reshape((-1, 1))
S_pred = np.reshape(np.meshgrid(T_pred, X_pred), (2, n*n)).T
sigma = Y * 0.01

########################################################################################################################

gp_prior = gpse((25.22253, 32.1705, 12))
gp_post = gp_prior.condition(S, Y, sigma=sigma)
mu, std = gp_post(S_pred)

########################################################################################################################

fig = plt.figure()

xx, yy = np.meshgrid(T_pred, X_pred)
# In this section I take the computed values and reshape them into a grid matrix so that they may be
# plotted as a surface

# Creating grid to store the values of TPS
L_grid = np.zeros((n, n))
print('\nL_grid shape: ', L_grid.shape)

# Assigning values ot grid
k = 0
for i in np.arange(n):
    for j in np.arange(n):
        L_grid[i, j] = mu[k]
        k += 1

plt.contourf(xx, yy, L_grid)
plt.colorbar()
plt.show()
