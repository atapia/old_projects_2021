import numpy as np
import numba as nb


# pts1 = Mx2 matrix (original coordinates)
# z1   = Mx1 column vector (original values)
# pts2 = Nx2 matrix (interpolation coordinates)

def gen_K(n, pts1):
    K = np.zeros((n, n))

    for i in range(0, n):
        for j in range(0, n):
            if i != j:
                r = ((pts1[i, 0] - pts1[j, 0]) ** 2.0 + (pts1[i, 1] - pts1[j, 1]) ** 2.0) ** 0.5
                K[i, j] = r ** 2.0 * np.log(r)

    return K


def compute_z2(m, n, pts1, pts2, coeffs):
    z2 = np.zeros((m, 1))

    x_min = np.min(pts1[:, 0])
    x_max = np.max(pts1[:, 0])
    y_min = np.min(pts1[:, 1])
    y_max = np.max(pts1[:, 1])

    for k in range(0, m):
        pt = pts2[k, :]

        # If point is located inside bounding box of pts1
        if (pt[0] >= x_min and pt[0] <= x_max and pt[1] >= y_min and pt[1] <= y_max):
            z2[k, 0] = coeffs[-3, 0] + coeffs[-2, 0] * pts2[k, 0] + coeffs[-1, 0] * pts2[k, 1]

            for i in range(0, n):
                r2 = ((pts1[i, 0] - pts2[k, 0]) ** 2.0 + (pts1[i, 1] - pts2[k, 1]) ** 2.0) ** 0.5

                if r2 != 0:
                    z2[k, 0] += coeffs[i, 0] * (r2 ** 2.0 * np.log(r2))

        else:
            z2[k, 0] = np.nan

    return z2


gen_K_nb = nb.jit(nb.float64[:, :](nb.int64, nb.float64[:, :]), nopython=True)(gen_K)
compute_z2_nb = nb.jit(nb.float64[:, :](nb.int64, nb.int64, nb.float64[:, :], nb.float64[:, :], nb.float64[:, :]),
                       nopython=True)(compute_z2)


def TPS(pts1, z1, pts2, factor):
    n, m = pts1.shape[0], pts2.shape[0]

    P = np.hstack((np.ones((n, 1)), pts1))
    Y = np.vstack((z1, np.zeros((3, 1))))

    K = gen_K_nb(n, pts1)
    K += factor * np.identity(n)

    L = np.zeros((n + 3, n + 3))
    L[0:n, 0:n] = K
    L[0:n, n:n + 3] = P
    L[n:n + 3, 0:n] = P.T

    L_inv = np.linalg.inv(L)
    coeffs = L_inv.dot(Y)

    return compute_z2_nb(m, n, pts1, pts2, coeffs)
