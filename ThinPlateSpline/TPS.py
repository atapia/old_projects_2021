"""
deMello Group

Author: Andres Rocha Tapia
Date: 26.03.2020

In this script, we use the RBF model to interpolate using thin plate spline (TPS), a data space and derive a reaction surface.
The employed data if from Perovskite research provided by Shangkun/Phil

"""

# Data import packages
import pandas as pd

# Plotting packages
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.pyplot import Axes

# Interpolation Package
from rbf.interpolate import RBFInterpolant
from TPS_own import TPS

# miscellaneous
from datetime import datetime
import numpy as np


# Here we read the data using a pandas data frame
data = pd.read_csv('./Data/_stats.csv')
S = data[['Temperature', 'Base:acid']]
S = S.to_numpy()
column = 'Median / nm'
Y = data[column].to_numpy()

# We create the linear space to predict
# Mind that if we use a vale of n, we effectively are probing a surface with n^2 points
# Think of speed of calculation, n=100 can take a few minutes!!!!
n = 100
T_pred = np.linspace(90, 190, n).reshape((-1, 1))
X_pred = np.linspace(.7, 1.2, n).reshape((-1, 1))
S_pred = np.reshape(np.meshgrid(T_pred, X_pred), (2, n*n)).T

# Creating the TPS interpolant
inty = RBFInterpolant(S, Y, sigma=10, phi='phs2', order=1)

# Interpolate points
y_pred = inty(S_pred)

#######################################################################################################################

# Using self coded impletantion (source StackOverflow following https://profs.etsmtl.ca/hlombaert/thinplates/)

y_pred2 = TPS(S, Y.reshape((-1, 1)), S_pred, 0.7)

#######################################################################################################################
# Plotting the results

xx, yy = np.meshgrid(T_pred, X_pred)
# In this section I take the computed values and reshape them into a grid matrix so that they may be
# plotted as a surface

# Creating grid to store the values of TPS
L_grid = np.zeros((n, n))
K_grid = L_grid.copy()
print('\nL_grid shape: ', L_grid.shape)

# Assigning values ot grid
k = 0
for i in np.arange(n):
    for j in np.arange(n):
        L_grid[i, j] = y_pred[k]
        K_grid[i, j] = y_pred2[k]
        k += 1

# Plotting TPS surfaces
fig, axs = plt.subplots(1, 2)
ax1 = axs[0]
ax2 = axs[1]

# Plotting surface obtained from RBF package
im1 = axs[0].contourf(xx, yy, L_grid)
fig.colorbar(im1, ax=axs[0])
ax1.set_xlabel('Temperature (°C)')
ax1.set_ylabel('Acid Ratio')
ax1.set_title(column)

# plotting from self implemented code
im2 = ax2.contourf(xx, yy, K_grid)
fig.colorbar(im2, ax=ax2)
ax2.set_xlabel('Temperature (°C)')
ax2.set_ylabel('Acid Ratio')
ax2.set_title('Self-Implementation')

fig.tight_layout()
plt.show()

#######################################################################################################################

