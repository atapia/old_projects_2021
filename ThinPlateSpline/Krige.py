import numpy as np
import numpy.matlib as npmat
import constrNMPy.constrNMPy as opt
import scipy.optimize as opt2


def GaussModel(S, theta, X=np.array([]), p=2, predict=False):
    """
    This function computes the Auto-correlation matrix R of a sampled surfaced of k dimensions
    R = matrix (nxn) whose (i, j) entries are Correlation[x(i), x(j)]

    It also serves to calculate the auto-correlation vector R for a single additional point for which we want
    to predict a new output. Only active when when call the function with 'predict=True',
    and we provide an array X (1xk)

    The underlying formula of the correlation is:

    Correlation[x(i), x(j)] = exp( - Sum|k : [ theta * ( x(i) - x(j) ) ^ p ] )

    That is we compute the distance between each point along all dimensions adn take the exponential of their sum.

    In the future parameter 'p' can be used as an additional complexity in order to fit data with increased accuracy.

    :param S: n-vector of sampled variables in the k-dimmensional space (nxk)
    :param theta: k-vector fitting parameter associated to the correlation of each point (1xk)
    :param p: defines the smoothness of the data, by default 2 (gaussian like distribution)
    :param X: (1xk) vector of desired prediction point, by default empty so to use only when predictions are needed
    :param predict: activates whether an X point is to be interpolated (must provide X)
    :return: R = correlation matrix (nxn) whose (i, j) entries are Corre[x(i), x(j)]
          or R, R = correlation matrix (nxn) and correlation correlation vector (1xk) [when predict=True]

    """

    # Normalisation of sampled points
    S_norm = (S - np.mean(S, 0)) / np.std(S, 0, ddof=1)

    # Calculate the pairwise distances

    # we create a column vector with n*(n-1)/2 entries (we have a symmetrical matrix along diagonal!),
    # each entry would represent the distance of each vector to the rest of the others

    # Obtainning the size of the column vector
    n = np.size(S, 0)
    size_v = int(n * (n - 1) / 2)

    # Here we create a column vector 'vectors' to store the ( x(i) - x(j) ) distance for all points.
    # This is done in a element-wise method, therefore it works in all dimensions

    #                           This term her determines
    #                           the dimensionality of
    #                           the system
    vectors = np.ones([size_v, np.size(S, 1)])
    k = -1
    for i in np.arange(n - 1):
        vectors[k + 1: (k + n - i), ] = S_norm[i + 1:, ] - S_norm[i]
        k = k + n - i - 1

    # 'theta_vec' is a replicated vector of user provided input 'theta'. We replicate the entries of theta so
    # that we obtain a vector of equal size to that of the previously computed 'vectors'

    theta_vec = npmat.repmat(theta, np.size(vectors, 0), 1)
    #print('GAUSS: theta is of shape in after repmat: ', np.shape(theta_vec))

    # We compute a correlation vector using the correlation function
    R_vec = np.exp(-np.sum((vectors ** p) * theta_vec, 1))

    # In this section we create a preliminary correlation matrix 'R_lower' for which we can assign the values of 'R_vec'
    # and put them into the lower triangle of matrix 'R_lower'
    R_lower = np.ones([n, n])

    # Loops used to arrange the values of 'R_vec' into shape in 'R_lower
    k = -1
    j = 0
    for i in np.arange(n - 1):
        R_lower[i + 1:, j] = R_vec[k + 1:k + n - i]
        j += 1
        k += n - 1 - i

    # Since R is a symmetric matrix we simpy transpose the lower half of the correlation matrix to obtain the full final
    # R correlation matrix
    R = np.tril(R_lower) + (np.tril(R_lower, -1)).T

    # This section computes the auto-correlation vector for a desired prediction point 'X'. It works in a similar manner
    # to the computation of the 'R_vec' variable
    if predict:

        # h_r is the distance vector equivalent to 'Vectors'
        h_r = S_norm - (X - np.mean(S, 0)) / np.std(S, 0, ddof=1)

        # computation of correlation vector based on correlation function
        r = np.exp(-np.sum(h_r ** p * npmat.repmat(theta, np.size(h_r, 0), 1), 1))
        return R, r
    else:
        return R


def loglike(R, Y, N=False, allparam=False):
    """

    This function computes the logarithmic likelihood of the model. The expression is derived in the paper's support
    info MARIA colors by Leonard Bezinge et al.

    :param R: (nxn) correlation matrix obtained from the GaussModel function
    :param Y: n-vector of values of sampled points
    :param N: determines whether the Y values need to be normalised, by default false. This is an internal configuration
              of the overall Krigging model. Normalisation is not used when predicting, only for the optimisation part
    :param allparam: determines whether the function returns the full calculated parameters (mu, ss and llh)
                     or only the Likelihood (llh)
    :return: mu, ss, llh = that is estimated mean and variance and the likelihood respectively
    """

    # Obtain the size of the column vector Y
    n = len(Y)

    # Checks whether normalisation is need
    if N:
        Y = (Y - Y.mean()) / np.std(Y, ddof=1)

    # The model requires a column vector of ones of the same size as Y
    ones = np.ones([n, 1])

    # We calculate the inverse matrix of R
    r_inv = np.linalg.inv(R)

    # Calculation of the estimated mean 'mu'
    mu = tripleP(ones.T, r_inv, Y) / tripleP(ones.T, r_inv, ones)

    # Intermediary vector where we substract the mean from all entries of Y
    y_sub = np.subtract(Y, ones * mu)

    # Calculate the estimated variance 'ss'
    ss = tripleP(y_sub.T, r_inv, y_sub) / n

    # This section serves only as error catching mechanism to avoid errors while running the optimisation of theta
    det_R = np.linalg.det(R)
    if det_R > 0 and ss > 0:

        # Calculate the logarithmic likelihood
        llh = -0.5 * n * np.log(ss) - 0.5 * np.log(det_R)
    else:
        llh = -10

    # Determines the output based on 'allparam' input
    if allparam:
        return llh, mu, ss
    else:
        return llh


# Simple function created to compute the matrix product of three matrices a X b X c
def tripleP(a, b, c):
    return np.matmul(a, np.matmul(b, c))


def model(R, r, mu, sig_sq, Y):
    """

    This function is a "compilation" function. It takes all the computed and provided values of the previously
    defined functions and uses them to compute our predictions.

    :param R: computed auto-correlation matrix (nxn), obtained from GaussModel()
    :param r: computed auto-correlation vector (nx1), obtained from GaussModel()
    :param mu: estimated mean (1, ), obtained from loglike()
    :param sig_sq: estimated variance (1, ), obtained from loglike()
    :param Y: n-vector of values of sampled points
    :return: y, y_ss : that is the predicted value at point r(X) and its associated estimated variance
    """

    # We require a column vector of ones of same size as 'Y'
    n = np.size(Y, 0)
    ones = np.ones([n, 1])

    # Compute inverse matrix of R and intermediary vector of Y - mean for simplicity
    R_inv = np.linalg.inv(R)
    y_sub = np.subtract(Y, np.multiply(ones, mu))

    # N.B. : notice that the following lines are what the model of Krigging actually relies on to compute
    #        the predictions. These are simply the estimated mean and variance of the sampled space
    #        plus a deviation based on the correlations

    # Compute the prediction point
    y = mu + tripleP(r.T, R_inv, y_sub)

    # Compute the variance of y
    y_ss = sig_sq * (1 + tripleP(r.T, R_inv, r) +
                     (1 - tripleP(ones.T, R_inv, r)) ** 2 / tripleP(ones.T, R_inv, ones))

    return y, y_ss


def optimizer(S, Y):
    """
    This function will get for us the optimal values of theta for a given sampled points.
    This is done by performing a maximisation of the likelihood estimator as a function of theta.

    :param S: Sampled points vector in the k-dimensional space (nxk)
    :param Y: n-vector of values of sampled points
    :return: optimum: that is the optimal value of theta found
    """

    # Reporter
    print('\nEntering Optimizer:\n')

    # Initial Values and Bounds
    k = np.size(S, 1)  # This get the dimension we are working on
    X0 = 10 * np.ones([1, k])
    print('The initial guess for theta: ', X0)
    lb = 0.1
    ub = 20

    # This is a function that creates a Tuple list of size k, of repeating values of the lower and upper bounds.
    # It was needed for certain bounded optimisation algorithms. In the final case left unused, but remains in case of
    # needing another type of optimisation
    def bound(k):
        return ((lb, ub), ) * k

    # Here we re-functionalize the likelihood expression as a sole function of theta
    def fun(theta):
        # print(theta)
        return -loglike(GaussModel(S, theta), Y, N=True)

    '''
    The section below is where we call upon the optimizer. 
    There are multiple commented lines as I was testing which optimizer should we used, since I was having problems
    with some of them to converge to a solution.
    
    In the end, the simplex algorithm was used in unbounded optimisation.
    
    '''

    #KrigPar = opt.constrNM(fun, X0, npmat.repmat(lb, 1, k), npmat.repmat(ub, 1, k), full_output=False, ftol=1e-8, maxfun=1000)
    #print('Optimized Value for Theta is: ', KrigPar)

    #return KrigPar['xopt']

    optimum = opt2.minimize(fun, X0, method='Nelder-Mead', tol=1e-8)

    #optimum = opt2.fmin_l_bfgs_b(fun, X0, approx_grad=True, bounds=bound(k), pgtol=1e-5, epsilon=0.001,
    #                           maxfun=1000, factr=1e3)

    print(optimum)
    print('\nExiting optimizer...')
    return optimum.x


def blup(S, Y, X):
    """
    This is also a type of compilation function. It unifies all the previous functions into a single function and
    computes the predictions based on the given sampled points and their respective values

    Notice that here we employ all other functions (see below for the list):
    - GaussModel()
    - optimizer()
    - loglike()
    - model()

    :param S: n-vector of sampled variables in the k-dimmensional space (nxk)
    :param Y: n-vector of values of sampled points (nx1)
    :param X: m-vector of desired prediction points (mxk)
    :return: y, ss_y :  that is the predicted value and its respective estimated variance

    """
    # Get the optimal value of theta from sampled points
    theta = optimizer(S, Y)
    print('BLUP: Optimized Value for Theta is: ', theta)

    # Get model constants
    R = GaussModel(S, theta)
    L, mu, ss = loglike(R, Y, allparam=True)
    print('BLUP: Estimated mean and variance are: ', mu, ' ', ss)

    # Calculate predictions

    # Create empty column vectors to store computed solutions
    y = np.zeros(np.size(X, 0))
    ss_y = np.copy(y)

    for i in np.arange(np.size(X, 0)):
        _, r = GaussModel(S, theta, X=X[i], predict=True)
        y[i], ss_y[i] = model(R, r, mu, ss, Y)

    return y, ss_y
