import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib import cm

directory = R'C:\Users\atapia\PycharmProjects\DataPlotter\Data\\'
DATA = pd.read_csv(directory + 'oneTaq_qPCR_data.csv')
DATA = pd.pivot(DATA, columns=['Cycle'], values=['Rn'], index=['Well'])
DATA.to_csv(directory + 'pivot.csv')

fig, ax = plt.subplots(2, 2, figsize=(12, 12))
cycles = np.arange(40) + 1
colors = ['Purples', 'Greens', 'Blues', 'Reds']
titles = ['OneTaq SYBR(-)', 'OneTaq SYBR(+)', 'OneTaq PTO SYBR(-)', 'OneTaq PTO SYBR(+)']

cc = 0
ax = ax.reshape(-1)
for axis in ax:
    cmap = cm.get_cmap(colors[cc])
    cmap = cmap(np.linspace(0, 1, 12))
    axis.set_title(titles[cc])
    #axis.set_ylim([0, 2e6])
    for j in np.arange(8) * 12:
        im = axis.plot(cycles, DATA.iloc[j + cc * 3] / np.max(DATA.iloc[j + cc * 3]), color=cmap[11 - int(j / 12)])
        axis.plot(cycles, DATA.iloc[j + cc * 3], color=cmap[11 - int(j / 12)])
        axis.plot(cycles, DATA.iloc[j + cc * 3], color=cmap[11 - int(j / 12)])
    cc += 1

ax[2].set_xlabel('Cycle')
ax[2].set_ylabel('Absolute Fluorescent Units')
fig.tight_layout()
plt.show()
fig.savefig(directory + 'qPCR_2022-01-19_RBT_templateConc.jpg')
