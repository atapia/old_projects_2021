close all
clear

inputDirectory = '/Volumes/ICB-DEM-ART/PCR Project/HS Camera Sessions/2022.04.04/HFE2%_droplets_suspended_in_FC40/Raw Image/day1_10%HFE_UW/day1_10%HFE_UW.AVI';

t=VideoReader(inputDirectory);
cropped=read(t,1);
[centers, radii, metric] = imfindcircles(cropped,[10 55],'ObjectPolarity','bright');
radiinew=(70/40)*radii*2;
radiinew=radiinew(radiinew<90);
h=figure
imshow(cropped)
viscircles(centers, radii,'Color','b');
h=figure
histogram(radiinew)
title('Droplet Size Distribution')
xlabel('Size in um') 
ylabel('Number of Droplets') 
grid on