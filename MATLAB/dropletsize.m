close all
clear 

inputDirectory = '/Volumes/ICB-DEM-ART/PCR Project/HS Camera Sessions/2022.04.04/HFE2%_droplets_suspended_in_FC40/';
inputFiles = dir(join([inputDirectory, 'Raw Image/'], ''));
outputDirectory = '/Users/andres/Dev/Repositories/Pycharm Projects/DropletDistribution/';
dirFlag = [inputFiles.isdir];
subfolders = inputFiles(dirFlag);
subfolderNames = {subfolders.name}; subfolderNames = subfolderNames(4:end);

for folder = subfolderNames
    if startsWith(folder, 'day1')
%         continue
    elseif startsWith(folder, 'day4') || startsWith(folder, 'day3') || startsWith(folder, 'day2')
        continue
    end
    findCircles(folder, inputDirectory, outputDirectory);
end

% findCircles(subfolderNames(3), inputDirectory, outputDirectory);

% 
% % display frame and overlay the found circles
% h=figure
% imshow(frame)
% viscircles(centers, radii,'Color','b')
% 
% % display distribution of found diameters
% h=figure
% set(h,'WindowStyle','docked')
% %histfit(diameter(diameter>72))
% histogram(diameter)
% title('Droplet Size Distribution')
% xlabel('Size in um') 
% ylabel('Number of Droplets') 
% grid off
% 
% fitdist(diameter(diameter>72), 'Normal')
