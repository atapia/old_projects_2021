function findCircles(folder, inputDirectory, outputDirectory)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
inputFullPath = join([inputDirectory, 'Raw Image/', folder, '/' ,folder, '.AVI'], '') %#ok<NOPRT> 

saveNameStart = extractBefore(folder, 'HFE');
saveNameEnd =  extractAfter(folder, 'HFE');
saveNameFull = join([saveNameStart, saveNameEnd], '');
savePath = join([outputDirectory, 'Data/', saveNameFull, '.csv'], '');

% Extracting frame
t=VideoReader(string(inputFullPath));
frame=read(t,1);
% Find circles, with the range specified in the array argument (in pixels)
[centers, radii, metric] = imfindcircles(frame,[10 55],'ObjectPolarity','bright');
% Converts radii into um units and double for diamete

% Correcting nuit conversion dependeing on magnification
if(startsWith(saveNameFull, 'day2')) || (contains(saveNameFull, 'NG') & startsWith(saveNameFull, 'day1')) % for 2x values on old ones
    ratio = (70/29);
elseif startsWith(saveNameFull, 'day4') || startsWith(saveNameFull, 'day3') % for 2x values on middle ones day 3-4
    ratio = 70/62;
elseif endsWith(saveNameFull, '2x')
    ratio = 70/19.5;
elseif endsWith(saveNameFull, '4x')
    ratio = 70/41;
else
    ratio = 70/40;
end

% Thresholding and removing far outliers
diameter = ratio*2*radii;
flagDiam = 60<diameter & diameter<120;
diameter = diameter(flagDiam);
xpos = centers(:, 1);
xpos = xpos(flagDiam);
ypos = centers(:, 2);
ypos = ypos(flagDiam);
metric = metric(flagDiam);

data = [xpos, ypos, diameter, metric];
writematrix(data, string(savePath));
imwrite(frame, string(join([outputDirectory, 'Images/', saveNameFull, '.png'], '')), "png");

end