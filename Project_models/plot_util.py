from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.pyplot import Axes
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import ticker

def format(x, pos):
    return '%.3e' % (x)

def data_surface(xx, yy, DataGrid, setName, axis, fig):

    im1 = axis.contourf(xx, yy, DataGrid, 30, cmap='viridis')
    im2 = axis.contour(xx, yy, DataGrid, 5, colors='k', linewidths=0.8)
    fig.colorbar(im1, ax=axis, cmap='viridis')
    formatter = ticker.FuncFormatter(format)
    axis.clabel(im2, inline=True, fontsize=8, fmt=formatter)
    # Formatting
    axis.set_xlabel('Temperature (°C)')
    axis.set_ylabel('Cs:Pb Ratio')
    axis.set_title(setName, pad=5)

