# Interpolation Package (and dependencies)
import Krige as kr

# Standard Packages
import pandas as pd
import numpy as np

# Plotting Packages
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.pyplot import Axes
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D

# Importing Data
data = pd.read_csv('./Data/_stats.csv')
S = data[['Temperature', 'Base:acid']]
S = S.to_numpy()
column = ['Median / nm', 'IQR / nm']
Y_med = data[column[0]].to_numpy()
Y_iqr = data[column[1]].to_numpy()

# Create the linear space to predict
n = 100
T_pred = np.linspace(90, 190, n).reshape((-1, 1))
X_pred = np.linspace(.7, 1.2, n).reshape((-1, 1))
S_pred = np.reshape(np.meshgrid(T_pred, X_pred), (2, n*n)).T

# Interpolating Points
y_med_pred, _ = kr.blup(S, Y_med.reshape((-1, 1)), S_pred)
y_iqr_pred, _ = kr.blup(S, Y_iqr.reshape((-1, 1)), S_pred)

#######################################################################################################################
# Plotting the results

# Creating necessary girds to plot in 3D surface style
xx, yy = np.meshgrid(T_pred, X_pred)
med_grid = y_med_pred.reshape((n, n))
iqr_grid = y_iqr_pred.reshape((n, n))

# Plotting Kriged surfaces
fig = plt.figure(figsize=plt.figaspect(2))

# Plotting Median surface
ax1 = fig.add_subplot(2, 1, 1)
im1 = ax1.contourf(xx, yy, med_grid, 30, cmap='viridis')
fig.colorbar(im1, ax=ax1, cmap='viridis')
# Formatting
ax1.set_xlabel('Temperature (°C)')
ax1.set_ylabel('Base:Acid Ratio')
ax1.set_title('Median / nm', pad=5)

# Plotting IQR surface
ax2 = fig.add_subplot(2, 1, 2)
im2 = ax2.contourf(xx, yy, iqr_grid, 30, cmap='viridis')
fig.colorbar(im2, ax=ax2, cmap='viridis')
# Formatting
ax2.set_xlabel('Temperature (°C)')
ax2.set_ylabel('Base:Acid Ratio')
ax2.set_title('IQR / nm', pad=5)

# General Formatting
plt.subplots_adjust(left=0.05, bottom=0.06, right=0.97, top=0.99, wspace=0.02, hspace=0.08)
fig.tight_layout()

# Visualisation and Saving
plt.show()
fig.savefig('./Graphs/kr_plots')
plt.close()
