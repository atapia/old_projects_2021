# Standard Packages
import pandas as pd
import numpy as np

# Plotting Packages
from matplotlib import cm
import matplotlib.pyplot as plt
from plot_util import data_surface

# Importing Data
data = pd.read_csv('./Data/_stats.csv')
S = data[['Temperature', 'Base:acid']]
S = S.to_numpy()
xx = S[:, 0].reshape((11, 11))
yy = S[:, 1].reshape((11, 11))
column = ['Median / nm', 'IQR / nm']
Y_med = data[column[0]].to_numpy().reshape((11, 11))
print(Y_med.shape)
Y_iqr = data[column[1]].to_numpy().reshape((11, 11))
Y = [Y_med, Y_iqr]

#######################################################################################################################
# Plotting the Actual Data
fig = plt.figure(figsize=plt.figaspect(2))

# Plotting Median surface
ax1 = fig.add_subplot(2, 1, 1)
im1 = ax1.tricontourf(S[:, 0], S[:, 1], Y_med.ravel(), levels=30)
fig.colorbar(im1, ax=ax1, cmap='viridis')
# Formatting
ax1.set_xlabel('Temperature (°C)')
ax1.set_ylabel('Base:Acid Ratio')
ax1.set_title('Median / nm', pad=5)

# Plotting IQR surface
ax2 = fig.add_subplot(2, 1, 2)
im2 = ax2.tricontourf(S[:, 0], S[:, 1], Y_iqr.ravel(), levels=30)
fig.colorbar(im2, ax=ax2, cmap='viridis')
# Formatting
ax2.set_xlabel('Temperature (°C)')
ax2.set_ylabel('Base:Acid Ratio')
ax2.set_title('IQR / nm', pad=5)

# General Formatting
plt.subplots_adjust(left=0.05, bottom=0.06, right=0.97, top=0.99, wspace=0.02, hspace=0.08)
fig.tight_layout()




# Visualisationg and Saving
plt.show()
fig.savefig('./Graphs/actual_plots')
plt.close()
