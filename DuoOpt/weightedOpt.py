# General Packages
import numpy as np
import pandas as pd
import time

# Model Packages
from sklearn.preprocessing import StandardScaler
from rbf.interpolate import RBFInterpolant

# Optimisation
from scipy.optimize import minimize
from opt_util import prepare_data, find_optimum

# Plotting
import matplotlib.pyplot as plt
from plot_util import data_surface

# Measure excution time
begin_time = time.time()

directory = './Data/'

#######################################################################################################################
# Import Data Section and Data Manipulators

# Import Data
C8 = pd.read_csv(directory + 'C8.csv')
C18 = pd.read_csv(directory + 'C18.csv')
columns = list(C8.columns[2:])
inputs = C8[['Temperature', 'Base:acid']].to_numpy()

# Import CV Results
C8_sigma = pd.read_csv(directory + 'C8_CVresults.csv')
C18_sigma = pd.read_csv(directory + 'C18_CVresults.csv')

# Define 6 dataScale instances for each response variable,
# we will denote Intensities as F1 and F2 and IQR as C1 and C2 respectively from each data set
# Furthermore we will define additional constraint response variable C3 and C4 for peak position
scale_inputs = StandardScaler()
F1_data, scale_F1 = prepare_data(C8, columns[1])
F2_data, scale_F2 = prepare_data(C18, columns[1])
C1_data, scale_C1 = prepare_data(C8, columns[-1], TypeOptimisation='min')
C2_data, scale_C2 = prepare_data(C18, columns[-1], TypeOptimisation='min')
C3_data, scale_C3 = prepare_data(C8, columns[0], TypeOptimisation='min')
C4_data, scale_C4 = prepare_data(C18, columns[0], TypeOptimisation='min')
scalers = [scale_F1, scale_F2, scale_C1, scale_C2, scale_C3, scale_C4]

# Select the optimal model parameters
sigma_F1 = C8_sigma[columns[1]].to_numpy()[0]
sigma_F2 = C18_sigma[columns[1]].to_numpy()[0]
sigma_C1 = C8_sigma[columns[-1]].to_numpy()[0]
sigma_C2 = C18_sigma[columns[-1]].to_numpy()[0]
sigma_C3 = C8_sigma[columns[0]].to_numpy()[0]
sigma_C4 = C18_sigma[columns[0]].to_numpy()[0]

# Scale inputs
inputs = scale_inputs.fit_transform(inputs)

########################################################################################################################
# Create Model Instances

# We create the instances of the Fitting model (in this case we use TPS, for speed). These effectively
# are the functions we wish to optimise
F1 = RBFInterpolant(inputs, F1_data.ravel(), sigma=sigma_F1, phi='phs2', order=1)
F2 = RBFInterpolant(inputs, F2_data.ravel(), sigma=sigma_F2, phi='phs2', order=1)
C1 = RBFInterpolant(inputs, C1_data.ravel(), sigma=sigma_C1, phi='phs2', order=1)
C2 = RBFInterpolant(inputs, C2_data.ravel(), sigma=sigma_C2, phi='phs2', order=1)
C3 = RBFInterpolant(inputs, C3_data.ravel(), sigma=sigma_C3, phi='phs2', order=1)
C4 = RBFInterpolant(inputs, C4_data.ravel(), sigma=sigma_C4, phi='phs2', order=1)


# Create helper method to facilitate optimisation
def predict(x, RBF):
    # print('Entering Predict')
    x = x.reshape((1, -1))
    return RBF(x)


########################################################################################################################

# Define necessary variables to run optimisations

# Define Bounds of Optimisation
# Note that we must rescale them in order for the models to be able to run
Xmin = scale_inputs.transform(np.array([80.0001, 0.600001]).reshape((1, -1)))
Xmax = scale_inputs.transform(np.array([189.99999, 1.099999]).reshape((1, -1)))
F1_max = find_optimum(F1, [[0, 0]], [Xmin, Xmax])
F2_max = find_optimum(F2, [[0, 0]], [Xmin, Xmax])
print(scale_inputs.inverse_transform(F1_max))

def weighted_opt(X, w):
    print('Entering weighted_opt method')
    w1, w2, w3, w4 = w
    def constrain_C1(x):
        x = x.reshape((1, -1))
        return - C1(x) + scale_C1.transform([[20]])[0]  # contraint IQR <= 20 nm

    def constrain_C2(x):
        x = x.reshape((1, -1))
        return - C2(x) + scale_C2.transform([[20]])[0]

    def constrain_C3(x):
        x = x.reshape((1, -1))
        return - C3(x) + scale_C3.transform([[500]])[0] # contraint peak <= 510 nm

    def constrain_C4(x):
        x = x.reshape((1, -1))
        return - C4(x) + scale_C4.transform([[500]])[0]

    constraints = (
        #{'type': 'ineq', 'fun': constrain_C1},
        #{'type': 'ineq', 'fun': constrain_C2},
        {'type': 'ineq', 'fun': constrain_C3},
        {'type': 'ineq', 'fun': constrain_C4},
    )

    def F_to_optimise(x):
        #print('Real max:',  predict(F1_max, F1), predict(F2_max, F2))
        #print(predict(x, F1), predict(x, F2), 'at ', scale_inputs.inverse_transform(x))
        return w1 * predict(x, F1) + w2 * predict(x, F2) + w3 * predict(x, C1) + w4 * predict(x, C2)

    # Call the optimizer and initialize with function to be optimized and its constraints
    solution = minimize(F_to_optimise, X, constraints=constraints,
                        bounds=((Xmin[0, 0], Xmax[0, 0]), (Xmin[0, 1], Xmax[0, 1])),
                        options={
                            #'disp': True,
                            # 'gtol': 1e-12
                        }
                        )

    if solution.success:
        print('Succesful Optimisation')
        print('Initial Guess x = ', scale_inputs.inverse_transform(X[0]))
        print('Solution found x = ', scale_inputs.inverse_transform(solution.x))
        print('Objective Values = ', np.around([scale_F1.inverse_transform(predict(solution.x, F1))[0], scale_F2.inverse_transform(predict(solution.x, F2))[0],
                                        scale_C1.inverse_transform(predict(solution.x, C1))[0], scale_C2.inverse_transform(predict(solution.x, C2))[0]], 2))
        print()
        return scale_inputs.inverse_transform(solution.x)
    else:
        print('Error in optimisation: ', solution.message)
        print('Optimum not found, try another initial guess')
        return [0, 0]

#######################################################################################################################

# Define a guess area:
m = 11
T = np.linspace(120, 170, m).reshape((-1, 1))
X = np.linspace(0.95, 1.05, m).reshape((-1, 1))
Guess_Grid = np.reshape(np.meshgrid(T, X), (2, m*m)).T
Guess_Grid = scale_inputs.transform(Guess_Grid)
X_sol = np.zeros_like(Guess_Grid)
print(X_sol.shape)

# Define the set of weihgts:
w = [.4, .4, 0.1, 0.1]

for i, X_guess in enumerate(Guess_Grid):
    X_sol[i] = np.around(weighted_opt(X_guess.reshape((1, -1)), w), 3)

X_sol = np.unique(X_sol, axis=0)
print(X_sol)
########################################################################################################################
# Plotting found Solutions onto map to see position relative to response variables

# Create the linear space to generate surface
n = 100
T_pred = np.linspace(90, 190, n).reshape((-1, 1))
X_pred = np.linspace(.6, 1.1, n).reshape((-1, 1))
S_pred = np.reshape(np.meshgrid(T_pred, X_pred), (2, n*n)).T
xx, yy = np.meshgrid(T_pred, X_pred)

# Scale input map
S_pred = scale_inputs.transform(S_pred)

fig, axs = plt.subplots(2, 3, figsize=(15, 10))
axes = list(axs.flatten())

for axis, Fi, scale_i, dataSet in \
        zip(axes, [F1, C1, C3, F2, C2, C4], [scale_F1, scale_C1, scale_C3, scale_F2, scale_C2, scale_C4],
            ['Intensity C8', 'IQR C8', 'Peak C8', 'Intensity C18', 'IQR C18', 'Peak C18']):

    # Running TPS
    Y_pred = Fi(S_pred)
    Y_pred = scale_i.inverse_transform(Y_pred)
    # Turn Y_pred into grid
    dataGrid = Y_pred.reshape((n, n))

    # Treatment of maximized variables (must multiply by -1 since in this script we run all as minimisation)
    if (Fi == F1) or (Fi == F2):
        dataGrid = - dataGrid

    # Plotting surface
    data_surface(xx, yy, dataGrid, dataSet, axis, fig)

    axis.scatter(X_sol[:, 0], X_sol[:, 1], marker='*', color='k', s=120)


    axis.axis([90, 190, 0.6, 1.1])

# General formatting
plt.subplots_adjust(left=0.05, bottom=0.06, right=0.97, top=0.80, wspace=0.2, hspace=0.08)
fig.tight_layout()
fig.savefig('./Graphs/Weighted_opt')

#############################################################################################################
print('Execution time: %.2f seconds' % (time.time() - begin_time))
plt.show()
