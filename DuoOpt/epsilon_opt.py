# General Packages
import numpy as np
import pandas as pd
import time

# Model Packages
from sklearn.preprocessing import StandardScaler
from rbf.interpolate import RBFInterpolant

# Optimisation
from scipy.optimize import minimize
from opt_util import find_optimum

# Plotting
import matplotlib.pyplot as plt
from plot_util import data_surface

begin_time = time.time()
directory = './Data/'

# Import Data
C8 = pd.read_csv(directory + 'C8.csv')
C18 = pd.read_csv(directory + 'C18.csv')
columns = list(C8.columns[2:])
S = C8[['Temperature', 'Base:acid']].to_numpy()
print(S.shape)

# Import CV Results
C8_sigma = pd.read_csv(directory + 'C8_CVresults.csv')
C18_sigma = pd.read_csv(directory + 'C18_CVresults.csv')

# Scalers
scale_inputs = StandardScaler()
scale_outputs_int_1 = StandardScaler() # For C8, Intensity
scale_outputs_int_2 = StandardScaler() # For C18, Intensity
scale_outputs_iqr_1 = StandardScaler() # For C8, IQR
scale_outputs_iqr_2 = StandardScaler() # For C18, IQR
scale_outputs_pak_1 =  StandardScaler() # For C8, peak position
scale_outputs_pak_2 =  StandardScaler() # For C18, peak position

# Scale inputs
S = scale_inputs.fit_transform(S)

# Selected the response variables to optimize, and scale them
int_1 = -C8['Intensity / a.u.'].to_numpy().reshape((-1, 1))
int_1 = scale_outputs_int_1.fit_transform(int_1)

iqr_1 = C8['IQR / nm'].to_numpy().reshape((-1, 1))
iqr_1 = scale_outputs_iqr_1.fit_transform(iqr_1)

int_2 = -C18['Intensity / a.u.'].to_numpy().reshape((-1, 1))
int_2 = scale_outputs_int_2.fit_transform(int_2)

iqr_2 = C18['IQR / nm'].to_numpy().reshape((-1, 1))
iqr_2 = scale_outputs_iqr_2.fit_transform(iqr_2)

peak_1 = C8['Peak position / nm'].to_numpy().reshape((-1, 1))
peak_1 = scale_outputs_pak_1.fit_transform(peak_1)

peak_2 = C18['Peak position / nm'].to_numpy().reshape((-1, 1))
peak_2 = scale_outputs_pak_2.fit_transform(peak_2)

# Select the optimal model parameters
sigma_int_1 = C8_sigma[columns[1]].to_numpy()[0]
sigma_int_2 = C18_sigma[columns[1]].to_numpy()[0]
sigma_iqr_1 = C8_sigma[columns[-1]].to_numpy()[0]
sigma_iqr_2 = C18_sigma[columns[-1]].to_numpy()[0]
sigma_pak_1 = C8_sigma[columns[0]].to_numpy()[0]
sigma_pak_2 = C18_sigma[columns[0]].to_numpy()[0]

# Create interpolators
tps_int_1 = RBFInterpolant(S, int_1.ravel(), sigma=sigma_int_1, phi='phs2', order=1)
tps_int_2 = RBFInterpolant(S, int_2.ravel(), sigma=sigma_int_2, phi='phs2', order=1)
tps_iqr_1 = RBFInterpolant(S, iqr_1.ravel(), sigma=sigma_iqr_1, phi='phs2', order=1)
tps_iqr_2 = RBFInterpolant(S, iqr_2.ravel(), sigma=sigma_iqr_2, phi='phs2', order=1)
tps_pak_1 = RBFInterpolant(S, peak_1.ravel(), sigma=sigma_pak_1, phi='phs2', order=1)
tps_pak_2 = RBFInterpolant(S, peak_2.ravel(), sigma=sigma_pak_2, phi='phs2', order=1)

# Create helper method to facilitate optimisation
def predict(x, RBF):
    # print('Entering Predict')
    x = x.reshape((1, -1))
    return RBF(x)

# Defnie Bounds of Optimisation
Xmin = scale_inputs.transform(np.array([80.0001, 0.600001]).reshape((1, -1)))
Xmax = scale_inputs.transform(np.array([189.99999, 1.099999]).reshape((1, -1)))

# Find global of maximum/minimum
## Attention, solver might run into a local minima, it is adviced to check found optumums and compare to plots
## If the given solution is wrong, try:
#### --> Change Guess X_init (where the solver starts looking)
#### --> Constrain the search space further, by changing Xmin, Xmax
#### --> Change gtol and ftol to lower values (usually this is already at lowest so hopefully you dont need to do this)
#### If all else fails, then probably the optimisation is not feasible
X_init = scale_inputs.transform(np.array([180, 0.7]).reshape((1, -1)))
int_1_max = find_optimum(tps_int_1, X_init, [Xmin, Xmax])
#int_1_max = scale_inputs.inverse_transform(int_1_max)
print(scale_inputs.inverse_transform(int_1_max))

X_init = scale_inputs.transform(np.array([160, 1.0]).reshape((1, -1)))
int_2_max = find_optimum(tps_int_2, X_init, [Xmin, Xmax])
#int_2_max = scale_inputs.inverse_transform(int_2_max)
print(scale_inputs.inverse_transform(int_2_max))

X_init = scale_inputs.transform(np.array([160, 1.05]).reshape((1, -1)))
iqr_1_max = find_optimum(tps_iqr_1, X_init, [Xmin, Xmax])
#iqr_1_max = scale_inputs.inverse_transform(iqr_1_max)

X_init = scale_inputs.transform(np.array([140, 1.05]).reshape((1, -1)))
iqr_2_max = find_optimum(tps_iqr_2, X_init, [Xmin, Xmax])
#iqr_2_max = scale_inputs.inverse_transform(iqr_2_max)

########################################################################################################
# implementation of Epsilon-Constrained Optimisation for 2 response variables for intensity

F_int_1_max = (tps_int_1(int_1_max)).reshape((1, -1))
#F_int_1_max = scale_outputs_int_1.inverse_transform(F_int_1_max)
F_int_1_from2 = (tps_int_1(int_2_max)).reshape((1, -1))
#F_int_1_from2 = scale_outputs_int_1.inverse_transform(F_int_1_from2)

F_int_2_max = (tps_int_2(int_2_max)).reshape((1, -1))
#F_int_2_max = scale_outputs_int_2.inverse_transform(F_int_2_max)
F_int_2_from1 = (tps_int_2(int_1_max)).reshape((1, -1))
#F_int_2_from1 = scale_outputs_int_2.inverse_transform(F_int_2_from1)

# Define the Epsilo9n interval in terms of F_int_2:
epsilon = np.linspace(F_int_2_max[0], F_int_2_from1[0], 1000)

# define empty results vector to store coordinate solutions
X_sol_vec = np.zeros((len(epsilon), 2))

k = 0
for eps in epsilon:

    def predict_from2(x):
        x = x.reshape((1, -1))
        return - tps_int_2(x) + eps[0]

    def constraint_C1(x):
        x = x.reshape((1, -1))
        return - tps_iqr_1(x) + scale_outputs_iqr_1.transform([[21]])[0]

    def constraint_C2(x):
        x = x.reshape((1, -1))
        return - tps_iqr_2(x) + scale_outputs_iqr_2.transform([[21]])[0]

    def constraint_C3(x):
        x = x.reshape((1, -1))
        return - tps_pak_1(x) + scale_outputs_pak_1.transform([[510]])[0]

    def constraint_C4(x):
        x = x.reshape((1, -1))
        return - tps_pak_2(x) + scale_outputs_pak_2.transform([[510]])[0]

    constraint = (
        {'type': 'ineq', 'fun': predict_from2},
        {'type': 'ineq', 'fun': constraint_C1},
        {'type': 'ineq', 'fun': constraint_C2},
        {'type': 'ineq', 'fun': constraint_C3},
        {'type': 'ineq', 'fun': constraint_C4},
    )

    solution = minimize(predict, int_2_max, args=(tps_int_1,), constraints=constraint,
                            bounds=((Xmin[0, 0], Xmax[0, 0]), (Xmin[0, 1], Xmax[0, 1])),
                            #options={'disp': True}
                            )

    if solution.success:
        X_sol_vec[k] = solution.x
    else:
        print(k)
        X_sol_vec[k] = [500, 500]

    k+=1

X_sol_vec = X_sol_vec[X_sol_vec != [500, 500]].reshape((-1, 2))

# Compute values of Intesities for 1 and 2
pareto_1 = scale_outputs_int_1.inverse_transform(tps_int_1(X_sol_vec))
pareto_2 = scale_outputs_int_2.inverse_transform(tps_int_2(X_sol_vec))

# Scale Back Pareto Locations
X_sol_vec = scale_inputs.inverse_transform(X_sol_vec)

n = 100
T_pred = np.linspace(90, 190, n).reshape((-1, 1))
X_pred = np.linspace(.6, 1.1, n).reshape((-1, 1))
S_pred = np.reshape(np.meshgrid(T_pred, X_pred), (2, n*n)).T
xx, yy = np.meshgrid(T_pred, X_pred)
S_pred = scale_inputs.transform(S_pred)

Y_pred_1 = - scale_outputs_int_1.inverse_transform(tps_int_1(S_pred)).reshape((n, n))
Y_pred_2 = - scale_outputs_int_2.inverse_transform(tps_int_2(S_pred)).reshape((n, n))

fig, ax =  plt.subplots(1, 3, figsize=(15, 5))

data_surface(xx, yy, Y_pred_1, 'Intensity C8', ax[1], fig)
data_surface(xx, yy, Y_pred_2, 'Intensity C18', ax[2], fig)

ax[0].plot(-pareto_2, -pareto_1, 'r', linewidth=1.2)
ax[0].set_xlabel('Intensity C18')
ax[0].set_ylabel('Intensity C8')

ax[1].scatter(X_sol_vec[:, 0], X_sol_vec[:, 1], marker='o', s=8, c='r')
ax[2].scatter(X_sol_vec[:, 0], X_sol_vec[:, 1], marker='o', s=8, c='r')

ax[1].axis([90, 190, 0.6, 1.1])
ax[2].axis([90, 190, 0.6, 1.1])

fig.tight_layout()
fig.savefig('./Graphs/pareto_curve')

print()
print('Execution time: %.2f seconds' % (time.time()-begin_time))
plt.show()






