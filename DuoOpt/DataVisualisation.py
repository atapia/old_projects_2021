# Interpolation Package
from rbf.interpolate import RBFInterpolant
from sklearn.preprocessing import StandardScaler

# Standard Packages
import pandas as pd
import numpy as np

# Plotting Packages
from plot_util import data_surface
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.pyplot import Axes
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D

directory = './Data/'

# Import Data
C8 = pd.read_csv(directory + 'C8.csv')
C18 = pd.read_csv(directory + 'C18.csv')
columns = list(C8.columns[2:])
S = C8[['Temperature', 'Base:acid']].to_numpy()

# Import CV Results
C8_sigma = pd.read_csv(directory + 'C8_CVresults.csv')
C18_sigma = pd.read_csv(directory + 'C18_CVresults.csv')

# Create the linear space to predict
n = 100
T_pred = np.linspace(90, 190, n).reshape((-1, 1))
X_pred = np.linspace(.6, 1.1, n).reshape((-1, 1))
S_pred = np.reshape(np.meshgrid(T_pred, X_pred), (2, n*n)).T
xx, yy = np.meshgrid(T_pred, X_pred)

# Scalers
scale_inputs = StandardScaler()
scale_outputs = StandardScaler()

# Scale inputs
S = scale_inputs.fit_transform(S)
S_pred = scale_inputs.transform(S_pred)

# We select a single DataFrame  for simplicity
select = 2

if select == 1:
    DF = C8
    DF_sigma = C8_sigma
elif select == 2:
    DF = C18
    DF_sigma = C18_sigma
else:
    print('Please make sure you set the \'select\' variable to 1 or 2'
          '\n or reassign the variables \'DF\' to your desired DataFrame')


fig = plt.figure(figsize=(20, 5))
ax1 = fig.add_subplot(1, 4, 1)
ax2 = fig.add_subplot(1, 4, 2)
ax3 = fig.add_subplot(1, 4, 3)
ax4 = fig.add_subplot(1, 4, 4)
axs = (ax1, ax2, ax3, ax4)
for axis, dataSet, sigma in zip(axs, columns, list(DF_sigma.iloc[0, 2:].T)):
    print(sigma)

    # Rescaling outputs to feed into TPS
    Y = scale_outputs.fit_transform(DF[dataSet].to_numpy().reshape((-1, 1)))

    # Running TPS
    interpolator = RBFInterpolant(S, Y.ravel(), sigma=sigma, phi='phs2', order=1)
    Y_pred = interpolator(S_pred)
    # Scale back
    Y_pred = scale_outputs.inverse_transform(Y_pred)
    # Turn predictions into grid
    dataGrid = Y_pred.reshape((n, n))

    # Plotting surfaces
    data_surface(xx, yy, dataGrid, dataSet, axis, fig)

# General formatting
plt.subplots_adjust(left=0.05, bottom=0.06, right=0.97, top=0.80, wspace=0.2, hspace=0.08)
fig.tight_layout()

if select == 1:
    plt.show()
    fig.savefig('./Graphs/C8_surfaces')
    print('\nC8 Results saved.')
elif select == 2:
    plt.show()
    fig.savefig('./Graphs/C18_surfaces')
    print('\nC18 Results saved.')
else:
    print()
    print('Work done on other DF, results not saved'
          '\nPlease make sure to make the appropriate modifications to this script')
    print(results)
