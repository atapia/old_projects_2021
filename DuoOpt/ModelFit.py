# General Packages
import numpy as np
import pandas as pd

# Model Packages
from sklearn.preprocessing import StandardScaler
from rbf.interpolate import RBFInterpolant

# CV Packages
from sklearn.utils import shuffle
from sklearn.model_selection import KFold
from sklearn.metrics import r2_score

# Plotting packages
import matplotlib.pyplot as plt

# Import Data
directory = './Data/'
C8 = pd.read_csv(directory + 'C8.csv')
C18 = pd.read_csv(directory + 'C18.csv')
columns = list(C8.columns[2:])
S = C8[['Temperature', 'Base:acid']].to_numpy()
scale_inputs = StandardScaler()

S = scale_inputs.fit_transform(S)

# CV split instance
kf = KFold(3, shuffle=True, random_state=99)

# We select a single DataFrame  for simplicity
select = 1

if select == 1:
    DF = C8
elif select == 2:
    DF = C18
else:
    print('Please make sure you set the \'select\' variable to 1 or 2'
          '\n or reassign the variable \'DF\' to your desired DataFrame')

# Space of sigma to evaluate
sigma = np.linspace(0, 10, 1000)

best_sigma = []
best_scores = []
best_stds = []
for set in enumerate(columns):
    print()
    print('Working on Set: ', set)
    scale_outputs = StandardScaler()
    Y = scale_outputs.fit_transform(DF[set[1]].to_numpy().reshape((-1, 1)))

    scores = []
    std = []
    for sig in sigma:
        score_avg = []
        for train_index, test_index in kf.split(S, Y.ravel()):
            # Split data into Train and Test sets
            xtrain, xtest = S[train_index], S[test_index]
            ytrain, ytest = Y[train_index], Y[test_index]

            # Running model
            int = RBFInterpolant(xtrain, ytrain.ravel(), sigma=sig, phi='phs2', order=1)
            ypred = int(xtest)

            # Scoring model
            score = r2_score(ytest, ypred)
            score_avg.append(score)
            # Computing and storing final score of CV test for jth sigma
        scores.append(np.array(score_avg).mean())
        std.append(np.array(score_avg).std())
    best_sigma.append(sigma[np.argmax(scores)])
    best_scores.append(np.max(scores))
    best_stds.append(std[np.argmax(scores)])

best_sigma, best_scores, best_stds, rows = np.array(best_sigma).reshape((-1, 1)), \
                                              np.array(best_scores).reshape((-1, 1)), \
                                              np.array(best_stds).reshape((-1, 1)) / np.sqrt(kf.get_n_splits()), \
                                              np.array(['Opt. Sigma', r'$R^2$ Score', 'Std. Error']).reshape((-1, 1))

results = np.concatenate([best_sigma, best_scores, best_stds], axis=1).T
results = np.concatenate([rows, results], axis=1)
DF_columns = np.concatenate([[''], columns])
results = pd.DataFrame(data=results, columns=[DF_columns])

if select == 1:
    results.to_csv(directory + 'C8_CVresults.csv')
    print(results)
    print('\nC8 Results saved.')
elif select == 2:
    results.to_csv(directory + 'C18_CVresults.csv')
    print(results)
    print('\nC18 Results saved.')
else:
    print()
    print('Work done on other DF, results not saved'
          '\nPlease make sure to make the appropiate modefications to this script')
    print(results)
