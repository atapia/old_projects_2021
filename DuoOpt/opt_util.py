from scipy.optimize import minimize
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler


def find_optimum(RBF, X_init, Bounds):
    Xmin, Xmax = Bounds

    # Create helper method to facilitate optimisation
    def predict(x, RBF):
        # print('Entering Predict')
        x = x.reshape((1, -1))
        return RBF(x)

    optimum = minimize(predict, X_init, args=(RBF,),
                       bounds=((Xmin[0, 0], Xmax[0, 0]), (Xmin[0, 1], Xmax[0, 1])),
                       options={
                           #'disp':True,
                           'gtol': 1e-12}
                       )
    print('Successful Optimisation?: ', optimum.success)
    return optimum.x.reshape((1, -1))


def prepare_data(DataFrame, column, TypeOptimisation='max'):
    if TypeOptimisation == 'max':
        # We are performing maximisation
        tp = -1
    elif TypeOptimisation== 'min':
        # We are performing minimisation
        tp = 1

    # Define the Scaler Instance
    scale_F = StandardScaler()

    # Select data
    F = scale_F.fit_transform((tp * DataFrame[column]).to_numpy().reshape((-1, 1)))

    return F, scale_F

def build_epsilon(functions, guesses, bounds, n=10):

    # Define the number of dimensions we must compute an epsilon value
    k = len(functions)

    optimum = np.zeros((k, 2))
    epsilon = []
    for i, fi, guess_i in zip(range(k), functions, guesses):
        optimum[i] = find_optimum(fi, guess_i, bounds)
        print(optimum[i])
        if i > 0:
            F1_eval = fi(optimum[0].reshape((1, -1)))
            Fi_eval = fi(optimum[i].reshape((1, -1)))
            epsilon.append(np.linspace(Fi_eval, F1_eval, n))

    return np.reshape(np.meshgrid(*epsilon), (k-1, n**3)).T

