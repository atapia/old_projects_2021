# General Packages
import numpy as np
import pandas as pd
import time

# Model Packages
from sklearn.preprocessing import StandardScaler
from rbf.interpolate import RBFInterpolant

# Optimisation
from scipy.optimize import minimize
from opt_util import find_optimum, prepare_data, build_epsilon

# Plotting
import matplotlib.pyplot as plt
from plot_util import data_surface

# Measure excution time
begin_time = time.time()

directory = './Data/'

########################################################################################################################
# Import Data Section and Data Manipulators

# Import Data
C8 = pd.read_csv(directory + 'C8.csv')
C18 = pd.read_csv(directory + 'C18.csv')
columns = list(C8.columns[2:])
inputs = C8[['Temperature', 'Base:acid']].to_numpy()

# Import CV Results
C8_sigma = pd.read_csv(directory + 'C8_CVresults.csv')
C18_sigma = pd.read_csv(directory + 'C18_CVresults.csv')

# Define 4 dataScale instances for each response variable,
# we will denote Intensities as F1 and F2 and IQR as F3 and F4 respectively from each data set
# Furthermore we will define additional constraint response variable C1 and C2 for peak position
scale_inputs = StandardScaler()
F1_data, scale_F1 = prepare_data(C8, columns[1])
F2_data, scale_F2 = prepare_data(C18, columns[1])
F3_data, scale_F3 = prepare_data(C8, columns[-1], TypeOptimisation='min')
F4_data, scale_F4 = prepare_data(C18, columns[-1], TypeOptimisation='min')
C1_data, scale_C1 = prepare_data(C8, columns[0], TypeOptimisation='min')
C2_data, scale_C2 = prepare_data(C18, columns[0], TypeOptimisation='min')
scalers = [scale_F1, scale_F2, scale_F3, scale_F4, scale_C1, scale_C2]

# Select the optimal model parameters
sigma_F1 = C8_sigma[columns[1]].to_numpy()[0]
sigma_F2 = C18_sigma[columns[1]].to_numpy()[0]
sigma_F3 = C8_sigma[columns[-1]].to_numpy()[0]
sigma_F4 = C18_sigma[columns[-1]].to_numpy()[0]
sigma_C1 = C8_sigma[columns[0]].to_numpy()[0]
sigma_C2 = C18_sigma[columns[0]].to_numpy()[0]

# Scale inputs
inputs = scale_inputs.fit_transform(inputs)

########################################################################################################################
# Create Model Instances

# We create the instances of the Fitting model (in this case we use TPS, for speed). These effectively
# are the functions we wish to optimise
F1 = RBFInterpolant(inputs, F1_data.ravel(), sigma=sigma_F1, phi='phs2', order=1)
F2 = RBFInterpolant(inputs, F2_data.ravel(), sigma=sigma_F2, phi='phs2', order=1)
F3 = RBFInterpolant(inputs, F3_data.ravel(), sigma=sigma_F3, phi='phs2', order=1)
F4 = RBFInterpolant(inputs, F4_data.ravel(), sigma=sigma_F4, phi='phs2', order=1)
C1 = RBFInterpolant(inputs, C1_data.ravel(), sigma=sigma_C1, phi='phs2', order=1)
C2 = RBFInterpolant(inputs, C2_data.ravel(), sigma=sigma_C2, phi='phs2', order=1)

# Create helper method to facilitate optimisation
def predict(x, RBF):
    # print('Entering Predict')
    x = x.reshape((1, -1))
    return RBF(x)

########################################################################################################################
# Define necessary variables to run optimisations

# Define Bounds of Optimisation
# Note that we must rescale them in order for the models to be able to run
Xmin = scale_inputs.transform(np.array([80.0001, 0.600001]).reshape((1, -1)))
Xmax = scale_inputs.transform(np.array([189.99999, 1.099999]).reshape((1, -1)))

# Define the initial guess for the optimisation based on the shape of the countour plot
F1_init = scale_inputs.transform(np.array([185, 0.75]).reshape((1, -1)))
F2_init = scale_inputs.transform(np.array([145, 1.05]).reshape((1, -1)))
F3_init = scale_inputs.transform(np.array([150, 1.05]).reshape((1, -1)))
F4_init = scale_inputs.transform(np.array([140, 1.05]).reshape((1, -1)))

# Building the values of Epsilon that we will set the constraints for the optimisation
print()
print('Running build_epsilon method:\n')
n = 5
epsilon = build_epsilon([F1, F2, F3, F4], [F1_init, F2_init, F3_init, F4_init], [Xmin, Xmax], n=n)

########################################################################################################################
# Running optimisation
# now that we have a vector of epsilon values that we can contraints the optimisation of F1
# we could perform the same process for all response variables
# This means that in this case we are only optimising F1 with constraints defined by F2, F3 and F4 but we could do
# the same for the rest

# Redefined the optimisation procedure as a method to facilitate its reimplementation for each guess
def epsilon_constrain(guess):
    X_sol_vec = np.zeros((np.size(epsilon, 0), 2))
    k = 0
    for eps in epsilon:

        #print([scale_F2.inverse_transform(eps[0].reshape((1, -1))),
        #       scale_F3.inverse_transform(eps[1].reshape((1, -1))),
        #       scale_F4.inverse_transform(eps[2].reshape((1, -1)))])

        # We define the constraints as a function such that c(x) >= 0
        # In our case we want Fi(x) <= Ei -----> -Fi(x) + E >= 0
        def constrain_F2(x):
            x = x.reshape((1, -1))
            return - F2(x) + eps[0]

        def constrain_F3(x):
            x = x.reshape((1, -1))
            return - F3(x) + eps[1]

        def constrain_F4(x):
            x = x.reshape((1, -1))
            return - F4(x) + eps[2]

        def constrain_C1(x):
            x = x.reshape((1, -1))
            return - C1(x) + scale_C1.transform([[510]])[0]

        def constrain_C2(x):
            x = x.reshape((1, -1))
            return - C2(x) + scale_C2.transform([[510]])[0]

        constraints = (
                        {'type': 'ineq', 'fun': constrain_F2},
                        {'type': 'ineq', 'fun': constrain_F3},
                        {'type': 'ineq', 'fun': constrain_F4},
                        {'type': 'ineq', 'fun': constrain_C1},
                        {'type': 'ineq', 'fun': constrain_C2}
        )

        # Call the optimizer and initialize with function to be optimized and its constraints
        solution = minimize(predict, guess, args=(F2,), constraints=constraints,
                                bounds=((Xmin[0, 0], Xmax[0, 0]), (Xmin[0, 1], Xmax[0, 1])),
                                options={
                                    #'disp':True,
                                    #'gtol': 1e-12
                                }
                                )

        # Filter to remove unsuccessful runs
        #print(solution.success)
        if solution.success:
            #print(solution.x)
            X_sol_vec[k] = solution.x
        else:
            X_sol_vec[k] = [500, 500]
        k += 1

    return X_sol_vec.reshape((-1, 2))


# I noticed that the problem was giving different answers based on the initial guess so I tried running
# it multiple times with a different initial guess for each run. The I filtered the unsuccessful runs.
# But most of these guesses gave very similar results, so the problem gives consistent solutions actually.
X_F1 = find_optimum(F1, F1_init, [Xmin, Xmax])
X_F2 = find_optimum(F2, F2_init, [Xmin, Xmax])
X_F3 = find_optimum(F3, F3_init, [Xmin, Xmax])
X_F4 = find_optimum(F4, F4_init, [Xmin, Xmax])

Xs = [X_F1, X_F2, X_F3, X_F4]
print(scale_inputs.inverse_transform(Xs))

sol_vec = []
for guess in Xs:
    print()
    sol_vec.append(epsilon_constrain(guess))

# Filtering the unsuccessful solutions
set1, set2, set3, set4 = sol_vec

#print(scale_inputs.inverse_transform(set2[set2 != [500, 500]].reshape((-1, 2))))
set1 = scale_inputs.inverse_transform(set1[set1 != [500, 500]].reshape((-1, 2)))
print('Number of failed SOOPs: ', (n**3 - np.size(set2[set2 != [500, 500]].reshape((-1, 2)), 0)))
set2 = scale_inputs.inverse_transform(set2[set2 != [500, 500]].reshape((-1, 2)))
set3 = scale_inputs.inverse_transform(set3[set3 != [500, 500]].reshape((-1, 2)))
set4 = scale_inputs.inverse_transform(set4[set4 != [500, 500]].reshape((-1, 2)))

set2 = np.vstack([set1, set2, set3, set4])

set2 = np.around(set2[np.argsort(set2[:, 1])], 2)
set2 = np.unique(set2, axis=0)
print('Number of unique solutions: ', np.size(set2, 0))




########################################################################################################################
# Plotting found Solutions onto map to see position relative to response variables

# Create the linear space to generate surface
n = 100
T_pred = np.linspace(90, 190, n).reshape((-1, 1))
X_pred = np.linspace(.6, 1.1, n).reshape((-1, 1))
S_pred = np.reshape(np.meshgrid(T_pred, X_pred), (2, n*n)).T
xx, yy = np.meshgrid(T_pred, X_pred)

# Scale input map
S_pred = scale_inputs.transform(S_pred)

fig = plt.figure(figsize=(20, 5))
ax1 = fig.add_subplot(1, 4, 1)
ax2 = fig.add_subplot(1, 4, 2)
ax3 = fig.add_subplot(1, 4, 3)
ax4 = fig.add_subplot(1, 4, 4)
axs = (ax1, ax2, ax3, ax4)

for axis, Fi, scale_i, dataSet in zip(axs, [F1, F2, F3, F4], [scale_F1, scale_F2, scale_F3, scale_F4], ['Intensity C8', 'Intensity C18', 'IQR C8', 'IQR C18']):

    # Running TPS
    Y_pred = Fi(S_pred)
    Y_pred = scale_i.inverse_transform(Y_pred)
    # Turn Y_pred into grid
    dataGrid = Y_pred.reshape((n, n))

    # Treatment of maximized variables (must multiply by -1 since in this script we run all as minimisation)
    if (Fi == F1) or (Fi == F2):
        dataGrid = - dataGrid

    # Plotting surface
    data_surface(xx, yy, dataGrid, dataSet, axis, fig)

    axis.scatter(set3[:, 0], set3[:, 1], marker='*', color='g', s=120)
    axis.scatter(set4[:, 0], set4[:, 1], marker='*', color='b', s=200)
    axis.scatter(set2[:, 0], set2[:, 1], marker='*', color='k')

    axis.axis([90, 190, 0.6, 1.1])

# General formatting
plt.subplots_adjust(left=0.05, bottom=0.06, right=0.97, top=0.80, wspace=0.2, hspace=0.08)
fig.tight_layout()
fig.savefig('./Graphs/Epsilon_4variables')

##############################################################################################################
# Saving Candidate solution into a dataFrame in order to analyze in following script
# We choose set 2, since this one offered the most candidate solutions (three graphically distinct ones)

# Here we compute the values of the response functions at the candidate solutions
set2 = scale_inputs.transform(set2)
F1_sol, F2_sol, F3_sol, F4_sol, C1_sol, C2_sol = F1(set2), F2(set2), F3(set2), F4(set2), C1(set2), C2(set2)
Solutions = [F1_sol, F2_sol, F3_sol, F4_sol, C1_sol, C2_sol]
for i, scale in enumerate(scalers):
    Solutions[i] = scale.inverse_transform(Solutions[i])

# Compute maximums
M_F1, M_F2, M_F3, M_F4 = F1(X_F1), F2(X_F2), F3(X_F3), F4(X_F4)
Maxima = [M_F1, M_F2, M_F3, M_F4]
for i, scale in enumerate(scalers[0:4]):
    Maxima[i] = scale.inverse_transform(Maxima[i])

set2 = scale_inputs.inverse_transform(set2)
# Assign solutions to a dataFrame
df = pd.DataFrame({
    'Temperature (°C)': list(set2[:, 0]),
    'Base:Acid Ratio': list(set2[:, 1]),
    'Intensity C8': list(-Solutions[0]),
    'Intensity C18': list(-Solutions[1]),
    'IQR C8': list(Solutions[2]),
    'IQR C18': list(Solutions[3]),
    'Peak Pos. C8': list(Solutions[4]),
    'Peak Pos. C18': list(Solutions[5])
})

df2 = pd.DataFrame([[0, 0, -Maxima[0][0], -Maxima[1][0], Maxima[2][0], Maxima[3][0], 510, 510]], columns=list(df.columns.values))
df = pd.concat([df, df2], axis=0, ignore_index=True)
df.to_csv('./Data/epsilon_4variables.csv', index=True)

print()
print('Execution time: %.2f seconds' % (time.time()-begin_time))
plt.show()
