import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.axis
import matplotlib.pyplot as plt
from scipy.optimize import minimize
import pandas as pd
import os


def meniscus(t, w, p):
    """
    Here we define the ODE system, since we are working with a 2nd order ODE, it was split
    into an extra variable to turn the problem into a system of 1st order ODEs.

    :return: return the value of the ode
    :param w: vector of state variables
                    w = [x, y] (in this case x = H, y = H')
    :param t:  time (in this case x distance)
    :param p: vector of parameters p = [rho, g_constant, gamma]
    """
    # Define Variables and Parameters:
    x, y = w
    rho, g_const, gamma = p
    l = np.sqrt(gamma / (rho * g_const))
    Bo = rho * g_const * gamma * l ** 2

    # Define the ODE
    f = [y, Bo * x * (1 + y ** 2) ** 1.5]

    return f


def ode_solv(theta, H0, X):
    """

    :param theta:
    :param H0:
    :param X:
    :return:
    """
    # Parameter Values
    rho = 1
    g = 1
    gamma = 1

    # Pack Parameters:
    p = [rho, g, gamma]

    # Initial Conditions
    x0 = H0
    y0 = - np.cos(np.deg2rad(theta)) / np.sin(np.deg2rad(theta))

    # Pack IC
    w0 = [x0, y0]

    # Time
    tf = [0, X]

    ode_sol = solve_ivp(meniscus, tf, w0, method='DOP853', args=(p,), max_step=X / 1000)
    x = ode_sol.t
    y = ode_sol.y

    return x, y, ode_sol.status


# Import Conditions
Xmax = [3, 5, 10]


def shooting(H0, theta0, direction):
    # We define the absolute error of the ODE solver since we know that
    # for [H, dH](Xmax) = 0
    #
    #
    # H0 is a known initial solution (ie start from H0 = 0 for Theta = 90)
    # Theta is the associated angle to H0 (but we will employed to calculate the next solution)
    # step is the deviation from theta0, ie the next solution
    # direction (-1 or 1) determines whether we are going from 90---->0 or 90---->180,
    # since we now that from previous trials that the initial solution H0 is monotonically decreasing as a function of
    # theta

    precision = 30  # we define with these how many extra points will be calculated for a guess H0 (see below in H_vec)

    # Checking valid directional input
    if direction == -1:
        print('Computing in range \u03B8 = (90, 180)')
    elif direction == 1:
        print('Computing in range \u03B8 = (0, 90)')
    else:
        return print('Direction input invalid! Choose -1 for \u03B8 > 90, or 1 for \u03B8 < 90')

    # Here we defined an error function that calculates the total sum of the squared error. Since we know that at Xmax
    # both H(Xmax) and dH(Xmax) should be zero. Then the error is simply the value of the solution at the
    # final position
    def abs_r(guess, theta):
        _, e, b = ode_solv(theta, guess, Xmax[0])
        if b == 0:
            return e[0, np.size(e, 0) - 1] ** 2 + e[1, np.size(e, 0) - 1] ** 2
        else:
            return 1e6

    H_vec = np.linspace(H0, H0 + direction * 0.01, precision)
    error_vec = np.zeros([len(H_vec), 1])

    for i in np.arange(len(H_vec)):
        error_vec[i] = abs_r(H_vec[i], theta0)

    return H_vec[np.argmin(error_vec, axis=0)]


####################################################################################

# Part 1.4

# Here I define the script part to calculate the solutions of H0 based on the SECOND method I tried

def part4_comp():
    ss = 0.25
    theta_under90 = np.arange(ss, 90, ss)
    Hsol = np.zeros(len(theta_under90))

    solutions2 = open('H0_solutions_under90_method2.csv', 'w')
    solutions2.write('{:.2f},{:.8f}\n'.format(90, Hsol[0]))

    for i in np.arange(len(theta_under90) - 1):
        print(90 - theta_under90[i])
        Hsol[i + 1] = shooting(Hsol[i], 90 - theta_under90[i], 1)
        solutions2.write('{:.2f},{:.8f}\n'.format(90 - theta_under90[i], Hsol[i + 1]))

    solutions2.close()
    solutions = open('H0_solutions_over90_method2.csv', 'w')
    solutions.write('{:.2f},{:.8f}\n'.format(90, Hsol[0]))

    theta_over90 = theta_under90

    for i in np.arange(len(theta_over90) - 1):
        print(90 + theta_over90[i])
        Hsol[i + 1] = shooting(Hsol[i], 90 + theta_over90[i], -1)
        solutions.write('{:.2f},{:.8f}\n'.format(90 + theta_over90[i], Hsol[i + 1]))
    solutions.close()

# This method below will plot the data written by the method above

def plot_data():

    data = []
    for file in os.listdir('./'):
        if file.endswith('method2.csv'):
            sols = pd.read_csv(file, dtype=np.float64).T
            sols = sols.to_numpy()
            data.append(sols)

    print(np.shape(data))
    data = np.array(data)

    # Sampled Angles
    x_axis = np.concatenate((np.flip(data[1, 0, :], 0), data[0, 0, :]), 0)

    # H0 Solutions
    y_axis = np.concatenate((np.flip(data[1, 1, :], 0), data[0, 1, :]), 0)

    # Creating Figure
    fig3 = plt.figure(3)
    ax3 = plt.subplot(111)

    # Formatting axis
    ax3.axis([0, 180, -2.2, 2.2])

    # Title and Labels
    ax3.set_ylabel('H0 Solution')
    ax3.set_xlabel(' Angle \u03B8')
    ax3.set_title('H0(\u03B8)')

    # Plotting the data
    ax3.plot(x_axis.reshape((-1, 1)), y_axis.reshape((-1, 1)), linewidth=0.7)

    fig3.savefig('./Graphics/' + 'Q1_p4_method2')

####################################################################################

###

# COMMENTED TO AVOID TIME HEAVY PROCESS

#part4_comp()

###
plot_data()


