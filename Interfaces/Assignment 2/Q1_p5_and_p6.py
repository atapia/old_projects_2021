import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.axis
import matplotlib.pyplot as plt
from scipy.optimize import minimize
import pandas as pd
import os


def meniscus(t, w, p):
    """
    Here we define the ODE system, since we are working with a 2nd order ODE, it was split
    into an extra variable to turn the problem into a system of 1st order ODEs.

    :return: return the value of the ode
    :param w: vector of state variables
                    w = [x, y] (in this case x = H, y = H')
    :param t:  time (in this case x distance)
    :param p: vector of parameters p = [rho, g_constant, gamma]
    """
    # Define Variables and Parameters:
    x, y = w
    rho, g_const, gamma = p
    l = np.sqrt(gamma / (rho * g_const))
    Bo = rho * g_const * gamma * l ** 2

    # Define the ODE
    f = [y, Bo * x * (1 + y ** 2) ** 1.5]

    return f


def ode_solv(theta, H0, X):
    """

    :param theta:
    :param H0:
    :param X:
    :return:
    """
    # Parameter Values
    rho = 1
    g = 1
    gamma = 1

    # Pack Parameters:
    p = [rho, g, gamma]

    # Initial Conditions
    x0 = H0
    y0 = - np.cos(np.deg2rad(theta)) / np.sin(np.deg2rad(theta))

    # Pack IC
    w0 = [x0, y0]

    # Time
    tf = [0, X]
    # Evaluation step
    t_ev = np.linspace(0, X, 120)

    # options = {'rtol': 0.0001, 'atol': 1e-8}
    ode_sol = solve_ivp(meniscus, tf, w0, method='RK45', args=(p,), t_eval=t_ev)
    x = ode_sol.t
    y = ode_sol.y

    return x, y, ode_sol.status


def part6_approx(X, theta):
    return (np.pi - theta) * np.exp(-X)


# Import Conditions
Xmax = [3, 5, 10]


def shooting(H0, theta0, direction):
    # We define the absolute error of the ODE solver since we know that
    # for [H, dH](Xmax) = 0
    #
    #
    # H0 is a known initial solution (ie start from H0 = 0 for Theta = 90)
    # Theta is the associated angle to H0 (but we will employed to calculate the next solution)
    # step is the deviation from theta0, ie the next solution
    # direction (-1 or 1) determines whether we are going from 90---->0 or 90---->180,
    # since we now that from previous trials that the initial solution H0 is monotonically decreasing as a function of
    # theta

    precision = 50  # we define with these how many extra points will be calculated for a guess H0 (see below in H_vec)

    # Checking valid directional input
    if direction == -1:
        print('Computing in range \u03B8 = (90, 180)')
    elif direction == 1:
        print('Computing in range \u03B8 = (0, 90)')
    else:
        return print('Direction input invalid! Choose -1 for \u03B8 > 90, or 1 for \u03B8 < 90')

    # Here we defined an error function that calculates the total sum of the squared error. Since we know that at Xmax
    # both H(Xmax) and dH(Xmax) should be zero. Then the error is simply the value of the solution at the
    # final position
    def abs_r(guess, theta):
        _, e, b = ode_solv(theta, guess, Xmax[0])
        if b == 0:
            return e[0, np.size(e, 0) - 1] ** 2 + e[1, np.size(e, 0) - 1] ** 2
        else:
            return 1e6

    H_vec = np.linspace(H0, H0 + direction * 0.1, precision)
    error_vec = np.zeros([len(H_vec), 1], dtype=np.float64)

    for i in np.arange(len(H_vec)):
        error_vec[i] = abs_r(H_vec[i], theta0)

    return H_vec[np.argmin(error_vec, axis=0)]


def plot_part_5and6():

    ####################################################################################

    # Part 1.5

    # Reinstate Conditions
    Xmax = np.array([3, 5, 10])
    theta = np.array([20, 70, 150])

    # We import the 'accurate' initial solutions from the previous exercise for each theta

    # Solutions found by Minimization algorithm (Nelder-Mead)
    Hinit = [0.62285097, 0.02114172, -0.35872984]

    # Solutions found by Second Method (H0 vector)
    Hinit = np.array([1.08474747, 0.26050505, -0.94414141])

    # Working solutions found manually, modifiying values based on the previous solution found from method 2
    Hinit = np.array([1.1474747, 0.3550505, -.99314141])

    H_axis = []
    X_axis = []

    # Call the ODE solver to find our integrated solution
    for i in np.arange(len(theta)):
        x, y, b = ode_solv(theta[i], Hinit[i], Xmax[0])
        print('Solver Status: ', b)
        H_axis.append(y[0, :])
        X_axis.append(x)

    print(X_axis)

    # Generate solutions based on approximated expression Part 1.6

    X_vec = np.linspace(0, Xmax[0], 100)
    H_20 = part6_approx(X_vec, np.deg2rad(20))
    H_70 = part6_approx(X_vec, np.deg2rad(70))

    fig4 = plt.figure(4)
    ax4 = plt.subplot(111)

    # Plot solutions found by my method
    ax4.plot(X_axis[0], H_axis[0], color='b', linewidth=0.85)
    ax4.plot(X_axis[1], H_axis[1], color='r', linewidth=0.85)
    ax4.plot(X_axis[2], H_axis[2], color='g', linewidth=0.85)

    # Plot solutions according to part 1.6
    line1, = ax4.plot(X_vec, H_20, linestyle='-.', color='b', linewidth=0.85)
    line2, = ax4.plot(X_vec, H_70, linestyle='-.', color='r', linewidth=0.85)

    # Title and Labels
    ax4.set_ylabel('H(Y)')
    ax4.set_xlabel('Y')
    ax4.set_title('Meniscus Profile')
    plt.legend(['20°', '70°', '150°',
                'Part 6: 20°', 'Part 6: 70°'
                ])

    fig4.savefig('./Graphics/' + 'Q1_p6')


plot_part_5and6()
