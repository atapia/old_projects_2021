import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.axis
import matplotlib.pyplot as plt
from scipy.optimize import minimize
import pandas as pd
import os


# Andres Rocha Tapia
# 23.03.2020
# Question 1.2: Define function f (here ode_solv)

def meniscus(t, w, p):
    """
    Here we define the ODE system, since we are working with a 2nd order ODE, it was split
    into an extra variable to turn the problem into a system of 1st order ODEs.

    :return: return the value of the ode
    :param w: vector of state variables
                    w = [x, y] (in this case x = H, y = H')
    :param t:  time (in this case x distance)
    :param p: vector of parameters p = [rho, g_constant, gamma]
    """
    # Define Variables and Parameters:
    x, y = w
    rho, g_const, gamma = p
    l = np.sqrt(gamma / (rho * g_const))
    Bo = rho * g_const * gamma * l ** 2

    # Define the ODE
    f = [y, Bo * x * (1 + y ** 2) ** 1.5]

    return f


def ode_solv(theta, H0, X):
    """

    :param theta:
    :param H0:
    :param X:
    :return:
    """
    # Parameter Values
    rho = 1
    g = 1
    gamma = 1

    # Pack Parameters:
    p = [rho, g, gamma]

    # Initial Conditions
    x0 = H0
    y0 = - np.cos(np.deg2rad(theta)) / np.sin(np.deg2rad(theta))

    # Pack IC
    w0 = [x0, y0]

    # Time span
    tf = [0, X]

    # options = {'rtol': 0.0001, 'atol': 1e-8}
    ode_sol = solve_ivp(meniscus, tf, w0, method='DOP853', args=(p,), max_step=X/1000)
    x = ode_sol.t
    y = ode_sol.y

    return x, y, ode_sol.status


####################################################################################

# Part 1.3

# Define 'infinity' approximation
Xmax = 3
# Vector H0 to be inserted into f function
H_vec = np.linspace(0, 1, 26)

# Vectors to store solutions
H_res_3 = np.zeros([np.size(H_vec, 0), 1])
dH_res_3 = np.zeros([np.size(H_vec, 0), 1])

fig = plt.figure(1)
ax1 = plt.subplot(121)
ax2 = plt.subplot(122)

# Formatting axis H
ax1.axis([0, Xmax, -2, 2])
ax2.axis([0, Xmax, -2, 2])

# Title and Labels
ax1.set_ylabel('H')
ax1.set_xlabel('Y')
ax2.set_ylabel('dH')
ax2.set_xlabel('Y')
ax1.set_title('H(Y) for \u03B8 = 45\u00B0')
ax2.set_title('dH(Y) for \u03B8 = 45\u00B0')

# We plot here the solutions of the ODE solver for (H, dH) and
# and at the same time we collect the solution for Y=Xmax
for i in np.arange(len(H_vec)):
    x, y, _ = ode_solv(45, H_vec[i], Xmax)
    line1, = ax1.plot(x, y[0, :])
    line2, = ax2.plot(x, y[1, :])
    # Store last result for Y=3
    H_res_3[i] = y[0, np.size(y, 1) - 1]
    dH_res_3[i] = y[1, np.size(y, 1) - 1]

fig.tight_layout()

fig2 = plt.figure(2)
ax1 = plt.subplot(121)
ax2 = plt.subplot(122)

# Formatting axis H
ax1.axis([0 , 1, -2, 2])
ax1.set_xticks(np.arange(0, 1.25, 0.25))
ax1.set_yticks(np.arange(-2, 2., 0.5))

# Formatting axis dH
ax2.axis([0, 1, -2e7, 0.75e7])
ax2.set_xticks(np.arange(0, 1.25, 0.25))
ax2.set_yticks(np.arange(-2.e7, 1.25e7, .25e7))

# Title and Labels
ax1.set_title('H(Y=3) for \u03B8 = 45\u00B0')
ax2.set_title('dH(Y=3) for \u03B8 = 45\u00B0')
ax1.set_ylabel('ODE H(Y=3)')
ax1.set_xlabel('H0')
ax2.set_ylabel('ODE dH(Y=3)')
ax2.set_xlabel('H0')
fig2.tight_layout()

# Plotting values for [H, dH](Y=3)
line1, = ax1.plot(H_vec, H_res_3)
linex, = ax1.plot(np.array([0, 1]), np.array([0, 0]), color='r', linewidth=0.7)
line2, = ax2.plot(H_vec, dH_res_3)
liney, = ax2.plot([0, 1], [0, 0], color='r' , linewidth=0.7)

fig.savefig('./Graphics/' + 'Q1_p3_1')
fig2.savefig('./Graphics/' + 'Q1_p3_2')
