import numpy as np
import matplotlib.pyplot as plt

###########################################################################################

# Case 1:

L = 1  # length of the cone
Vm = 1  # molar volume


def G_homo(n):
    return (Vm * -n ** 2 + 4 * np.pi * (Vm * n * 0.75 / np.pi) ** (2 / 3))


n = np.linspace(0, 10, 1000)
dG_h = G_homo(n)


# Case i)

def G_case1(n, a):
    return G_homo(n) * 0.5 * (1 - np.cos(np.deg2rad(a / 2)))


dG_50 = G_case1(n, 50)
dG_90 = G_case1(n, 90)
dG_160 = G_case1(n, 160)

fig = plt.figure(1)
ax = fig.add_subplot(111)
ax.plot(n, dG_h)
ax.plot(n, dG_50)
ax.plot(n, dG_90)
ax.plot(n, dG_160)

ax.axis([0, 6, -2, np.max(dG_h) * 1.2])

xmin, xmax = ax.get_xlim()
ymin, ymax = ax.get_ylim()

# removing the default axis on all sides:
for side in ['bottom', 'right', 'top', 'left']:
    ax.spines[side].set_visible(False)

# get width and height of axes object to compute
# matching arrowhead length and width
dps = fig.dpi_scale_trans.inverted()
bbox = ax.get_window_extent().transformed(dps)
width, height = bbox.width, bbox.height

# manual arrowhead width and length
hw = 1. / 20. * (ymax - ymin)
hl = 1. / 20. * (xmax - xmin)
lw = 0.8  # axis line width
ohg = 0.1  # arrow overhang

# compute matching arrowhead length and width
yhw = hw / (ymax - ymin) * (xmax - xmin) * height / width
yhl = hl / (xmax - xmin) * (ymax - ymin) * width / height

# draw x and y axis
ax.arrow(xmin, ymin, xmax - xmin, 0., fc='k', ec='k', lw=lw,
         head_width=hw, head_length=hl, overhang=ohg,
         length_includes_head=True, clip_on=False)

ax.arrow(0, ymin, 0., ymax - ymin, fc='k', ec='k', lw=lw,
         head_width=yhw, head_length=yhl, overhang=ohg,
         length_includes_head=True, clip_on=False)

ax.set_xticks([])
ax.set_yticks([0])
plt.legend(['dG(Homo)', '\u03B1: 50°', '\u03B1: 90°', '\u03B1: 160°'])
fig.savefig('./Graphics/' + 'Q2_p3_case1')

###########################################################################################

# Case 3:


def G_case3(n, o):
    return G_homo(n) * (0.25 * (2-3*np.cos(np.deg2rad(o))+np.cos(np.deg2rad(o))**3))

dG_case1 = G_case1(n, 50)
dG_case3 = G_case3(n, 90)

fig2 = plt.figure(2)
ax = fig2.add_subplot(111)
ax.plot(n, dG_h)
ax.plot(n, dG_case1)
ax.plot(n, dG_case3)

ax.axis([0, 6, -2, np.max(dG_h) * 1.2])

xmin, xmax = ax.get_xlim()
ymin, ymax = ax.get_ylim()

# removing the default axis on all sides:
for side in ['bottom', 'right', 'top', 'left']:
    ax.spines[side].set_visible(False)

# get width and height of axes object to compute
# matching arrowhead length and width
dps = fig.dpi_scale_trans.inverted()
bbox = ax.get_window_extent().transformed(dps)
width, height = bbox.width, bbox.height

# manual arrowhead width and length
hw = 1. / 20. * (ymax - ymin)
hl = 1. / 20. * (xmax - xmin)
lw = 0.8  # axis line width
ohg = 0.1  # arrow overhang

# compute matching arrowhead length and width
yhw = hw / (ymax - ymin) * (xmax - xmin) * height / width
yhl = hl / (xmax - xmin) * (ymax - ymin) * width / height

# draw x and y axis
ax.arrow(xmin, 0, xmax - xmin, 0., fc='k', ec='k', lw=lw,
         head_width=hw, head_length=hl, overhang=ohg,
         length_includes_head=True, clip_on=False)

ax.arrow(0, ymin, 0., ymax - ymin, fc='k', ec='k', lw=lw,
         head_width=yhw, head_length=yhl, overhang=ohg,
         length_includes_head=True, clip_on=False)

ax.set_xticks([])
ax.set_yticks([0])
plt.legend(['dG(Homo)', 'Case 1: \u03B1 = 50°, \u03B8 = 90°', 'Case 3: \u03B8 = 90°'])
fig2.savefig('./Graphics/' + 'Q2_p3_case3')

