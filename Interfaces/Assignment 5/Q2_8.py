import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import elementary_charge, Boltzmann, epsilon_0
from scipy.integrate import odeint


kB = Boltzmann
e = elementary_charge
T = 300
beta = e / (kB * T)


def PBE(x, w, p):

    nj0, pj0, epsr = p

    x1, x2 = w
    C = - e / (epsilon_0 * epsr)
    D = e / (kB * T)

    # define ODE
    f = [
        x2,
        C * (nj0*(1 - np.exp(D*x1)) + pj0*(np.exp(-D*x1)-1))
    ]

    return f


def bigF(nj0, pj0, Yj0):

    operand = nj0 * (np.exp(beta * Yj0) - beta * Yj0 - 1) \
              + pj0 * (np.exp(-beta * Yj0) + beta * Yj0 - 1)

    return np.sqrt(operand)


def devY(nj0, pj0, Yj0, epsr):

    C = - np.sqrt((2 * e) / (beta * epsr * epsilon_0))

    return C * bigF(nj0, pj0, Yj0)

n = 100
m = 50

def ode_solv(Yj0, dYj0, nA0, nB0, epsr):

    # Pack Parameters
    p = nA0, nB0, epsr

    # Pack Initial solutions
    w0 = [Yj0, dYj0]

    # time span
    if Yj0 < 0:
        t_eval = - np.linspace(0, m, 1000) * 1e-6
    else:
        t_eval = np.linspace(0, n, 1000) * 1e-6

    sol = odeint(PBE, w0, t_eval, args=(p,), tfirst=True, printmessg=True)

    return sol[:, 0]

# Intrinsic Carrier Densities
n_iA = 2.1e3
n_iB = 2.1e6

# Molbile Carrier Densites
nA0 = 1e18
nB0 = 1e11
pA0 = n_iA ** 2 / nA0
pB0 = n_iB ** 2 / nB0

# Permitivities
eA = 12.9
eB = 12.0

# Initial Conditions (Obtained from previous Script) -0.23737038559579837 0.47289390772751916
YA0 = -0.23737038559579837
dYA0 = devY(nA0, pA0, YA0, eA)
#print(YA0, dYA0)
YB0 = 0.47289390772751916
dYB0 = devY(nB0, pB0, YB0, eB)
#print(YB0, dYB0)

# Compute potential profile
YA_vec = ode_solv(YA0, dYA0, nA0, pA0, eA)
YB_vec = ode_solv(YB0, dYB0, nB0, pB0, eB)

xb = np.linspace(0, n, 1000) * 1e-6
xa = np.linspace(0, m, 1000) * 1e-6
# Compute Bands
ECB = - np.array(YB_vec) + kB * T * np.log(nB0/n_iB)/e
ECA = - np.array(YA_vec) + kB * T * np.log(nA0/n_iB)/e

# Plot results
fig, ax = plt.subplots(1, 2, figsize=(10, 5))

ax[0].plot(xb, YB_vec + YA0, 'r')
ax[0].plot(-xa, YA_vec + YB0, 'r')
ax[0].axvline(0, color='k', lw=0.9)
ax[0].ticklabel_format(axis='x', style='sci', scilimits=(2, 0.1))
ax[0].set_xlabel(r'$x$')
ax[0].set_ylabel(r'$\psi (x)$')

ax[1].plot(xb, ECB, 'red')
ax[1].plot(-xa, ECA, 'blue')
ax[1].axhline(0, linestyle='--', color='0.5')
ax[1].axvline(0, color='k', lw=0.9)
ax[1].ticklabel_format(axis='x', style='sci', scilimits=(2, 0.1))
ax[1].set_xlabel(r'$x$')
ax[1].set_ylabel(r'$E_{c} (x)$')

fig.tight_layout()
fig.savefig('./Graphs/q2_8')
plt.show()
