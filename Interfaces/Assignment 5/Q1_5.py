import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import elementary_charge, Boltzmann, epsilon_0
from scipy.optimize import fsolve

kb = Boltzmann
eps0 = epsilon_0
epsr = 11
e = elementary_charge
T = 300 # K
alpha = 1e19
ni = 1e10

def W_solve(W):

    C = e * alpha / (12 * eps0 * epsr)
    D = kb * T / e

    return (C * (W ** 3)) - D * np.log((alpha**2 * W**2) / (4 * ni**2))

# We use the Bandgap of Si as initial value
Si_eg = 1.12 # eV
W_si = ((e * alpha / (12 * eps0 * epsr))**-1 * Si_eg) ** (1/3)
print(W_si)

# Solve the system to find the
W_sol = fsolve(W_solve, W_si, full_output=True)
print(W_sol)
W = W_sol[0]
X = np.linspace(-W/2, W/2, 1000).reshape((-1, 1))

# Define the potential profile solution found in Q1.3
def Psi(x):
    C = e * alpha / (2 * epsr * eps0)
    D = kb * T / e
    phi = D * np.log(alpha**2 * W**2 / (4 * ni**2))

    return phi/2 + C * (x[:]**3/3 - (W/2)**2 * x[:])

# Plot
fig, ax = plt.subplots(figsize=(5, 5))
ax.plot(X, Psi(X), 'r', lw=1.2)
ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
ax.axis([-W/2, W/2, -0.01, 0.7])
ax.set_ylabel(r'$\psi (x)$ [V]')
ax.set_xlabel(r'$x$ [cm]')
plt.show()
fig.savefig('./Graphs/q1_5')
