import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import elementary_charge, Boltzmann, epsilon_0, Planck, speed_of_light
from scipy.optimize import minimize
from scipy.integrate import simps

h = Planck
eps0 = epsilon_0
kB = Boltzmann
e = elementary_charge
c = speed_of_light

def BlackBody(v, T=6600):

    return (2 * h * (v[:,]**3)) / (c**2) * (np.exp((h*v[:,]) / (kB * T), dtype=np.float64) - 1) ** -1

########################################################################################################################
# Q3.1

v = np.logspace(12, 17, 100000).reshape((-1, 1))
B = BlackBody(v)

fig, ax = plt.subplots(figsize=(5, 5))

ax.semilogx(v, B, 'r', lw=1.2)
ax.axhline(np.max(B), linestyle='--', color='0.5', lw=1.2)
ax.axvline(v[np.argmax(B)], linestyle='--', color='0.5', lw=1.2)
ax.axvline(1.1*e/(h), color='blue', lw=1.2)
ax.fill_between(v[v>=(1.1*e/h)], 0, B[v>=(1.1*e/h)], alpha=0.3, color='orange')
ax.legend([r'$B(\nu,T)$', 'Max B', '_', r'$E_{g,Si}=1.1eV$'])
ax.axis([np.min(v), np.max(v), 0-np.max(B)*0.01, 6e-8])

ticksy = np.concatenate([np.arange(0, 6, 1)*1e-8, np.max(B).ravel()])
ax.set_yticks(ticksy)
ax.ticklabel_format(axis='y', style='sci', scilimits=(2, 0.1))
ax.set_xlabel(r'Frequency $\nu$ [Hz]')
ax.set_ylabel(r'$W sr^{-1} m^{-2} Hz^{-1}$')
fig.tight_layout()
fig.savefig('./Graphs/q3_1')
plt.close()
v_max = v[np.argmax(B)]

print()
print('v_max = %0.3e Hz\nE_photon = %0.3f eV\nwavelength = %0.3f nm' % (v_max, h *v_max/e, c/(v_max * 1e-9)))
print()

########################################################################################################################
# Q3.2

abs_ratio = simps(B[v>=(1.1*e/h)], v[v>=(1.1*e/h)]) / simps(B.ravel(), v.ravel())
print('The percentage of light absorbed is : %0.1f%%' % (abs_ratio*100))
print()

########################################################################################################################
# Q3.4 and 3.5


Eg = 1.1
v_abs = v[v >= (Eg * e / h)].reshape((-1, 1))
def E_v(v):
    return BlackBody(v) * Eg * e  / (h * v)

# Eta
E = E_v(v[v >= (Eg * e / h)])
eta = simps(E.ravel(), v_abs.ravel()) / simps(B.ravel(), v.ravel())
print('The efficiency of silicon as PV is: %.1f%%' % (eta*100))

# Variable BangGap

Eg_vec = np.linspace(0, 6, 100)
eta_vec = np.zeros_like(Eg_vec)

for i in np.arange(len(Eg_vec)):
    Eg = Eg_vec[i]
    v_abs = v[v >= (Eg * e / h)].reshape((-1, 1))
    E = E_v(v[v >= (Eg * e / h)])
    eta_vec[i] = simps(E.ravel(), v_abs.ravel()) / simps(B.ravel(), v.ravel())

fig, ax =  plt.subplots(figsize=(5, 5))
ax.plot(Eg_vec, eta_vec * 100, 'r', lw=1.2)
ax.axvline(Eg_vec[np.argmax(eta_vec)], linestyle='--' ,color='0.5')
ax.axhline(np.max(eta_vec)*100, linestyle='--', color='0.5')
ax.set_xticks(np.concatenate([np.arange(0, 7, 2), Eg_vec[np.argmax(eta_vec)].reshape(1,)], axis=0))
ax.set_yticks(np.concatenate([np.arange(0, 60, 10), np.max(eta_vec).reshape(1,) * 100]))
ax.axis([-0.1, 6.1, 0, 50])
ax.set_xlabel(r'$E_g$ [eV]')
ax.set_ylabel(r'$\eta$ [%]')
ax.legend(['Efficiency', 'Max Efficiency'])
fig.tight_layout()
fig.savefig('./Graphs/q3_5')





