import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import elementary_charge, Boltzmann, epsilon_0
from scipy.optimize import minimize, root, fsolve

kB = Boltzmann
e = elementary_charge
T = 300
beta = e / (kB * T)

def bigF(nj0, pj0, Yj0):

    operand = (nj0)*(np.exp(beta*Yj0) - beta*Yj0 -1) \
              + (pj0) * (np.exp(-beta*Yj0)+ beta*Yj0 -1)

    return np.sqrt(operand)

# Intrinsic Carrier Densities
n_iA = 2.1e3
n_iB = 2.1e6

def devY(nj0, pj0, Yj0, epsr):

    C = - np.sqrt((2 * e) / (beta * epsr * epsilon_0))

    return C * bigF(nj0, pj0, Yj0)

def BuiltPot(EfA, EfB, nA0, nB0):

    return (EfA + kB * T * np.log(nA0 / n_iA) - EfB - kB * T * np.log(nB0 / n_iB)) / e

def error(Y, EPS, N, P, EF):

    # unpack variables
    YA0, YB0 = Y[0], Y[1]
    epsA, epsB = EPS
    EfA, EfB = EF
    nA0, nB0 = N[0], N[1]
    pA0, pB0 = P[0], P[1]

    R1 = BuiltPot(EfA, EfB, nA0, nB0) - (-YA0 + YB0)
    R2 = (epsilon_0 * epsA * devY(nA0, pA0, YA0, epsA) - epsilon_0 * epsB * devY(nB0, pB0, YB0, epsB))
    #print('Value of R2: ', R2)

    return np.array([R1, R2])


# Mobile Charges
nA0_vec = np.array([1e14, 1e15, 1e16, 1e17, 1e18, 1e19, 1e20])
#print(np.log10(nA0_vec))
nB0_vec = np.array([1e10, 1e11, 1e12, 1e13, 1e14, 1e15, 1e16, 1e17, 1e18])
#print(np.log10(nB0_vec))
N = np.reshape(np.meshgrid(nA0_vec, nB0_vec), (2, len(nA0_vec)*len(nB0_vec))).T
#print(np.log10(N))
P = np.zeros(np.shape(N))
P[:, 0] = n_iA ** 2 / N[:, 0]
P[:, 1] = n_iB ** 2 / N[:, 1]
print(N.shape, P.shape)


# Permittivities
EPS = [12.9, 12.0]
# Fermi levels (intrinsic)
EF = np.array([-4.635, -4.75]) * e # in eV

Y_a = np.zeros(np.size(N, axis=0))
Y_b = Y_a.copy()
k=0
for nA, nB, pA, pB in zip(N[:,0], N[:,1], P[:,0], P[:,1]):
    bi = BuiltPot(EF[0], EF[1], nA, nB)
    print('Iteration #:', k)
    print(bi)
    sol, _, int, mess = fsolve(error, np.array([-0.5*bi, 0.5*bi]), args=(EPS, [nA, nB], [pA, pB], EF), full_output=True
                             #options={
                             #    #'disp':True,
                             #    'maxiter': 100000
                             #}
                             )
    if int != True:
        print(mess)
    print('Solution Found: ', sol)
    Y_a[k], Y_b[k] = sol[0], sol[1]
    k += 1

print('iter: ', k)

# Compute the dE2d layer value
dE_2d = (-e * Y_b + kB * T * np.log(N[:, 1] / n_iB))/e

# Reshape response value into grid
dE_2d = np.reshape(dE_2d, (np.size(nB0_vec, axis=0), np.size(nA0_vec, axis=0)))
xx, yy = np.meshgrid(np.log10(nA0_vec), np.log10(nB0_vec))

fig, ax = plt.subplots(figsize=(5, 5))
im1 = ax.contourf(xx, yy, dE_2d, 10, cmap='viridis')
im2 = ax.contour(xx, yy, dE_2d, 5, colors='k', linewidths=0.8)
ax.set_ylabel(r'$log_{10}(n_{B0})$')
ax.set_xlabel(r'$log_{10}(n_{A0})$')
ax.title.set_text(r'$\Delta E_{2D}$')
fig.colorbar(im1, ax=ax)
plt.clabel(im2, inline=True, fontsize=8)

fig.savefig('./Graphs/q2_7')

# Compute Potential Solutions for specific case
nA0, nB0 = 1e18, 1e11
pA0, pB0 = n_iA**2 / nA0, n_iB**2 / nB0
bi = BuiltPot(EF[0], EF[1], nA0, nB0)
sol = fsolve(error, np.array([-0.5 * bi, 0.5 * bi]), args=(EPS, [nA0, nB0], [pA0, pB0], EF)
                    #, options={'disp':True}
                  )
YA0, YB0 = sol

print()
print(YA0, YB0)
dE_2d = (-e * YB0 + kB * T * np.log(nB0 / n_iB)) / e
print('Value of dE_2d: ', dE_2d)

plt.show()
