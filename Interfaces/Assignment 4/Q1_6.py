import numpy as np
from scipy.constants import elementary_charge, Boltzmann, epsilon_0, N_A, nano, atm
import matplotlib.axis
import matplotlib.pyplot as plt


# Define some constants
e = elementary_charge
eps0 = epsilon_0
kB = Boltzmann
eps_r = 80
e0, ew = eps0, eps_r
kb = kB
Na = N_A

######################################################################################################

# Q1.6

# Problem Parameters
T = 298.15 # K
R = 5 * nano # nm

# Computing Max Concetration
c_max = 8 * e0 * ew * kb * T / (e**2 * R**2) # in molecules per m^3

print()
print('Max concentration attainable is: %0.3f M' % (c_max / (1000 * Na)))
print()

Pwall = c_max*kb*T / atm


print('The maximum pressure on the wall is: %0.3f atm' % Pwall)
print()


################################################################################################

# Q1.9

# Defining the velocity profiles in terms of U/Umax
def pouseille(rR):
    return 1 - rR[:, ]**2

def eof(rR, kR):

    PsiR = (kb * T / e) * np.log((1 - (1 / 8) * (1/kR)**2)**2)
    Psi = (kb * T / e) * np.log((1 - (1 / 8) * (rR[:, ]/kR)**2)**2)

    return 1 - Psi / PsiR

# Setting conditions and vector to plot
kR = [0.36, 0.4, 0.5, 1.0]
rR = np.linspace(0.0, 1, 100).reshape((-1, 1))

fig, axs = plt.subplots(2, 2, figsize=(9, 6))
ax = [axs[0, 0], axs[0, 1], axs[1, 0], axs[1, 1]]

# Plotting velocity profiles
for i in range(len(kR)):
    title = r'$\frac{\kappa ^{-1}}{R} = $'+'${:.2f}$'.format(kR[i])
    ax[i].plot(rR, eof(rR, kR[i]), 'r', lw=1.2, label=r'$\frac{v_z}{v_{max}} = 1 - \frac{\psi(r)}{\psi _{R}}$')
    ax[i].plot(rR, pouseille(rR), 'b', lw=1.2, label=r'$\frac{v_z}{v_{max}} = 1 - (\frac{r}{R})^2$')
    ax[i].title.set_text(title)

# Formatting
ax[-2].set_ylabel(r'$\frac{u}{u_{max}}$')
ax[-2].set_xlabel(r'$\frac{r}{R}$')
handles, labels = ax[-1].get_legend_handles_labels()
fig.legend(handles, labels, loc='center right', borderaxespad=0.2)
plt.subplots_adjust(right=0.83, left=0.07, hspace=0.3, top=0.93, bottom=0.09)
plt.show()
fig.savefig('Graphs/q1_6')
