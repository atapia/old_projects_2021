import numpy as np
from pandas import read_csv
from scipy.constants import elementary_charge, Boltzmann, epsilon_0, N_A
from scipy.optimize import fsolve
import matplotlib.axis
import matplotlib.pyplot as plt

# Andres Rocha Tapia
# 11.05.2020

# Define some constants
e = elementary_charge
eps0 = epsilon_0
kB = Boltzmann
eps_r = 80
e0, ew = eps0, eps_r
kb = kB
Na = N_A

def PBE(Y, p):
    """
        Here we define the ODE system, for the finite differences with f(Y) = the poisoon boltzman distribution

        :return: return the value of the ode
        :param Y:  potential Psi
        :param p: vector of parameters p = [z+, z-, M0, T]
        """

    # Define Variables and Parameters:
    z_pos, z_neg, M0, T = p
    M0_pos = abs(z_neg) * M0
    M0_neg = abs(z_pos) * M0

    C = e / (eps0 * eps_r)
    D = - e / (kB * T)

    f = [C * z_pos * 1000 * N_A * M0_pos * np.exp(z_pos * D * Y[:, ]) + C * z_neg * 1000 * N_A * M0_neg * np.exp(
        z_neg * D * Y[:, ])]

    return np.array(f).reshape((-1, 1))


def matrixA(n):

    A = np.zeros([n, n])

    for i in np.arange(n):
        if i == 0:
            A[i, 0] = 2
            A[i, 1] = -1
            continue

        if i == n-1:
            A[i, i-1] = -1
            A[i, i] = 2
            continue

        A[i, i-1] = -1
        A[i, i] = 2
        A[i, i+1] = -1

    return A


def vectorF(Y, a, b, d, p):
    F = PBE(Y, p)
    F[0] = F[0] + a / d**2
    F[-1] = F[-1] + b / d**2
    #print('vectorF:', F.shape)

    return F


def solver(Yguess, a, b, n, h, p):
    Yguess = Yguess.reshape((-1, 1))
    #print('solver Y:', Yguess.shape)
    A = matrixA(n)
    d = h / (n+1)
    A = A / d ** 2
    #print('solver A:', A.shape)
    F = vectorF(Yguess, a, b, d, p)
    #print('solver F:', F.shape)
    sol = A.dot(Yguess) - F
    #print('solver sol:', sol.shape)
    sol = sol.flatten()
    return sol


def kappa(M0, T=300):
    return (1000 * (e ** 2) * Na * 2 * M0 / (e0 * ew * kB * T)) ** 0.5


def GC(x, M0, V0, T=300):
    C = (4 * kb * T / e)
    sol = C * np.arctanh(np.exp(-kappa(M0, T) * x[:, ]) * np.tanh( V0 / (C)))
    return np.array(sol).reshape((-1, 1))

M0 = 0.001 #M
Z = [1, -1]
V0 = 40e-3 #V
H = np.arange(10e-9, 105e-9, 5e-9)
p = [Z[0], Z[1], M0, 300]

# Setting the number of points
n = 100

# Vector to store solutions
V12_fd = np.zeros([np.size(H, 0), 1])
#fig = plt.figure()
for h in H:
    print(h)
    # generating the Psi_init using the GC solutions
    d = h / (n+1)
    Xinit = np.linspace(d, h/2, int(n/2))
    print(Xinit.shape)
    halfGC = GC(Xinit, M0, V0)
    Yinit = np.concatenate([halfGC, np.flip(halfGC)])
    #print('loop: ', Yinit.shape)

    Ysol = fsolve(solver, Yinit, args=(V0, V0, n, h, p) #, full_output=True
                  )

    #plt.plot(np.linspace(d, h, n), Ysol)

    V12_fd[np.argwhere(H == h)] = Ysol[len(Xinit)-1]

#plt.legend(['10', '20', '30', '40', '50', '60', '70', '80', '90', '100'])
#plt.show()

V12_fd = V12_fd * 1e3 # in mV
H = H * 1e9 # in nm
print(V12_fd.shape)

# Importing the previous solutions from Q2.3
V12_sh = read_csv('./Data/q2_2_Y12.csv', header=None).to_numpy()
print(V12_sh.shape)

# Computing the difference between the two methods as a function of n points
error = []
for n in np.arange(10, 210, 10):
    print(n)
    v12 = np.zeros([np.size(H, 0), 1])
    for h in H/1e9:
        d = h / (n + 1)
        Xinit = np.linspace(d, h / 2, int(n/2))
        halfGC = GC(Xinit, M0, V0)
        Yinit = np.concatenate([halfGC, np.flip(halfGC)])
        Ysol = fsolve(solver, Yinit, args=(V0, V0, n, h, p)  # , full_output=True
                      )
        v12[np.argwhere(H/1e9 == h)] = Ysol[len(Xinit) - 1]
    print(v12.shape)
    er = ((v12 * 1e3 - V12_sh[:, 1].reshape((-1, 1))) ** 2).mean()
    print(er, er.shape)
    error.append(er)

error = np.array(error)**0.5


# Plotting Solutions to compare
fig, ax = plt.subplots(1, 2, figsize=(8, 6))
ax1, ax2 = ax[0], ax[1]

# Plotting
ax1.plot(V12_sh[:,0], V12_sh[:,1], 'r', linestyle='dashed', lw=1.2)
ax1.scatter(H, V12_fd, c='k', marker='x')
ax2.scatter(np.arange(10, 210, 10), error, 20, c='r')
ax2.plot(np.arange(10, 210, 10), error, 'r', linestyle='dotted', lw=1.2)

# Format axis
ax1.axis([8, 102, 0, 35])
ax1.set_xticks(np.arange(10, 110, 10))
ax1.set_yticks(np.arange(0, 40, 5))
ax1.set_ylabel(r'$\Psi (\frac{h}{2})$' ' (mV)')
ax1.set_xlabel('H (n$m$)')
ax1.legend(['Shooting Method (Sh) Solution', 'Finite Differences (FD) Solution \n@ $n=100$'], loc='best')


ax2.axis([-10, 210, -0.02, 0.3])
ax2.set_xticks(np.arange(0, 220, 20))
ax2.set_yticks(np.arange(0, 0.35, .05))
ax2.set_ylabel('RMSE (mV)')
ax2.set_xlabel('$n$ points')
ax2.legend(['_', r'$RMSE = \sqrt{\frac{\sum_{h}^{K} (\psi _{Sh} - \psi _{FD,h})^2}{K}}$'])

fig.tight_layout()
plt.show()
fig.savefig('./Graphs/q2_5')
























