import numpy as np
from scipy.constants import elementary_charge, Boltzmann, epsilon_0, N_A
from scipy.integrate import solve_ivp, odeint
from scipy.optimize import minimize, fsolve
import matplotlib.axis
import matplotlib.pyplot as plt

# Andres Rocha Tapia
# 11.05.2020

# Define some constants
e = elementary_charge
eps0 = epsilon_0
kB = Boltzmann
eps_r = 80
e0, ew = eps0, eps_r
kb = kB
Na = N_A


def PBE(x, w, p):
    """
        Here we define the ODE system, since we are working with a 2nd order ODE, it was split
        into an extra variable to turn the problem into a system of 1st order ODEs.

        :return: return the value of the ode
        :param w: vector of state variables
                        w = [x1, x2] (in this case x1 = Psi, x2 = Psi')
        :param x:  time (in this case x distance)
        :param p: vector of parameters p = [z+, z-, M0, T]
        """

    # Define Variables and Parameters:
    z_pos, z_neg, M0, T = p
    M0_pos = abs(z_neg) * M0
    M0_neg = abs(z_pos) * M0

    x1, x2 = w
    C = - e / (eps0 * eps_r)
    D = - e / (kB * T)

    # Define the ODE
    f = [x2,
         C * z_pos * 1000 * N_A * M0_pos * np.exp(z_pos * D * x1) + C * z_neg * 1000 * N_A * M0_neg * np.exp(
        z_neg * D * x1)]

    return f


def ode_solv(V12, V0, Z, M0, H, T=300):
    # Some constants
    z_pos, z_neg = Z

    # Pack Parameters
    p = z_pos, z_neg, M0, T

    # Initial Solutions
    y0 = V0
    z0 = BC_check(V12, V0, Z, M0, T)  # this would be a guess initial condition
    w0 = [y0, z0]

    # time span
    t = [0, H / 2]
    t_eval = np.linspace(0, H / 2, 1000)

    sol = odeint(PBE, w0, t_eval, args=(p,), tfirst=True, printmessg=True)
    return sol


def BC_check(V12, V0, Z, M0, T=300):
    # Some constants
    C = 2 * kB * T / (eps0 * eps_r)
    D = - e / (kB * T)
    z_pos, z_neg = Z
    M0_pos = abs(z_neg) * M0
    M0_neg = abs(z_pos) * M0

    operand = C * (1000 * N_A * M0_pos * np.exp(D * z_pos * V0) + 1000 * N_A * M0_neg * np.exp(D * z_neg * V0)) \
              - C * (1000 * N_A * M0_pos * np.exp(D * z_pos * V12) + 1000 * N_A * M0_neg * np.exp(D * z_neg * V12))

    return -np.sign(V0) * np.sqrt(operand)


def shooting(guess, V0, Z, M0, H, T=300):
    # We define the absolute error of the ODE solver since we know that
    # for [dV/dx](Y=h/2) = 0
    # step is the deviation

    # Here we defined an error function that calculates the total sum of the squared error.
    # Then the error is simply the value of the solution at the
    # final position
    def abs_r(guess, V0, Z, M0, H, T):
        e = ode_solv(guess, V0, Z, M0, H, T)
        return abs(e[-1, 1])

    sol1 = minimize(abs_r, guess, args=(V0, Z, M0, H, T,), method='Nelder-Mead', options={'ftol': 1e-6}
                  )
    #print(sol1)

    return sol1.x


def kappa(M0, T=300):
    return (1000 * (e ** 2) * Na * 2 * M0 / (e0 * ew * kB * T)) ** 0.5


def GC(x, M0, V0, T=300):
    C = (4 * kb * T / e)
    return C * np.arctanh(np.exp(-kappa(M0, T) * x[:, ]) * np.tanh( V0 / (C)))


########################################################################################################################

# Q 2.2 Computer V_1/2 for H = 10nm - 100 nm


# Attempting for a single value

M0 = 0.001 #M
Z = [1, -1]
V0 = 40e-3 #V
H = np.arange(10e-9, 105e-9, 5e-9)


# Vector to store solutions of Psi(h/2), Psi'(h/2)
Y = np.zeros([np.size(H, 0), 1])
dY = np.zeros([np.size(H, 0), 1])
Y_GC = Y.copy()
print(Y.shape, dY.shape)

fig, axs = plt.subplots(1, 2, figsize=(9, 6))
ax1 = axs[0]
ax2 = axs[1]

# Computing solutions
for h in H:
    v12 = shooting(0.5e-3, V0, Z, M0, h)
    y = ode_solv(v12, V0, Z, M0, h)
    Y[np.argwhere(H == h)], dY[np.argwhere(H == h)] = y[-1]
    Y_GC[np.argwhere(H == h)] = GC(np.array(h/2).reshape((-1, 1)), M0, V0) * 2

H = H*1e9 # in nm
Y = Y*1e3 # in mV
Y_GC = Y_GC*1e3
dY = dY*1e5 # in Sci notation
Y12sols = np.vstack([H, Y[:, 0]]).T
print(Y12sols.shape)

np.savetxt('./Data/q2_2_Y12.csv', Y12sols, delimiter=',')


# Formatting axis
ax1.axis([10, 100, 0, 50])
ax1.set_xticks(np.arange(10, 110, 10))
ax2.axis([10, 100, -5, 5])
ax2.set_yticks(np.arange(-5, 6, 1))
ax2.set_xticks(np.arange(10, 110, 10))

# Setting Labels
ax1.set_ylabel(r'$\Psi (\frac{h}{2})$' ' (mV)')
ax1.set_xlabel('H (n$m$)')
ax2.set_ylabel(r'$\Psi (\frac{h}{2})$' ' (mV $m^{-1}$)')
ax2.set_xlabel('H (n$m$)')

# Plotting Solutions
ax1.plot(H, Y, 'r', lw=1.2)
ax1.plot(H, Y_GC, 'r', linestyle='dashed', lw=1.2)
ax1.legend(['Numerical Solution', 'Additive Solution (GC)'])
ax2.scatter(H, dY, c='r')
ax2.plot(H, dY, 'r', linestyle='dashed', lw=1)
ax2.legend(['Error on BC:\n' r'$\frac{d\psi}{dx} (x=\frac{h}{2}) = 0$', '_hidden'], loc='best')
plt.show()
fig.tight_layout()
fig.savefig('./Graphs/q2_2')





