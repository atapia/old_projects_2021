import numpy as np
from scipy.constants import Boltzmann, N_A, atm
import matplotlib.axis
import matplotlib.pyplot as plt

kB = Boltzmann
Na = N_A


def dP(betaV, dC, T=300):

    K = np.exp(-betaV[:, ])

    return (1-K) * kB * T * dC

betaV = np.concatenate([np.linspace(0.01, 1, 1000), np.linspace(1.01, 100, 500)]).reshape((-1, 1))
dC = 1.13 * 1000 * Na
dP_pos = dP(betaV, dC)
dP_neg = dP(-betaV, dC)

fig, ax = plt.subplots(1, 1, figsize=(9, 6))
ax.axis([0.01, 100, -30, 30])
ax.semilogx(betaV, dP_pos/atm, 'r')
ax.axhline(np.max(dP_pos/atm), linestyle='--', color='0.5', lw=0.9)
ax.semilogx(betaV, dP_neg/atm, 'b')

ax.set_ylabel(r'$\Delta p$ ' '(atm)')
ax.set_xlabel(r'$|\beta V_m|$')
ax.legend(['Repulsive Potential: ' r'$V_m > 0$', '_',  'Attractive Potential: ' r'$V_m < 0$'], loc='lower right')

ticks = np.concatenate([np.arange(-30, 40, 10), np.max(dP_pos/atm).ravel()])
ax.set_yticks(ticks)

fig.tight_layout()
fig.savefig('./Graphs/q3_8')


