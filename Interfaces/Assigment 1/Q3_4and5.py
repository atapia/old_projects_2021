import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt


# Andres Rocha Tapia
# 09.03.2020
# Question 3.4 Numerical Solution of Phi_NL

def phi_nl(N, beta, deltaGL):
    """

    :param N: scalar which determines the number of graphene layers
    :param beta: input parameter float, (1,) shape
    :param deltaGL: minimum interlayer distance, float, (1,) shape
    :return: Returns the toal vdWs intreaction between a variable density liquid and a stack of graphene
    """
    # Definition of global constants
    kb = 1.38e-23  # m2 * kg * s-2 * K-1 // Boltzmann's
    T = 298.15  # K
    pL = 997  # kg m-3 // In this case water
    MW = 18.01528  # g mol-1 // Molecular mass of water
    Na = 6.022e23  # Avogadro's
    pLm = Na * pL * 1000 / MW  # volumetric particle density of water
    d0 = 3.35e-10  # m // interlayer distance of graphene sheets
    a = 2.49e-10  # m // lattice constant
    S = np.sqrt(3) * (a ** 2) / 2  # area of hexagon
    sigma = 2 / S  # surface density of graphene

    # We define the constant coefficient that appears on our overall expressions
    # for simplicity
    mu = (-1 * np.pi * sigma * beta) / 2

    # Define a distance vector for each layer of graphene, starting from
    # the surface of the graphene stack
    N_vec = np.reshape(np.arange(N), [N, 1])
    N_vec = N_vec * d0

    # Defining the nested functions for the overall integration and sum

    def w_x(x):
        """
        In this function we calculate the vdWs of a single water molecule and N layers of graphene at distance x
        :param x: can take a 1x1 vector
        :return: returns the vdWs for a single water molecule and N layers
        :rtype: float ;

        """
        w = mu / (x + N_vec) ** 4

        return np.sum(w, 0)

    def rho(x):
        """
        Calculates the particle density of water as a function of distance x
        :param x: can take a 1x1 vector
        :return: returns a scalar of the density of water moleules at distance x from graphene layer
        """

        return pLm * np.exp(-w_x(x) / (kb * T))

    def integrand(x, ss):

        x_size = len(x)
        int_N = np.zeros([N, 1])

        # Loop one: Evaluating the overall integral at a given Nth layer
        for i in np.arange(N):

            int_vec = np.zeros([x_size, 1])

            # Loop 2: calculates the associated rho value at a given distance x (for current N) and
            # calculates the integral as a left hand Riemann Sum with step specified as input (Y, step)
            # appends the result to (x_size x 1) vector
            for j in np.arange(x_size):
                rho_j = rho(x[j])
                int_vec[j] = mu * rho_j * ss / ((x[j] + N_vec[i]) ** 4)

            # Sums over all the Riemann rectangles created and appends them to a integral vector with index N
            int_N[i] = int_vec.sum()


        # Calculates the total sum for each N
        total = int_N.sum()

        return total

    # Calculate the the integral for the N-layer using left hand Riemann Sums
    # We define the integration space starting from deltaGl to a large number (since power law of -4 makes a quick
    # convergence and avoid much time delay over large array sizes)
    # With a step size of 0.01
    step = d0 / 2
    points = 1000
    X = np.linspace(deltaGL, points * step, points)
    X = np.reshape(X, [len(X), 1])

    M = integrand(X, step)

    return M * 1000  # conversion to mJ / m2


# Specifying Inputs
b = 1e-78  # J m-6 // in the order of e-78
d = 3.28e-10  # m
n = 1

# we calculate the total integral given the imput above
print('Q3.4 --> The obtained value of Phi_NL is: ', phi_nl(n, b, d), ' mj m^-2')
print('\n')
# We obtain: -106.65635681065014 mJ/m2 (approx. 100 mJ/m2)
# though i noticed that the riemann sum is highly sensitive to the size of the step
# I adjusted the step so that when N = 1, then we obtain around 100 mj m-2


########################################################################################################################

# Question 3.5 Estimating beta

# Defining new additional constants

gammaL = 72.8  # mJ m-2
deltaGL = 3.28e-10  # m
theta = np.deg2rad(86)  # radians
n = 20  # we employ a larger number to mimic graphite


# Defining Young-Dupre Equation to solve for beta and including the defined
# function from the previous exercise 3.4 named phi_nl(N, beta, deltaGL)

def young(beta):
    return gammaL * (1 + np.cos(theta)) + phi_nl(n, beta, deltaGL)


# Initial Guess
beta0 = 1e-78

# Running root finder
res = fsolve(young, beta0)

print('Q3.5 ---> The estimated value of Beta_CL is: ', res[0], ' J m^-6')
print('\n')
# Q3.5 ---> The estimated value of Beta_CL is:  7.84962771881914e-79

########################################################################################################################

# Q4.1 Contant Angle of water as a function of N

# Obtained estimated beta
beta = res[0]

# Other constants redefined for ease of following
deltaGL = 3.28e-10
gammaL = 72.8


def angle(N):
    return np.rad2deg(np.arccos(-phi_nl(N, beta, deltaGL) / gammaL - 1))


N_max = 20
N_axis = np.arange(1, N_max + 1)
theta_ax = np.zeros([len(N_axis + 1)])

for i in np.arange(len(N_axis)):
    theta_ax[i] = angle(N_axis[i])

print('Q4.1 ---> For N=1, we have that the contact angle is: ', theta_ax[0])
print('\n')
# We obtain theta = 95.47, slightly below, but possible due to step and range choice during integral

fig = plt.figure(1)
ax1 = fig.add_subplot(111)
ax1.set_ylabel('Contact Angle in Degrees')
ax1.set_xlabel('N Layers of Graphene')
plt.xticks(np.arange(0, 21, 2))


line, = ax1.plot(N_axis, theta_ax, lw=2)
line2, = ax1.plot(N_axis, np.ones(len(theta_ax)) * np.min(theta_ax), color='red', lw=1)
fig.tight_layout()
plt.show()

