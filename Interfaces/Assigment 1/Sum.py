import numpy as np

def suminf(N):

    X = np.arange(1, N)
    xx, yy = np.meshgrid(X, X)
    M = 1 / (xx + yy - 1) ** 4

    return np.sum(np.sum(M, 1), 0)

E = suminf(1000)
print(E)
