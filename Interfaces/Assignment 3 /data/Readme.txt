This folder contains the epsilon'' values for 3 materials that you can use to calculate epsilon(i nu).

In each file, the first column is the energy h*nu in eV and second column is the epsilon'' value. The files are comma-delimited and you can extract the data using:

- readmatrix() method in Matlab
- numpy.genfromtxt() or numpy.loadtxt() methods in Python