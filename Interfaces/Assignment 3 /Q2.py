import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import Axes

#############################################################################
# Question 2.2
f_osc = 1.3409
h = 6.62607004 * 10 ** -34  # m2 kg s-1
e = 1.60217662e-19  # Coulombs
v_osc = 10 * e / h  # eV / h


def eps_a(v):
    ratio = f_osc / (1 + (v[:, ] / v_osc) ** 2)
    return 1 + ratio


# Defining search space
v = np.linspace(0, 1e17, 10000).reshape((-1, 1))
eps = eps_a(v)
# Transforming frequency values into energies
hv = h * v[:, ] / e

# Plotting values
fig1, ax = plt.subplots()
line, = ax.semilogx(hv, eps, lw=0.8)
lin2, = ax.plot([0, 1e17], [1, 1], lw=1, color='gray', linestyle='--')
ax.axis([0.1, 100, 0, 3])
ax.set_ylabel('\u03B5($i$\u03BD)')
ax.set_xlabel('$h$\u03BD (eV)')
fig1.savefig('./Graphs/q2_2')

#############################################################################
# Question 2.3

# define some constants
kb = 1.38064852e-23  # m2 kg s-2 K-1
eps0 = 1  # 8.854187812813e-12  # F⋅m−1


def matsubara(n, T):
    return 2 * np.pi * kb * T * n[:, ] / h


set_T = 298.15
def hamaker_sum(n, T=set_T):
    C = 3 * kb * T / 2
    n = np.arange(1, n + 1).reshape((-1, 1))
    zero = np.zeros((1, 1))
    sum0 = 0.5 * (eps_a(matsubara(zero, T)) - eps0) ** 2 / (eps_a(matsubara(zero, T)) + eps0) ** 2
    summand = (eps_a(matsubara(n, T)) - eps0) ** 2 / (eps_a(matsubara(n, T)) + eps0) ** 2
    return C * (summand.sum() + sum0.sum())


# Calculating Convergence of Hamaker Constant
n_space = np.arange(1000).reshape((-1, 1))
ham_space = np.zeros((len(n_space), 1))

for i in np.arange(len(n_space)):
    ham_space[i] = hamaker_sum(i)

# Calculating Hamaker Constant as a function of T

T_space = np.linspace(0, 1000, 2000).reshape((-1, 1))
hamT_space = np.zeros((len(T_space), 1))

set_n = 600
for i in np.arange(len(T_space)):
    hamT_space[i] = hamaker_sum(set_n, T_space[i])

# Plotting vectors
fig2, ax = plt.subplots(1, 2)
ax1, ax2 = ax

# Plotting convergence of Hamaker Constant at T = 298.15 K
im1 = ax1.semilogx(n_space, ham_space, lw=0.8, color='r')
lin, = ax1.plot([0, 1000], [np.max(ham_space), np.max(ham_space)], lw=0.5, color='gray', linestyle='--')
ax1.set_title('$a)$', fontweight='bold')
ax1.set_xlabel('Value of $n$')
ax1.set_ylabel('Hamaker Constant $A_a$')
ax1.legend(['@T = {}K'.format(set_T)], loc='upper left')
ax1.axis([.01, 1000, 0, np.max(ham_space) * 1.15])

# Plotting Hamaker as function of temperature fro set n = 600
im2 = ax2.semilogx(T_space, hamT_space, lw=0.8, color='r')
lin, = ax2.plot([0, 1000], [np.max(hamT_space), np.max(hamT_space)], lw=0.5, color='gray', linestyle='--')
ax2.set_title('$b)$', fontweight='bold')
ax2.set_xlabel('Temperature (K)')
ax2.legend(['@$n$ = {}'.format(set_n)], loc='upper left')
ax2.axis([0.01, 1000, 0, np.max(hamT_space) * 1.15])
fig2.savefig('./Graphs/q2_3')
