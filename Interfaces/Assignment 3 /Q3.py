import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import Axes
from scipy.constants import Boltzmann, epsilon_0, \
    N_A, elementary_charge, angstrom, nano
from scipy.optimize import fsolve

#############################################################################
# Q3.3 Compute potential profiles for GC and GCS model when Psi0 = 500mV at
# T = 300K for i) 1mM and ii) 1M KCl solutions

kb = Boltzmann
e0 = epsilon_0
Na = N_A
e = elementary_charge
ew = 80
es = 25
ds = 2.5e-10
#print(ds)
#print([kb, e0, Na, e])


def kappa(M0, T=300):
    return (1000 * (e ** 2) * Na * 2 * M0 / (e0 * ew * kb * T)) ** 0.5


def GC(x, M0, V0, T=300):
    C = (4 * kb * T / e)
    return C * np.arctanh(np.exp(-kappa(M0, T) * x[:, ]) * np.tanh( V0 / (C)))


def Stern(x, V0, Vi, d):
    return V0 - ((V0 - Vi) * x[:, ] / d)


def diffGC(V, M0, T=300):
    C = - np.sqrt((8 * kb * T * 1000 * Na * M0) / (ew * e0))
    return C * np.sinh(e * V[:, ] / (2 * kb * T))


def diffStern(V, V0, d):
    return (V[:, ] - V0) / d


def capGC(V, M0, T=300):
    return kappa(M0, T) * e0 * ew * np.cosh(e * V[:, ] / (2 * kb * T))


def solver(V, M0, V0, d, T=300):
    return ew * diffGC(V, M0, T) - es * diffStern(V, V0, d)


#############################################################################
# Q3.1 Compute d_EDL of GC model for V0 = 1V

V0 = 1 # V
M0 = 1 # M
K_1 = 1/kappa(M0)
print('Q3.1')
print('d_DH is: {:.2f}nm for 1M KCl at T = 300K'.format((K_1)/nano))
print('Effective d_EDL of GC model is: {:.2e}m for 1M KCl at T = 300K'.format((K_1/np.cosh(e*V0/(2*kb*300)))))

#############################################################################
# Q3.3 Compute potential profiles for GC and GCS model when Psi0 = 500mV at
# T = 300K for i) 1mM and ii) 1M KCl solutions

V0 = 0.5  # V
M0 = np.array([0.001, 1.])  # M

# Compute the potential profiles using Gouy-Chapman Method
dist = 50e-9
step = 0.5e-10
X = np.arange(0, dist+step, step)
X = X.reshape((-1, 1))
Xg = X.copy() * 1e9

GC1 = GC(X, M0[0], V0)
GC2 = GC(X, M0[1], V0)

# Compute solution for finding continuity conditions employing Gauss Law and Conservation of displacement field
Vsol_1 = fsolve(solver, np.array([0.3]), args=(M0[0], V0, ds), full_output=True)
Vsol_2 = fsolve(solver, np.array([0.3]), args=(M0[1], V0, ds), full_output=True)
#print(Vsol_1, '\n\n', Vsol_2)

# Compute potential profiles using Gouy-Chapman-Stern Method
Xstern = np.arange(0, ds+step, step).reshape((-1, 1))
Xgc = np.arange(ds, dist+step, step).reshape((-1, 1))
Xgcs = np.concatenate([Xstern, Xgc], 0) * 1e9
#print(Xgcs[:7])

GCS1 = np.concatenate([Stern(Xstern, V0, Vsol_1[0], ds), GC(Xgc-ds, M0[0], Vsol_1[0])], 0)
#print(GCS1[:7])
GCS2 = np.concatenate([Stern(Xstern, V0, Vsol_2[0], ds), GC(Xgc-ds, M0[1], Vsol_2[0])], 0)

fig, axs = plt.subplots(1, 2)
ax1, ax2 = axs

# Plotting GC and GCS solutions
ax1.plot(Xg, GC1, 'r', Xg, GC2, 'b', lw=0.8)
ax1.plot(Xgcs, GCS1, 'r', Xgcs, GCS2, 'b', lw=1.1, linestyle='-.')
ax1.set_title('GC and GCS models for:\n1mM and 1M KCl solutions' r' with $\Psi _0=0.5$V', fontsize=8)
ax1.set_ylabel(r'$\Psi$(x) (V)')
ax1.set_xlabel('x (n$m$)')
ax1.set_xticks([0, 10, 20, 30, 40, 50])
ax1.legend(['GC: 1mM', 'GC: 1M', 'GCS: 1mM', 'GCS: 1M'], fontsize=8)

# Zoomed in version
ax2.plot(Xg, GC1, 'r', Xg, GC2, 'b', lw=0.8)
ax2.plot(Xgcs, GCS1, 'r', Xgcs, GCS2, 'b', lw=1.1, linestyle='--')
ax2.axis([-0.1, 2, -0.01, 0.51])
ax2.set_title('Near interface GC and GCS models for:\n1mM and 1M KCl solutions' r' with $\Psi _0=0.5$V', fontsize=8)
ax2.set_xlabel('x (n$m$)')
ax2.legend(['GC: 1mM', 'GC: 1M', 'GCS: 1mM', 'GCS: 1M'], fontsize=8)

fig.savefig('./Graphs/q3_3')
plt.close()

#############################################################################
# Q3.4 Compute potential profiles for GC and GCS model when 𝜎M = 0.01 C/m2
# T = 300K for i) 1mM and ii) 1M KCl solutions

sigma0 = 0.01  # C m-2
T = 300
# Computing V0 for GC model
C = np.sqrt(8 * kb * T * e0 * ew * 1000 * Na * M0[:])
#print(C)
V0_GC1 = (np.arcsinh(sigma0 / C[0]) * 2 * kb * T) / e
V0_GC2 = np.arcsinh(sigma0 / C[1]) * 2 * kb * T / e
#print([V0_GC1, V0_GC2])

# Compute the potential profiles using Gouy-Chapman Method

GC1 = GC(X, M0[0], V0_GC1)
GC2 = GC(X, M0[1], V0_GC2)

# Computing V0 for GCS model
V0_GCS1 = sigma0 * ds / (es * e0) + V0_GC1
V0_GCS2 = sigma0 * ds / (es * e0) + V0_GC2

# Compute the potential profiles using GCS method
GCS1 = np.concatenate([Stern(Xstern, V0_GCS1, V0_GC1, ds), GC(Xgc-ds, M0[0], V0_GC1)], 0)
GCS2 = np.concatenate([Stern(Xstern, V0_GCS2, V0_GC2, ds), GC(Xgc-ds, M0[1], V0_GC2)], 0)

fig, axs = plt.subplots(1, 2)
ax1, ax2 = axs

# Plotting GC and GCS solution
ax1.plot(Xg, GC1, 'r', Xg, GC2, 'b', lw=0.8)
ax1.plot(Xgcs, GCS1, 'r', Xgcs, GCS2, 'b', lw=1.1, linestyle='--')
ax1.set_title('GC and GCS models for:\n1mM and 1M KCl solutions' r' with $\sigma _M=0.01 Cm^{-2}$', fontsize=8)
ax1.set_ylabel(r'$\Psi$(x) (V)')
ax1.set_xlabel('x (n$m$)')
ax1.set_xticks([0, 10, 20, 30, 40, 50])
ax1.legend(['GC: 1mM', 'GC: 1M', 'GCS: 1mM', 'GCS: 1M'], fontsize=8)

# Zoomed in version
ax2.plot(Xg, GC1, 'r', Xg, GC2, 'b', lw=0.8)
ax2.plot(Xgcs, GCS1, 'r', Xgcs, GCS2, 'b', lw=1.1, linestyle='--')
ax2.axis([0, 2, -0.01, 0.12])
ax2.set_title('Near interface GC and GCS models for:\n1mM and 1M KCl solutions' r' with $\sigma _M=0.01 Cm^{-2}$', fontsize=8)
ax2.set_xlabel('x (n$m$)')
ax2.legend(['GC: 1mM', 'GC: 1M', 'GCS: 1mM', 'GCS: 1M'], fontsize=8)
fig.tight_layout()
fig.savefig('./Graphs/q3_4')
plt.close()

#############################################################################
# Q3.5 Compute differential capacitance profiles for GC and GCS models as a function of Psi
# T = 300K for i) 1mM and ii) 1M KCl solutions
step = 0.01
Vm = np.arange(0, .5, step).reshape((-1, 1))

# Computing capacitance for GC model
C_GC1 = capGC(Vm, M0[0])
C_GC2 = capGC(Vm, M0[1])

# Computing capacitance for GCS model
C_GCS1 = (ds/(e0*es) + 1/capGC(Vm, M0[0])) ** -1
C_GCS2 = (ds/(e0*es) + 1/capGC(Vm, M0[1])) ** -1

fig, axs = plt.subplots(1, 2)
ax1, ax2 = axs

# Plotting GC and GCS solution for 1mM
ax1.plot(Vm, C_GC1, 'r', lw=0.8)
ax1.plot(Vm, C_GCS1, 'b', lw=1.1, linestyle='--')
ax1.set_title('C$_{diff}$ for 1mM KCl solution', fontsize=8)
ax1.set_ylabel(r'$C_{diff}$ (F)')
ax1.set_xlabel(r'$\psi_M$ (V)')
ax1.axis([0, 0.5, -10, 60])
ax1.set_xticks(np.arange(0, 0.5, 0.1))
ax1.legend(['GC', 'GCS'], fontsize=8)

# Zoomed in version
ax2.plot(Vm, C_GC1, 'r', lw=0.8)
ax2.plot(Vm, C_GCS1, 'b', lw=1.1, linestyle='--')
ax2.axis([0, 0.5, -0.2, 1.2])
ax2.set_title('Zoomed-in: C$_{diff}$ for 1mM KCl solution', fontsize=8)
ax2.set_xlabel(r'$\psi_M$ (V)')
ax2.legend(['GC', 'GCS'], fontsize=8)
fig.tight_layout()
fig.savefig('./Graphs/q3_5a')
plt.close()

# Plotting GC and GCS solution for 1M
fig, axs = plt.subplots(1, 2)
ax1, ax2 = axs

ax1.plot(Vm, C_GC2, 'r', lw=0.8)
ax1.plot(Vm, C_GCS2, 'b', lw=1.1, linestyle='--')
ax1.set_title('C$_{diff}$ for 1M KCl solution', fontsize=8)
ax1.set_ylabel(r'$C_{diff}$ (F)')
ax1.set_xlabel(r'$\psi_M$ (V)')
ax1.axis([0, 0.5, -10, 60])
ax1.set_xticks(np.arange(0, 0.5, 0.1))
ax1.legend(['GC', 'GCS'], fontsize=8)

# Zoomed in version
ax2.plot(Vm, C_GC2, 'r', lw=0.8)
ax2.plot(Vm, C_GCS2, 'b', lw=1.1, linestyle='--')
ax2.axis([0, 0.5, 0.5, 3.75])
ax2.set_title('Zoomed-in: C$_{diff}$ for 1M KCl solution', fontsize=8)
ax2.set_xlabel(r'$\psi_M$ (V)')
ax2.legend(['GC', 'GCS'], fontsize=8)
fig.tight_layout()
fig.savefig('./Graphs/q3_5b')
plt.close()

#############################################################################
# Q3.6 Compute the ratio of the differential capacitance profiles for GC and GCS models as a function of Psi
# T = 300K for i) 1mM and ii) 1M KCl solutions

ratio_1 = C_GC1/C_GCS1
ratio_2 = C_GC2/C_GCS2

fig, axs = plt.subplots(1, 2)
ax1, ax2 = axs

ax1.plot(Vm*1000, ratio_1, 'r', lw=0.8)
ax1.axis(np.array([0, 0.125*1000, 1.0, 1.5]))
ax1.set_xticks(np.arange(0, 0.127, .025)*1000)
ax1.set_title('1mM KCl Solution', fontsize=10)
ax1.set_xlabel('$\psi _{M}$ (mV)')
ax1.set_ylabel(r'$\frac{C_{GC}}{C_{GCS}}$', fontsize=14)
ax2.plot(Vm*1000, ratio_2, 'b', lw=0.8)
ax2.axis([0, 0.1*1000, 3, 10])
ax2.set_title('1M KCl Solution', fontsize=10)
ax2.set_xlabel('$\psi _{M}$ (mV)')
fig.savefig('./Graphs/q3_6')
plt.close()
