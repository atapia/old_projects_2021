import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import integrate
from scipy.constants import c
from matplotlib.pyplot import Axes

#############################################################################
# Q2.4 Calculate Epsilon(iv) from E''(hv)

# reading files
directory = './data/'
files = ['Au', 'Bromobenzene', 'SiO2']
d = 1
data_au = pd.read_csv(directory + files[0] + '.txt')
data_bromo = pd.read_csv(directory + files[1] + '.txt')
data_si = pd.read_csv(directory + files[2] + '.txt')
data_au, data_bromo, data_si = data_au.to_numpy() * d, \
                               data_bromo.to_numpy() * d, \
                               data_si.to_numpy() * d

# defining some constants
h = 6.62607004e-34  # m2 kg s-1
e = 1.60217662e-19  # Coulombs
kb = 1.38064852e-23


def matsubara(n, T):
    """
    Returns the corresponding Matsubara frequency in eV

    :param n: a scalar or vector in form (n, 1)
    :param T: temperature in K
    :return:
    """
    return 2 * np.pi * kb * T * n[:, ] / h


def kkr(v, w, eps):
    """

        :param v: the desired matsubara frequency to be calculated in [eV]
        :param w: the real frequencies used to measure 𝜀′′.
                   In in this implementation we keep them in [eV]
        :param eps: the imaginary permittivity 𝜀′′
        :return:
        """
    # This method assumes even spacing between frequency measurements
    w = w * e / h
    integrand = w[:, ] * eps[:, ] / (w[:, ] ** 2 + v ** 2)
    return 1 + (2 / np.pi) * integrate.simps(integrand, w, axis=0)


# For hv <= 4eV ----> We get n <= 25 approximately, we calculate at T = 298.15 K

set_T = 298.15
set_n = np.arange(0, 601).reshape((-1, 1))
v = matsubara(set_n, set_T)

# Calculating integrals
eps_au = np.zeros(len(v)).reshape((-1, 1))
eps_bro = eps_au.copy()
eps_si = eps_au.copy()

for i in np.arange(len(v)):
    eps_au[i] = kkr(v[i], data_au[:, 0].reshape((-1, 1)), data_au[:, 1].reshape((-1, 1)))
    eps_bro[i] = kkr(v[i], data_bromo[:, 0].reshape((-1, 1)), data_bromo[:, 1].reshape((-1, 1)))
    eps_si[i] = kkr(v[i], data_si[:, 0].reshape((-1, 1)), data_si[:, 1].reshape((-1, 1)))


#############################################################################
# Q2.7 Calulate G(v) contribution using the Retarded Effect A_123

def rn(d, e, v):
    return 4 * np.pi * d * np.sqrt(e[:, ]) * v[:, ] / c


def Rn(r):
    return (1 + r[:, ]) * np.exp(- r[:, ])


def hamaker_ret(e1, e2, e3, v, d, T=set_T):
    C = 3 * kb * T / 2
    sum0 = 0.5 * Rn(rn(d, e3[0], v[0])) * (e1[0] - e3[0]) * (e2[0] - e3[0]) / ((e1[0] + e3[0]) * (e2[0] + e3[0]))
    sum0 = sum0.reshape((-1, 1))
    summand = Rn(rn(d, e3[1:, ], v[1:, ])) * (e1[1:, ] - e3[1:, ]) * (e2[1:, ] - e3[1:, ]) / (
            (e1[1:, ] + e3[1:, ]) * (e2[1:, ] + e3[1:, ]))
    return C * np.concatenate([sum0, summand], 0)


d = np.array([0.5, 1, 5, 20, 50]) * 1e-9
A_123 = np.zeros([len(d), len(v), 1])
hv = h * v / e

fig, ax = plt.subplots()

for i in np.arange(len(d)):
    A_123[i, :, ] = hamaker_ret(eps_si, eps_au, eps_bro, v, d[i])
    ax.plot(hv, A_123[i, :, ], lw=1.2)

ax.legend(['0.5nm', '1nm', '5nm', '20nm', '50nm'])
ax.set_xlabel(r'$h\nu _n$ (eV)')
ax.set_ylabel(r'$G(\nu _n)$')
fig.savefig('./Graphs/q2_7a1')
plt.close()

fig, ax = plt.subplots()

for i in np.arange(len(d)):
    A_123[i, :, ] = hamaker_ret(eps_si, eps_au, eps_bro, v, d[i])
    ax.plot(hv, A_123[i, :, ], lw=1.2)

ax.legend(['0.5nm', '1nm', '5nm', '20nm', '50nm'])
ax.set_xlabel(r'$h\nu _n$ (eV)')
ax.set_ylabel(r'$G(\nu _n)$')
ax.axis([1, 100, -2e-22, 2e-22])
fig.savefig('./Graphs/q2_7a2')
plt.close()

d = np.linspace(0.5, 100, 10000) * 1e-9
A_123 = np.zeros(len(d))

for i in np.arange(len(d)):
    A_123[i] = np.sum(hamaker_ret(eps_si, eps_au, eps_bro, v, d[i]), 0)

print(A_123)

fig, ax = plt.subplots()
ax.plot(d * 1e9, A_123, lw=1.2, color='r')
ax.set_ylabel('Hamaker Constant A$_{132}$(d)')
ax.set_xlabel('d (n$m$)')
fig.savefig('./Graphs/q2_7b')
plt.close()


def vdw(d):
    return - np.sum(hamaker_ret(eps_si, eps_au, eps_bro, v, d), 0) / (12 * np.pi * d ** 2)


IO_123 = np.zeros(len(d))

d = np.linspace(19.99, 100, 10000) * 1e-9
for i in np.arange(len(d)):
    IO_123[i] = vdw(d[i])

F_123 = - np.gradient(IO_123)

fig, ax = plt.subplots()
ax.plot(d * 1e9, F_123, color='r', lw=1.2)
ax.set_ylabel('F$_{132}$(d)')
ax.set_xlabel('d (n$m$)')
fig.savefig('./Graphs/q2_7c')
plt.close()
