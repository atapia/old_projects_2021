import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import interpolate, integrate
from scipy.constants import nano, speed_of_light
from matplotlib.pyplot import Axes

#############################################################################
# Q2.4 Calculate Epsilon(iv) from E''(hv)

# reading files
directory = './data/'
files = ['Au', 'Bromobenzene', 'SiO2']
c = 1
data_au = pd.read_csv(directory + files[0] + '.txt')
data_bromo = pd.read_csv(directory + files[1] + '.txt')
data_si = pd.read_csv(directory + files[2] + '.txt')
data_au, data_bromo, data_si = data_au.to_numpy() * c, \
                               data_bromo.to_numpy() * c, \
                               data_si.to_numpy() * c

# defining some constants
h = 6.62607004e-34  # m2 kg s-1
e = 1.60217662e-19  # Coulombs
kb = 1.38064852e-23


def matsubara(n, T):
    """
    Returns the corresponding Matsubara frequency in eV

    :param n: a scalar or vector in form (n, 1)
    :param T: temperature in K
    :return:
    """
    return 2 * np.pi * kb * T * n[:, ] / h


def kkr(v, w, eps):
    """

        :param v: the desired matsubara frequency to be calculated in [eV]
        :param w: the real frequencies used to measure 𝜀′′.
                   In in this implementation we keep them in [eV]
        :param eps: the imaginary permittivity 𝜀′′
        :return:
        """
    w = w * e / h
    # This method assumes even spacing between frequency measurements
    integrand = w[:, ] * eps[:, ] / (w[:, ] ** 2 + v ** 2)
    return 1 + (2 / np.pi) * integrate.simps(integrand, w, axis=0)


# For hv <= 4eV ----> We get n <= 25 approximately, we calculate at T = 298.15 K

set_T = 298.15
set_n = np.arange(0, 1001).reshape((-1, 1))
v = matsubara(set_n, set_T)

# Calculating integrals
eps_au = np.zeros(len(v)).reshape((-1, 1))
eps_bro = eps_au.copy()
eps_si = eps_au.copy()

for i in np.arange(len(v)):
    eps_au[i] = kkr(v[i], data_au[:, 0].reshape((-1, 1)), data_au[:, 1].reshape((-1, 1)))
    eps_bro[i] = kkr(v[i], data_bromo[:, 0].reshape((-1, 1)), data_bromo[:, 1].reshape((-1, 1)))
    eps_si[i] = kkr(v[i], data_si[:, 0].reshape((-1, 1)), data_si[:, 1].reshape((-1, 1)))

v = h * v / e

fig, ax = plt.subplots()
ax.semilogy(v, eps_au, lw=1.2)
ax.plot(v, eps_bro, lw=1.2)
ax.plot(v, eps_si, lw=1.2)
ax.plot([4, 4], [0, 1e2], lw=0.9, linestyle='--')
ax.axis([0.5, 25, 1, 1e2])
ax.legend(['Au', 'Bromo', 'SiO2', '$h$\u03BD = 4eV'])
ax.set_xlabel('$h$\u03BD (eV)')
ax.set_ylabel('\u03B5($i$\u03BD)')
fig.savefig('./Graphs/q2_4')
plt.close()

#############################################################################
# Q2.5 Calulate Hamaker Constant from data and its G(v) contribution

def hamaker(e1, e2, e3, T=set_T):
    C = 3 * kb * T / 2
    sum0 = 0.5 * (e1[0] - e3[0]) * (e2[0] - e3[0]) / ((e1[0] + e3[0]) * (e2[0] + e3[0]))
    sum0 = sum0.reshape((-1, 1))
    summand = (e1[1:, ] - e3[1:, ]) * (e2[1:, ] - e3[1:, ]) / ((e1[1:, ] + e3[1:, ]) * (e2[1:, ] + e3[1:, ]))
    return C * np.concatenate([sum0, summand], 0)

A_123 = hamaker(eps_si, eps_au, eps_bro)
print('\nThe calculated Hamaker Constant A_123 is: {:.5} for n: {:.1e}'.format(A_123.sum(), set_n[-1, 0]))

# Plot G(Vn) contribution

fig, ax = plt.subplots()
ax.plot(v, A_123, lw=1.2, color='r')
ax.axis([-5, np.max(v)+5, -0.8e-21, 0.4e-21])
ax.legend(['$ G(\u03BD_n) $'], loc='upper right')
ax.set_xlabel(r'$h\nu _n$ (eV)')
ax.set_ylabel('$G(\u03BD_n) $')
fig.savefig('./Graphs/q2_5')
plt.close()

# Plot convergence of Hamaker as function of n for completeness
# We obtained the values of the H constant ahead of time by running this same script and changing the variabne "set_n"
# to different magnitudes

# The searched variable values were
n_axis = np.array([1e2, 1e3, 1e4, 1e5, 1e6])
hamaker_axis = np.array([-2.63597, 5.79583, 7.54716, 7.55053, 7.55024]) * 1e-19
hamaker_axis = np.array([3.2518e-21, 2.33e-20, 2.3552e-20, 2.3553e-20,  2.3553e-20])
f = interpolate.interp1d(np.log(n_axis), hamaker_axis, kind='cubic')
x_int = np.linspace(1e2, 1e6, 1000)
h_int = f(np.log(x_int))

fig, ax = plt.subplots()
ax.plot(x_int, h_int, lw=0.9, linestyle='--', color='r')
ax.scatter(n_axis, hamaker_axis, color='r')
ax.axis([50, 1.5e6, -0e-20, 4e-20])
ax.set_xscale('log')
ax.legend(['Approx. Interpolation', 'Evaluated A$_{123}$($n$)'], loc='best')
ax.annotate('Coverges to:\n' r'A$_{123}$($n\rightarrow\infty$)$\approx$ 2.3553e-20', xy=(x_int[-1], hamaker_axis[-1]), xytext=(10000, 3e-20),
            color='k', arrowprops=dict(arrowstyle='->', connectionstyle="angle3,angleA=0,angleB=-90", color='k'))
ax.set_xlabel('Value of $n$')
ax.set_ylabel('Hamaker Constant A$_{123}$')
fig.savefig('./Graphs/q2_5_converge')
plt.close()

#############################################################################
# Q2.6 Qualitatively assess the retardation effect and whether is is negligible

v = np.array([0.1, 50]) * e / h
ds = np.array([0.5, 50])* nano
w = 2 * np.pi * v
t = ds / speed_of_light

print('\n\nQ2.6: assess the retardation effect and whether is is negligible:\n')
for i in [0,1]:
    for j in [0,1]:
        print('1/\u03C9 is: {:.3e}s and \u03C4: {:.3e}s'.format(1/w[i], t[j]))









