import matplotlib.pyplot as plt
import numpy as np
import os
import math
import pandas as pd
from plot_dist import plot_droplet_distribution

directory = '/Users/andres/Dev/Repositories/Pycharm Projects/DropletDistribution/Data/'
columns = ['x center', 'y center', 'diameter (um)', 'strength']

# d = pd.read_csv(directory + 'day1_10%_UW' + '.csv', names=columns)
# image = plt.imread(directory + '../Images/' + 'day1_10%_UW' + '.png')
# figure = plot_droplet_distribution(d, image, columns, 'TEST')
# plt.show()

for file in sorted(os.listdir(directory)):
    filename = file[:-4]
    print(filename)

    details = filename.split('_')
    if details[2] == 'TE':
        phase = 'TE Buffer'
    elif details[2] == 'UW':
        phase = 'Ultra Pure Water'
    else:
        phase = 'Nigrosin (1mg/mL)'

    title = f'{phase}: Day {details[0][-1]} HFE {details[1]}'

    currentDF = pd.read_csv(directory + file, names=columns)
    currentImage = plt.imread(directory + '../Images/' + filename + '.png')
    figure = plot_droplet_distribution(currentDF, currentImage, columns, title)
    figure.savefig(directory + '../Figures/figure_' + filename + '.png', dpi=600)
    plt.close(figure)
