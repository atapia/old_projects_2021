import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import seaborn as sns
from scipy import stats


def plot_droplet_distribution(df, image, columns, title):
    plt.rcParams.update({'font.family': 'Arial'})

    diameter = df[columns[2]]
    fig, axs = plt.subplots(1, 2, figsize=(10, 5))
    [ax1, ax3] = axs

    if ('NG' in title) or ('Day 4' in title):
        ratio = (62 / 70)
    elif 'Day 2' in title:
        ratio = (32 / 70)
    else:
        ratio = (40 / 70)

    ax1.imshow(image, cmap='Greys_r')
    for x, y, r in zip(df[columns[0]], df[columns[1]], diameter / 2):
        circ = Circle((x, y), r * ratio, fill=False, edgecolor='seagreen', linestyle='--', linewidth=0.75, alpha=.8)
        ax1.add_patch(circ)

    ax1.set_title(title)
    ax1.set_xticks([])
    ax1.set_yticks([])

    # ax2.hist(diameter, color='seagreen', rwidth=0.95, alpha=0.75, bins=20)
    # ax2.set_title('Droplet Size Histogram')
    # ax2.set_ylabel('Counts')
    # ax2.set_xlabel('Droplet Diameter (um)')
    # ax2.set_xlim([55, 125])

    ae, loce, scalee = stats.skewnorm.fit(diameter)
    x = np.linspace(60, 120, 100)
    y_p = stats.skewnorm.pdf(x, ae, loce, scalee)

    #### Violin Plot
    # sns.violinplot(x=diameter, ax=ax3, color='mediumseagreen', alpha=0.65)
    # ax3.collections[0].set_alpha(0.75)
    # ax3.set_title('Fitted Probability Density Function')
    # ax3.set_xlabel('Droplet Diameter (um)')
    # ax3.set_xlim([55, 125])
    # ax3.spines['top'].set_visible(False)
    # ax3.spines['right'].set_visible(False)
    # ax3.spines['left'].set_visible(False)
    # ax3.set_yticks([])

    ### Density plot
    med = np.median(sorted(diameter))
    q75, q25 = np.percentile(diameter, [75, 25])
    sns.kdeplot(diameter, ax=ax3, shade=True, color='mediumseagreen')
    ax3.vlines([q25, med, q75], y_p.min(), y_p.max() / 2, colors=['mediumseagreen', 'seagreen', 'mediumseagreen'], )
    ax3.set_alpha(0.75)
    ax3.set_title('Density Function')
    ax3.set_xlabel('Droplet Diameter (um)')
    ax3.set_ylabel('Density')
    ax3.set_xlim([55, 125])
    # ax3.spines['top'].set_visible(False)
    # ax3.spines['right'].set_visible(False)
    # ax3.spines['left'].set_visible(False)

    fig.tight_layout()
    return fig
