# Importing packages
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import numpy as np
import os
import math
import pandas as pd

# Variables for the three different data types
header_PL = 17
header_Abs = 17
header_ref = 17
# Variables for PL
p_pl = 0.1
max_pl = 530
cutoff_pl = 490
binsize_pl = 10
# Variables for Abs
Abs_max = 480
binsize_abs = 10
p_abs = 0.01

# Declaring the folder to look at for data import
directory = '/Users/andres/Dev/Repositories/Pycharm Projects/PDots_Data_Processing/05.03.2020_F8BT_absorption/Absorption/'


# Reading in the data from the text files
# Firstly declaring three empty arrays for PL data, absorption data, reference data

# Importing the data via a Pandas Dataframe and then converting it into a numpy array
def data_comp(directory):
    dataAbs = []
    for file in os.listdir(directory):
        if file.endswith('.csv'):
            if file.startswith('P'):
                header = header_PL
                # These two lines are reading in the data with pandas and then converting it into numpy arrays,
                # because it is faster The lines below would od the same for a numpy array, which is slower but works
                # just as well
                spectrum_df = pd.read_csv(directory + file,  # delimiter='\t',
                                          dtype=np.float64, skiprows=[0, 3]).T
                spectrum = spectrum_df.to_numpy()
                dataPL.append(spectrum)
            # spectrum = np.genfromtxt(directory + file, skip_header=header, skip_footer=1, unpack=True)
            elif file.startswith('re'):
                header = header_ref
                spectrum_df = pd.read_csv(directory + file,  # delimiter='\t',
                                          dtype=np.float64).T
                spectrum = spectrum_df.to_numpy()
                ref.append(spectrum)
            else:
                header = header_Abs
                spectrum_df = pd.read_csv(directory + file,  # delimiter=',\t',
                                          dtype=np.float64, skiprows=[0, 3]).T
                spectrum = spectrum_df.to_numpy()
                dataAbs.append(spectrum)
                print('Shape of function: ', np.shape(dataAbs))
    return dataAbs

peak_abs = []
for i in np.arange(1, 5):
    data = []
    print(peak_abs)
    print(i)
    data = data_comp(directory + '0' + np.array2string(i) + '/')
    print(np.shape(data))
    peak = 0
    for j in np.arange(np.size(data, 0)):
        peak += np.max(data[j][1])
    peak_abs.append(peak / np.size(data, 0))

print(peak_abs)
x = (np.array([1 / 10, 1 / 20, 1 / 50, 1 / 100])).reshape((-1, 1))
model = LinearRegression().fit(x, peak_abs)
slope = model.coef_[0]
C = model.intercept_

# Drawing the line
X_line = np.linspace(0, .1, 100).reshape((-1, 1))
y_pred = model.predict(X_line)

fig1 = plt.figure(1)
line, = plt.plot(X_line, y_pred, linewidth=0.7, color='r')
plt.scatter(x, peak_abs)
plt.legend(('LinReg \nR^2: {:.3f} \ny = {:.5f}x + {:.5f}'.format(model.score(x, peak_abs), slope, C), 'Data'))
plt.xlabel('Relative Concentration (ratio)')
plt.ylabel('Absorbance (AU)')
plt.title('')
fig1.savefig(directory[:-1] + 'Beerlaw')
