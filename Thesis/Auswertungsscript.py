import matplotlib.pyplot as plt
import numpy as np
from numpy.lib import recfunctions as rfn
import h5py
import os
import math
from matplotlib import colors

cmap=colors.ListedColormap(['C0', 'C9'])
plt.rcParams.update({'figure.figsize': (12, 9), 'font.size': 16})
palette = plt.get_cmap('viridis', 5)


max_PL = 580
cutoff_PL_left = 500
cutoff_PL_right = 600
cutoff_Abs_left = 300
max_Abs = 900
ps = {'Abs': 0.01, 'PL': 10000}

# Adress to save the processed data in
folder_name = '/Users/andres/Documents/PycharmProjects/Thesis/Data/2020-12-15/data/Analysis/'

# Adress of source data
base = '/Users/andres/Documents/PycharmProjects/Thesis/Data/2020-12-15/data/'

# Redundancy to make sure destination folder exists
if not os.path.exists(folder_name):
    os.makedirs(folder_name)


def oil_filter(data, wavelength, mode, binsize=10):
    if mode=='PL':
        ix_of_cutoff_left = np.argmax(cutoff_PL_left < data[0,0,:])
        ix_of_cutoff_right = np.argmax(cutoff_PL_right < data[0,0,:])
        data = data[:,:,ix_of_cutoff_left:ix_of_cutoff_right]
    if mode=='Abs':
        ix_of_cutoff_left = np.argmax(cutoff_Abs_left < data[0,0,:])
        data = data[:,:,ix_of_cutoff_left:]
    ix_of_wavelength = np.argmax(wavelength < data[0,0,:])
    peakval = data[:,1,ix_of_wavelength]
    histy, histx = np.histogram(peakval, bins=np.arange(np.min(peakval), np.max(peakval), binsize))
    ix_middle = np.argmax(histx > np.mean(peakval))

    if mode=='Abs':
        iy_peak = np.argmax(histy[:ix_middle])
    elif mode=='PL':
        #DOESN'T WORK, HAS TO BE FIXED!
        #print(ix_middle, len(histy), np.mean(peakval))
        iy_peak = np.argmax(histy[ix_middle:])
    threshold_low = histx[iy_peak] * (1 - ps[mode])
    threshold_high = histx[iy_peak] * (1 + ps[mode])
    thresh_filter = (threshold_low < peakval) & (peakval < threshold_high)
    dropletspectra = data[thresh_filter]
    filtered_data = np.mean(dropletspectra, axis=0)
    plotting(peakval, thresh_filter, mode)
    return filtered_data


def plotting(peakval, thresh_filter, mode):
    ax_scatter.clear()
    ax_hist.clear()
    x_scatter = np.arange(len(peakval))
    ax_scatter.scatter(x_scatter, peakval,c=cmap(thresh_filter.astype(int)), s=5)
    ax_hist.hist(peakval, bins=100, orientation='horizontal', color='C0')
    if mode =='Abs':
        ax_scatter.set_ylabel('Intensity at max. Transmission Intensity [a.u.]')
    elif mode =='PL':
        ax_scatter.set_ylabel('Intensity at max. PL Intensity [a.u.]')
    ax_scatter.set_title('{} Intensity distribution'.format(mode))
    #fig_hist.savefig('{}/{}_Intensity_distribution_{}'.format(folder_name, mode, filename))


PL_all = []
Filename_order = []
Abs_all = []

fig_hist, (ax_scatter, ax_hist) = plt.subplots(ncols=2, gridspec_kw={'width_ratios': (2, 1), 'wspace': 0.025})
ax_hist.set_yticks([])
ax_hist.set_yticklabels([])
ax_hist.set_xlabel('Frequency')
ax_scatter.set_xlabel('Counts')
ax_hist.set_title('Histogram')

## This two figure object are designates fpr the individual PL/Abs spectra for each run ##

fig_abs, fx = plt.subplots()
fx.set_ylabel('Absorbance')
fx.set_xlabel('Wavelength [nm]')
fig_pl, gx = plt.subplots()
gx.set_ylabel('Photoluminescence Intensity [a.u.]')
gx.set_xlabel('Wavelength [nm]')


for (filename, h5file) in [(file[:-3], base+file)
                           for file in sorted(os.listdir(base)) if file.endswith('.h5')]:
    with h5py.File(h5file, 'r') as f:
        #this is reading in the data

        ##These two arrays are the shape of the data files.
        ## First is number of spectra taken, 2 is a default of spectra files,
        # and last number is the number of points taken per measurement
        #data_Abs = np.empty((1500, 2, 1044))
        data_PL = np.empty((200, 2, 2048))

        ## Read data for absrotion
        #for i, data in enumerate(f.get('Abs', dict()).values()):
        #    data.read_direct(data_Abs[i,:,:])

        ##This is the analysis of the Absorption data

        #print(data_Abs)
        #f_data_Abs = oil_filter(data_Abs, max_Abs, 'Abs')
        #if data.name.split('/')[-1].startswith('ref'):
        #    f_ref_Abs = np.copy(f_data_Abs)
        #else:
        #    absorbance = np.log10(f_ref_Abs[1,:]/f_data_Abs[1,:])
        #    wavelengths = f_data_Abs[0,:]
        #    spectrum = np.array([wavelengths, absorbance])
        #    spectrum_t = np.transpose(spectrum)
        #    fx.clear()
        #    fx.plot(*spectrum)
        #    fig_abs.savefig('{}\\Abs_{}'.format(folder_name, filename))
        #    np.savetxt('{}\\Abs_{}.txt'.format(folder_name, filename), spectrum_t, delimiter='\t')
        #    Abs_all.append((spectrum, dict(data.attrs.items())))

                #This is the analysis of the PL data
        for i, data in enumerate(f.get('PL', dict()).values()):
            data.read_direct(data_PL[i,:,:])

        print(filename)

        if data.name.split('/')[-1].startswith('ref'):
            pass
        else:
            f_data_PL = oil_filter(data_PL, max_PL, 'PL')
            f_data_PL[1,:] -= np.min(f_data_PL[1,:])
            #f_data_PL[1,:] /= np.max(f_data_PL[1,:])
            spectrum_t = np.transpose(f_data_PL)
            #gx.clear()
            #gx.plot(*f_data_PL)

            ## Saving the individual spectra for each run in both graph and text form
            #fig_pl.savefig('{}/PL_{}'.format(folder_name, filename))
            #np.savetxt('{}/PL_{}.txt'.format(folder_name, filename), spectrum_t, delimiter='\t')

            # script reads file in unknown order, we save that order for future referencing
            Filename_order.append(filename[-2:])

            ## We are able to save the attributes since in the loop varaible data the last spectra file is stored.
            ## All spectra files for each h5 files contains the same attr info.
            PL_all.append((f_data_PL, dict(data.attrs.items())))

#########################################################################################################################
## Averaged Absorption plotting section

#figi, cx = plt.subplots()
#cx.plot([], [], color='white', label=Abs_all[0][1]['Substances'])
#count = 0
#for spectrum, attr in Abs_all:
#    cx.plot(*spectrum, label=attr['Flowrates'], color=palette(count))
#    count += 1
#cx.legend(loc=0, prop={'size': 8})
#cx.set_ylabel('Absorbance')
#cx.set_xlabel('Wavelength [nm]')
#thedate = Abs_all[0][1]['Datetime']
#thetitle = Abs_all[0][1]['Experiment name'] + '   ' + str(thedate).split('T')[0].strip()
#cx.set_title(thetitle)
#figi.savefig('{}\\Abs_all'.format(folder_name))

#########################################################################################################################

## Plot the averaged PL data

fig2, bx = plt.subplots()
bx.plot([], [], color='white', label=PL_all[0][1]['Substances'])
count = 0
for spectrum, attr in PL_all:
    bx.plot(*spectrum, label=attr['Flowrates'], color=palette(count))
    count += 1
bx.legend(loc=1, prop={'size': 10})
bx.set_ylabel('Photoluminescence Intensity [a.u.]')
bx.set_xlabel('Wavelength [nm]')
thedate = PL_all[0][1]['Datetime']
thetitle = PL_all[0][1]['Experiment name'] + '   ' + str(thedate).split('T')[0].strip()
bx.set_title(thetitle)
#fig2.savefig('{}/PL_all'.format(folder_name))


#########################################################################################################################
###### Andres Section
## Plot individual
# Get the unique flow configurations
flows = []

# Peak Position in Spectrum File (index found by looking at text file)
F8_pos = 92
MEH_pos = 230
peaks = []

for i in np.arange(1,6):
    flows.append(PL_all[i][1]['Flowrates'])

for i in np.arange(5):

    ## Figure object to plot spectra
    fig2, bx = plt.subplots()
    bx.plot([], [], color='white', label='Experiment Run No.')
    bx.set_ylabel('Photoluminescence Intensity [a.u.]')
    bx.set_xlabel('Wavelength [nm]')
    thedate = PL_all[0][1]['Datetime']
    thetitle = 'Flows ' + np.array2string(flows[i])  + '   ' + str(thedate).split('T')[0].strip()
    bx.set_title(thetitle)

    # Paellete Counter (resets after 5 runs, inner if statement)
    count = 0
    # Experiment counter *resets after 26 runs, for loop)
    experiment = 0

    # Peak Intensity Storage for F8BT and MEHPPV
    F8_pk = 0
    MEH_pk = 0

    for spectrum, attr in PL_all:
        #print([attr['Flowrates'], flows[i]])

        # Condtional check to see if runs are identical (checks polymer flow only)
        if attr['Flowrates'][3] == flows[i][3]:
            bx.plot(*spectrum, label=Filename_order[experiment], color=palette(count))

            F8_pk, MEH_pk = spectrum[1][F8_pos], spectrum[1][MEH_pos]
            peaks.append((int(Filename_order[experiment]), i+1, F8_pk, MEH_pk))

            count += 1
        experiment += 1

    bx.legend(loc=1, prop={'size': 10})

    fig2.savefig('{}/{}'.format(folder_name, 'Condition' + np.array2string(i+1)))

dtype = [('Experiment', int), ('Condition', int), ('F8', float), ('MEH', float)]
peaks = np.array(peaks[:], dtype=dtype)
peaks = np.sort(peaks, order='Experiment')
print(np.shape(peaks))
peaks = rfn.structured_to_unstructured(peaks)

fig3, axs = plt.subplots(1, 3, figsize=(15, 5))
ax3, ax2, ax1 = axs

# Ratio Graph
ax1.set_ylabel('Intensity Ratio at 530/580 nm')
ax1.set_xlabel('Polymer Flowrate (μL/min)')
thedate = PL_all[0][1]['Datetime']
thetitle = str(thedate).split('T')[0].strip()
ax1.set_title(thetitle)

# F8Bt Graph
ax2.set_ylabel('Intensity [a.u.]')
ax2.set_xlabel('Polymer Flowrate (μL/min)')
thedate = PL_all[0][1]['Datetime']
thetitle = 'F8BT peak'
ax2.set_title(thetitle)

# MEHPPV Graph
ax3.set_ylabel('Intensity [a.u.]')
ax3.set_xlabel('Polymer Flowrate (μL/min)')
thedate = PL_all[0][1]['Datetime']
thetitle = 'MEHPPV peak'
ax3.set_title(thetitle)

for i in np.arange(5):
    start = i*5
    end = start + 5
    ax1.scatter(peaks[start:end, 1], peaks[start:end, 2]/peaks[start:end, 3],
                label='Cycle {}'.format(i+1), color=palette(i))
    ax2.scatter(peaks[start:end, 1], peaks[start:end, 2],
                label='Cycle {}'.format(i+1), color=palette(i))
    ax3.scatter(peaks[start:end, 1], peaks[start:end, 3],
                label='Cycle {}'.format(i+1), color=palette(i))

ax1.legend(loc='best', prop={'size': 10})

fig3.tight_layout()
fig3.savefig('{}/{}'.format(folder_name, 'Intensity Ratio'))


# plt.show()