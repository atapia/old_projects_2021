Data from PL_2polymer_F8BT_MEHPPV_USB2H096331_0131.txt Node
Date: Tue Dec 15 16:40:40 CET 2020
User: spadmin
Spectrometer: USB2+H09633
Trigger mode: 0
Integration Time (sec): 1.000000E-2
Scans to average: 1
Electric dark correction enabled: true
Nonlinearity correction enabled: false
Boxcar width: 0
XAxis mode: Wavelengths
Number of Pixels in Spectrum: 2048
>>>>>Begin Spectral Data<<<<<
174.027	-100.84
174.405	-100.84
174.783	-100.84
175.162	-100.84
175.54	-55.65
175.918	-121.18
176.296	-35.31
176.674	-132.48
177.052	-75.99
177.43	-98.58
177.808	-71.47
178.186	-3.67
178.564	-33.05
178.942	-78.25
179.32	27.97
179.698	37
180.076	-1.41
180.453	52.82
180.831	116.1
181.209	66.38
181.587	107.06
181.965	122.88
182.342	152.26
182.72	183.89
183.098	240.39
183.475	240.39
183.853	240.39
184.231	242.65
184.608	276.55
184.986	258.47
185.363	251.69
185.741	217.79
186.118	213.27
186.496	299.14
186.873	299.14
187.251	251.69
187.628	314.96
188.006	310.44
188.383	287.85
188.76	305.92
189.138	310.44
189.515	312.7
189.892	265.25
190.27	278.81
190.647	296.88
191.024	348.86
191.401	324
191.778	391.8
192.155	292.37
192.533	312.7
192.91	314.96
193.287	292.37
193.664	305.92
194.041	296.88
194.418	269.77
194.795	296.88
195.172	330.78
195.549	220.05
195.926	337.56
196.302	262.99
196.679	324
197.056	337.56
197.433	330.78
197.81	330.78
198.187	362.42
198.563	337.56
198.94	244.91
199.317	296.88
199.693	292.37
200.07	285.59
200.447	292.37
200.823	235.87
201.2	231.35
201.576	292.37
201.953	290.11
202.329	314.96
202.706	337.56
203.082	256.21
203.459	301.4
203.835	303.66
204.212	253.95
204.588	281.07
204.964	258.47
205.341	346.6
205.717	260.73
206.093	281.07
206.469	296.88
206.846	235.87
207.222	267.51
207.598	274.29
207.974	339.82
208.35	272.03
208.726	333.04
209.102	301.4
209.479	231.35
209.855	269.77
210.231	283.33
210.607	312.7
210.982	355.64
211.358	285.59
211.734	333.04
212.11	262.99
212.486	299.14
212.862	387.28
213.238	314.96
213.614	294.63
213.989	262.99
214.365	337.56
214.741	296.88
215.116	249.43
215.492	330.78
215.868	324
216.243	278.81
216.619	269.77
216.995	292.37
217.37	290.11
217.746	220.05
218.121	260.73
218.497	299.14
218.872	272.03
219.247	310.44
219.623	333.04
219.998	281.07
220.374	272.03
220.749	317.22
221.124	290.11
221.499	339.82
221.875	303.66
222.25	310.44
222.625	317.22
223	296.88
223.376	346.6
223.751	283.33
224.126	305.92
224.501	364.68
224.876	301.4
225.251	319.48
225.626	342.08
226.001	292.37
226.376	314.96
226.751	305.92
227.126	305.92
227.501	285.59
227.875	344.34
228.25	276.55
228.625	242.65
229	249.43
229.375	362.42
229.749	342.08
230.124	296.88
230.499	287.85
230.873	303.66
231.248	317.22
231.623	314.96
231.997	251.69
232.372	310.44
232.746	312.7
233.121	398.58
233.495	267.51
233.87	319.48
234.244	281.07
234.619	317.22
234.993	317.22
235.367	328.52
235.742	310.44
236.116	328.52
236.49	314.96
236.865	321.74
237.239	299.14
237.613	276.55
237.987	357.9
238.361	247.17
238.736	260.73
239.11	260.73
239.484	285.59
239.858	348.86
240.232	269.77
240.606	324
240.98	262.99
241.354	312.7
241.728	371.46
242.102	319.48
242.475	373.72
242.849	312.7
243.223	281.07
243.597	303.66
243.971	296.88
244.344	290.11
244.718	308.18
245.092	319.48
245.466	296.88
245.839	244.91
246.213	274.29
246.586	339.82
246.96	292.37
247.334	310.44
247.707	317.22
248.081	333.04
248.454	418.92
248.828	326.26
249.201	303.66
249.574	294.63
249.948	287.85
250.321	292.37
250.694	324
251.068	342.08
251.441	319.48
251.814	265.25
252.187	301.4
252.561	312.7
252.934	333.04
253.307	287.85
253.68	385.02
254.053	387.28
254.426	335.3
254.799	296.88
255.172	314.96
255.545	314.96
255.918	360.16
256.291	262.99
256.664	342.08
257.037	274.29
257.41	371.46
257.782	290.11
258.155	328.52
258.528	312.7
258.901	337.56
259.273	319.48
259.646	278.81
260.019	366.94
260.391	317.22
260.764	351.12
261.137	364.68
261.509	319.48
261.882	333.04
262.254	342.08
262.627	351.12
262.999	362.42
263.372	251.69
263.744	389.54
264.116	267.51
264.489	355.64
264.861	317.22
265.233	335.3
265.606	353.38
265.978	380.5
266.35	360.16
266.722	308.18
267.094	330.78
267.467	324
267.839	328.52
268.211	328.52
268.583	355.64
268.955	335.3
269.327	353.38
269.699	335.3
270.071	294.63
270.443	378.24
270.815	335.3
271.186	394.06
271.558	430.21
271.93	342.08
272.302	299.14
272.674	369.2
273.045	296.88
273.417	380.5
273.789	260.73
274.16	355.64
274.532	398.58
274.904	351.12
275.275	366.94
275.647	369.2
276.018	403.1
276.39	360.16
276.761	378.24
277.133	339.82
277.504	398.58
277.875	348.86
278.247	362.42
278.618	330.78
278.989	436.99
279.361	430.21
279.732	382.76
280.103	423.44
280.474	328.52
280.846	387.28
281.217	380.5
281.588	357.9
281.959	364.68
282.33	382.76
282.701	380.5
283.072	342.08
283.443	364.68
283.814	405.36
284.185	337.56
284.556	366.94
284.927	403.1
285.297	398.58
285.668	382.76
286.039	432.47
286.41	455.07
286.781	380.5
287.151	380.5
287.522	362.42
287.893	346.6
288.263	396.32
288.634	405.36
289.004	366.94
289.375	405.36
289.745	382.76
290.116	301.4
290.486	335.3
290.857	366.94
291.227	403.1
291.598	360.16
291.968	375.98
292.338	360.16
292.709	389.54
293.079	353.38
293.449	418.92
293.819	391.8
294.189	409.88
294.56	330.78
294.93	371.46
295.3	378.24
295.67	389.54
296.04	380.5
296.41	380.5
296.78	351.12
297.15	405.36
297.52	414.4
297.89	330.78
298.259	339.82
298.629	351.12
298.999	382.76
299.369	412.14
299.739	346.6
300.108	344.34
300.478	412.14
300.848	355.64
301.217	394.06
301.587	328.52
301.957	398.58
302.326	389.54
302.696	427.95
303.065	333.04
303.435	324
303.804	369.2
304.174	387.28
304.543	396.32
304.912	450.55
305.282	403.1
305.651	369.2
306.02	396.32
306.39	394.06
306.759	407.62
307.128	378.24
307.497	450.55
307.866	500.27
308.235	362.42
308.604	375.98
308.974	355.64
309.343	391.8
309.712	371.46
310.08	473.15
310.449	405.36
310.818	371.46
311.187	398.58
311.556	457.33
311.925	421.18
312.294	412.14
312.662	441.51
313.031	432.47
313.4	391.8
313.769	326.26
314.137	342.08
314.506	337.56
314.874	394.06
315.243	375.98
315.612	423.44
315.98	387.28
316.349	371.46
316.717	394.06
317.085	434.73
317.454	448.29
317.822	443.77
318.191	457.33
318.559	439.25
318.927	375.98
319.295	450.55
319.664	382.76
320.032	432.47
320.4	360.16
320.768	416.66
321.136	389.54
321.504	412.14
321.872	405.36
322.24	403.1
322.608	373.72
322.976	430.21
323.344	459.59
323.712	364.68
324.08	418.92
324.448	382.76
324.816	409.88
325.183	385.02
325.551	409.88
325.919	418.92
326.287	423.44
326.654	432.47
327.022	470.89
327.389	400.84
327.757	427.95
328.125	409.88
328.492	484.45
328.86	430.21
329.227	477.67
329.594	432.47
329.962	396.32
330.329	450.55
330.697	470.89
331.064	416.66
331.431	409.88
331.798	466.37
332.166	464.11
332.533	525.13
332.9	436.99
333.267	498.01
333.634	457.33
334.001	511.57
334.368	439.25
334.735	373.72
335.102	446.03
335.469	405.36
335.836	470.89
336.203	479.93
336.57	418.92
336.937	425.7
337.304	543.21
337.67	475.41
338.037	536.43
338.404	488.97
338.771	493.49
339.137	543.21
339.504	436.99
339.87	457.33
340.237	461.85
340.604	479.93
340.97	457.33
341.337	473.15
341.703	527.39
342.069	414.4
342.436	450.55
342.802	507.05
343.169	439.25
343.535	446.03
343.901	479.93
344.267	443.77
344.634	473.15
345	509.31
345.366	540.95
345.732	507.05
346.098	527.39
346.464	529.65
346.83	543.21
347.196	461.85
347.562	507.05
347.928	516.09
348.294	583.88
348.66	536.43
349.026	491.23
349.392	468.63
349.757	522.87
350.123	554.51
350.489	493.49
350.855	536.43
351.22	554.51
351.586	617.78
351.951	574.84
352.317	545.47
352.683	504.79
353.048	484.45
353.414	500.27
353.779	568.06
354.144	507.05
354.51	538.69
354.875	552.25
355.24	536.43
355.606	577.1
355.971	669.76
356.336	531.91
356.701	543.21
357.067	554.51
357.432	559.02
357.797	540.95
358.162	601.96
358.527	599.7
358.892	599.7
359.257	617.78
359.622	633.6
359.987	640.38
360.352	658.46
360.717	660.72
361.082	592.92
361.446	669.76
361.811	660.72
362.176	658.46
362.541	678.8
362.905	703.65
363.27	685.58
363.635	629.08
363.999	703.65
364.364	708.17
364.728	775.97
365.093	748.85
365.457	733.03
365.822	751.11
366.186	726.25
366.55	712.69
366.915	721.73
367.279	714.95
367.643	687.84
368.008	780.49
368.372	832.46
368.736	739.81
369.1	794.05
369.464	800.83
369.828	769.19
370.192	814.39
370.556	900.26
370.92	832.46
371.284	782.75
371.648	830.2
372.012	918.34
372.376	961.27
372.74	900.26
373.104	938.68
373.468	1026.81
373.831	1060.71
374.195	1094.6
374.559	1108.16
374.922	1069.75
375.286	1090.08
375.65	1173.7
376.013	1185
376.377	1185
376.74	1212.12
377.104	1331.89
377.467	1322.85
377.83	1383.86
378.194	1469.74
378.557	1548.83
378.92	1668.6
379.284	1736.4
379.647	1853.91
380.01	1912.66
380.373	2032.43
380.736	2131.86
381.1	2332.99
381.463	2443.72
381.826	2604.17
382.189	2821.11
382.552	3042.58
382.915	3212.06
383.278	3381.55
383.64	3758.94
384.003	4018.82
384.366	4421.07
384.729	4848.18
385.092	5155.51
385.454	5544.2
385.817	6005.21
386.18	6547.57
386.542	7117.04
386.905	7770.13
387.268	8764.46
387.63	9756.52
387.993	10425.43
388.355	11234.45
388.718	12633.28
389.08	13399.37
389.442	14545.1
389.805	16061.44
390.167	17329.21
390.529	19376.61
390.892	21489.55
391.254	22436.42
391.616	24323.37
391.978	27179.79
392.34	30320.95
392.702	31410.19
393.064	34146.84
393.426	36508.36
393.788	38973.83
394.15	41204.28
394.512	43886.7
394.874	46365.73
395.236	49484.29
395.598	52232.24
395.96	54406.2
396.321	56681.84
396.683	58582.36
397.045	60767.61
397.406	62837.61
397.768	64324.58
398.13	65081.62
398.491	65081.62
398.853	65081.62
399.214	65081.62
399.576	65081.62
399.937	65081.62
400.299	65081.62
400.66	65081.62
401.021	65081.62
401.383	65081.62
401.744	65081.62
402.105	65081.62
402.466	65081.62
402.827	65081.62
403.189	65081.62
403.55	65081.62
403.911	65081.62
404.272	65081.62
404.633	65081.62
404.994	65081.62
405.355	65081.62
405.716	65081.62
406.077	65081.62
406.437	65081.62
406.798	65081.62
407.159	65081.62
407.52	65081.62
407.88	65081.62
408.241	65081.62
408.602	65081.62
408.962	65081.62
409.323	65081.62
409.684	65081.62
410.044	65081.62
410.405	65081.62
410.765	65081.62
411.125	65081.62
411.486	65081.62
411.846	65081.62
412.206	65081.62
412.567	65081.62
412.927	65081.62
413.287	65081.62
413.647	65081.62
414.007	65081.62
414.368	65081.62
414.728	65081.62
415.088	65081.62
415.448	65081.62
415.808	65081.62
416.168	65081.62
416.528	65081.62
416.887	65081.62
417.247	65081.62
417.607	65081.62
417.967	65081.62
418.327	64595.76
418.686	63655.67
419.046	62636.49
419.406	61576.63
419.765	61282.85
420.125	59142.8
420.484	57875.03
420.844	57163.19
421.203	55524.81
421.563	53662.71
421.922	53314.7
422.282	50648.1
422.641	48137.43
423	46469.68
423.36	44722.84
423.719	41927.43
424.078	39136.54
424.437	37086.88
424.796	35886.91
425.155	34431.58
425.515	31844.08
425.874	31026.02
426.233	28336.83
426.592	25927.85
426.95	25826.16
427.309	23975.36
427.668	23751.64
428.027	21618.36
428.386	21118.94
428.745	19189.04
429.103	17928.06
429.462	17340.5
429.821	16045.62
430.179	15270.5
430.538	14809.5
430.897	14285.22
431.255	13480.72
431.614	12764.35
431.972	12380.18
432.33	11740.65
432.689	11788.11
433.047	11026.55
433.405	10203.97
433.764	9756.52
434.122	9600.6
434.48	9019.82
434.838	8741.86
435.197	8298.93
435.555	8102.33
435.913	7636.8
436.271	7291.05
436.629	7207.44
436.987	6714.8
437.345	6301.25
437.703	6143.06
438.061	5853.8
438.418	5496.75
438.776	5266.25
439.134	5232.35
439.492	4814.28
439.849	4667.39
440.207	4525.02
440.565	4260.62
440.922	4115.99
441.28	3844.81
441.637	3786.06
441.995	3697.93
442.352	3562.34
442.71	3392.85
443.067	3227.88
443.425	3148.79
443.782	3017.72
444.139	2857.27
444.496	2807.55
444.854	2760.1
445.211	2615.47
445.568	2588.35
445.925	2414.34
446.282	2344.29
446.639	2290.05
446.996	2192.88
447.353	2305.87
447.71	2290.05
448.067	2122.83
448.424	2023.39
448.781	1978.2
449.137	1971.42
449.494	1806.45
449.851	1670.86
450.208	1731.88
450.564	1659.56
450.921	1514.93
451.278	1526.23
451.634	1634.7
451.991	1503.63
452.347	1512.67
452.704	1474.26
453.06	1343.19
453.416	1340.93
453.773	1246.01
454.129	1340.93
454.485	1277.65
454.841	1191.78
455.198	1130.76
455.554	1085.56
455.91	1169.18
456.266	1103.64
456.622	1074.27
456.978	1076.53
457.334	972.57
457.69	922.86
458.046	934.16
458.402	902.52
458.758	859.58
459.114	884.44
459.469	913.82
459.825	868.62
460.181	836.98
460.536	836.98
460.892	789.53
461.248	723.99
461.603	755.63
461.959	733.03
462.314	762.41
462.67	723.99
463.025	705.91
463.38	778.23
463.736	710.43
464.091	737.55
464.446	687.84
464.802	601.96
465.157	617.78
465.512	669.76
465.867	615.52
466.222	601.96
466.577	665.24
466.932	676.54
467.287	620.04
467.642	631.34
467.997	581.62
468.352	590.66
468.707	581.62
469.062	604.22
469.417	617.78
469.771	563.54
470.126	572.58
470.481	552.25
470.835	640.38
471.19	552.25
471.544	570.32
471.899	561.28
472.253	545.47
472.608	518.35
472.962	586.14
473.317	468.63
473.671	518.35
474.025	502.53
474.38	554.51
474.734	504.79
475.088	486.71
475.442	574.84
475.796	570.32
476.15	493.49
476.504	556.77
476.858	574.84
477.212	518.35
477.566	511.57
477.92	468.63
478.274	488.97
478.628	441.51
478.982	516.09
479.336	581.62
479.689	477.67
480.043	452.81
480.397	502.53
480.75	486.71
481.104	491.23
481.457	468.63
481.811	461.85
482.164	466.37
482.518	414.4
482.871	482.19
483.225	518.35
483.578	446.03
483.931	522.87
484.284	475.41
484.638	484.45
484.991	468.63
485.344	504.79
485.697	457.33
486.05	439.25
486.403	459.59
486.756	425.7
487.109	430.21
487.462	466.37
487.815	405.36
488.168	466.37
488.52	479.93
488.873	441.51
489.226	425.7
489.579	477.67
489.931	455.07
490.284	427.95
490.637	427.95
490.989	473.15
491.342	416.66
491.694	459.59
492.047	439.25
492.399	468.63
492.751	425.7
493.104	382.76
493.456	450.55
493.808	403.1
494.16	378.24
494.513	455.07
494.865	416.66
495.217	434.73
495.569	409.88
495.921	464.11
496.273	412.14
496.625	441.51
496.977	425.7
497.329	416.66
497.68	414.4
498.032	389.54
498.384	423.44
498.736	430.21
499.087	409.88
499.439	400.84
499.791	464.11
500.142	391.8
500.494	425.7
500.845	396.32
501.197	398.58
501.548	391.8
501.9	475.41
502.251	398.58
502.602	432.47
502.954	389.54
503.305	362.42
503.656	387.28
504.007	448.29
504.358	369.2
504.709	380.5
505.061	369.2
505.412	468.63
505.763	405.36
506.113	351.12
506.464	378.24
506.815	427.95
507.166	427.95
507.517	389.54
507.868	387.28
508.218	436.99
508.569	448.29
508.92	450.55
509.27	355.64
509.621	391.8
509.971	459.59
510.322	430.21
510.672	432.47
511.023	391.8
511.373	432.47
511.723	382.76
512.074	375.98
512.424	373.72
512.774	385.02
513.124	436.99
513.475	342.08
513.825	416.66
514.175	405.36
514.525	409.88
514.875	378.24
515.225	446.03
515.575	403.1
515.924	461.85
516.274	412.14
516.624	371.46
516.974	416.66
517.324	416.66
517.673	409.88
518.023	412.14
518.373	427.95
518.722	412.14
519.072	398.58
519.421	382.76
519.771	418.92
520.12	484.45
520.469	457.33
520.819	387.28
521.168	405.36
521.517	366.94
521.867	434.73
522.216	464.11
522.565	346.6
522.914	385.02
523.263	355.64
523.612	423.44
523.961	333.04
524.31	418.92
524.659	378.24
525.008	477.67
525.357	455.07
525.705	382.76
526.054	439.25
526.403	371.46
526.752	461.85
527.1	425.7
527.449	387.28
527.797	486.71
528.146	502.53
528.495	421.18
528.843	427.95
529.191	418.92
529.54	455.07
529.888	427.95
530.236	455.07
530.585	452.81
530.933	335.3
531.281	455.07
531.629	418.92
531.977	441.51
532.325	407.62
532.673	414.4
533.021	432.47
533.369	389.54
533.717	389.54
534.065	432.47
534.413	373.72
534.761	382.76
535.108	339.82
535.456	430.21
535.804	375.98
536.151	344.34
536.499	373.72
536.847	412.14
537.194	439.25
537.542	486.71
537.889	414.4
538.236	427.95
538.584	344.34
538.931	351.12
539.278	418.92
539.626	387.28
539.973	324
540.32	398.58
540.667	405.36
541.014	464.11
541.361	353.38
541.708	314.96
542.055	375.98
542.402	378.24
542.749	396.32
543.096	344.34
543.443	333.04
543.789	364.68
544.136	387.28
544.483	371.46
544.83	326.26
545.176	287.85
545.523	382.76
545.869	425.7
546.216	394.06
546.562	339.82
546.909	296.88
547.255	405.36
547.601	380.5
547.948	380.5
548.294	362.42
548.64	342.08
548.986	314.96
549.332	326.26
549.678	405.36
550.025	371.46
550.371	389.54
550.717	387.28
551.062	333.04
551.408	387.28
551.754	382.76
552.1	346.6
552.446	296.88
552.792	391.8
553.137	378.24
553.483	405.36
553.828	396.32
554.174	373.72
554.52	317.22
554.865	375.98
555.211	369.2
555.556	375.98
555.901	360.16
556.247	414.4
556.592	339.82
556.937	382.76
557.282	409.88
557.628	398.58
557.973	330.78
558.318	339.82
558.663	378.24
559.008	427.95
559.353	342.08
559.698	333.04
560.043	394.06
560.388	380.5
560.732	373.72
561.077	366.94
561.422	405.36
561.766	378.24
562.111	355.64
562.456	371.46
562.8	339.82
563.145	287.85
563.489	348.86
563.834	346.6
564.178	360.16
564.523	360.16
564.867	400.84
565.211	328.52
565.555	400.84
565.9	346.6
566.244	335.3
566.588	351.12
566.932	339.82
567.276	321.74
567.62	382.76
567.964	351.12
568.308	369.2
568.652	330.78
568.996	362.42
569.339	344.34
569.683	389.54
570.027	385.02
570.371	364.68
570.714	326.26
571.058	269.77
571.401	321.74
571.745	387.28
572.088	375.98
572.432	330.78
572.775	348.86
573.118	328.52
573.462	353.38
573.805	380.5
574.148	342.08
574.491	362.42
574.835	321.74
575.178	378.24
575.521	362.42
575.864	378.24
576.207	305.92
576.55	294.63
576.892	348.86
577.235	333.04
577.578	351.12
577.921	355.64
578.264	339.82
578.606	348.86
578.949	319.48
579.292	328.52
579.634	321.74
579.977	312.7
580.319	353.38
580.662	308.18
581.004	312.7
581.346	324
581.689	328.52
582.031	333.04
582.373	299.14
582.715	283.33
583.057	265.25
583.4	283.33
583.742	317.22
584.084	333.04
584.426	339.82
584.768	391.8
585.109	339.82
585.451	362.42
585.793	292.37
586.135	310.44
586.477	348.86
586.818	335.3
587.16	287.85
587.502	373.72
587.843	274.29
588.185	353.38
588.526	382.76
588.868	335.3
589.209	299.14
589.55	258.47
589.892	346.6
590.233	366.94
590.574	333.04
590.915	362.42
591.257	317.22
591.598	324
591.939	362.42
592.28	364.68
592.621	308.18
592.962	333.04
593.303	355.64
593.643	330.78
593.984	303.66
594.325	292.37
594.666	364.68
595.006	324
595.347	337.56
595.688	362.42
596.028	321.74
596.369	387.28
596.709	337.56
597.05	303.66
597.39	305.92
597.73	251.69
598.071	290.11
598.411	378.24
598.751	265.25
599.091	326.26
599.431	324
599.772	312.7
600.112	308.18
600.452	333.04
600.792	305.92
601.132	330.78
601.471	296.88
601.811	348.86
602.151	335.3
602.491	290.11
602.831	328.52
603.17	303.66
603.51	326.26
603.849	344.34
604.189	391.8
604.528	321.74
604.868	294.63
605.207	312.7
605.547	357.9
605.886	292.37
606.225	353.38
606.565	378.24
606.904	296.88
607.243	305.92
607.582	360.16
607.921	324
608.26	303.66
608.599	380.5
608.938	337.56
609.277	281.07
609.616	328.52
609.955	285.59
610.293	330.78
610.632	326.26
610.971	351.12
611.309	364.68
611.648	294.63
611.987	253.95
612.325	278.81
612.664	342.08
613.002	242.65
613.34	314.96
613.679	346.6
614.017	373.72
614.355	242.65
614.694	355.64
615.032	301.4
615.37	231.35
615.708	324
616.046	292.37
616.384	364.68
616.722	265.25
617.06	348.86
617.398	242.65
617.736	335.3
618.073	292.37
618.411	335.3
618.749	267.51
619.086	326.26
619.424	321.74
619.762	328.52
620.099	296.88
620.437	337.56
620.774	269.77
621.112	231.35
621.449	326.26
621.786	324
622.123	339.82
622.461	362.42
622.798	278.81
623.135	333.04
623.472	348.86
623.809	324
624.146	265.25
624.483	285.59
624.82	233.61
625.157	229.09
625.494	265.25
625.83	285.59
626.167	360.16
626.504	314.96
626.841	276.55
627.177	294.63
627.514	256.21
627.85	253.95
628.187	283.33
628.523	337.56
628.86	267.51
629.196	296.88
629.532	260.73
629.869	247.17
630.205	344.34
630.541	342.08
630.877	355.64
631.213	319.48
631.549	285.59
631.885	326.26
632.221	375.98
632.557	333.04
632.893	305.92
633.229	278.81
633.564	328.52
633.9	308.18
634.236	260.73
634.572	231.35
634.907	310.44
635.243	290.11
635.578	337.56
635.914	317.22
636.249	324
636.585	362.42
636.92	269.77
637.255	294.63
637.59	333.04
637.926	208.75
638.261	296.88
638.596	258.47
638.931	330.78
639.266	278.81
639.601	324
639.936	333.04
640.271	339.82
640.606	308.18
640.94	253.95
641.275	290.11
641.61	303.66
641.945	342.08
642.279	283.33
642.614	321.74
642.948	339.82
643.283	283.33
643.617	339.82
643.952	342.08
644.286	317.22
644.62	305.92
644.955	303.66
645.289	308.18
645.623	296.88
645.957	382.76
646.291	301.4
646.625	348.86
646.959	224.57
647.293	262.99
647.627	342.08
647.961	292.37
648.295	238.13
648.629	274.29
648.962	283.33
649.296	337.56
649.63	269.77
649.963	326.26
650.297	308.18
650.63	290.11
650.964	294.63
651.297	296.88
651.631	249.43
651.964	339.82
652.297	276.55
652.631	326.26
652.964	373.72
653.297	267.51
653.63	272.03
653.963	314.96
654.296	265.25
654.629	319.48
654.962	240.39
655.295	317.22
655.628	272.03
655.961	260.73
656.293	328.52
656.626	292.37
656.959	308.18
657.291	319.48
657.624	326.26
657.956	305.92
658.289	305.92
658.621	285.59
658.954	328.52
659.286	296.88
659.618	247.17
659.95	242.65
660.283	220.05
660.615	269.77
660.947	278.81
661.279	290.11
661.611	287.85
661.943	348.86
662.275	296.88
662.607	278.81
662.939	265.25
663.27	208.75
663.602	287.85
663.934	256.21
664.266	235.87
664.597	292.37
664.929	324
665.26	269.77
665.592	301.4
665.923	265.25
666.255	317.22
666.586	305.92
666.917	292.37
667.248	324
667.58	231.35
667.911	281.07
668.242	240.39
668.573	256.21
668.904	290.11
669.235	319.48
669.566	312.7
669.897	274.29
670.228	317.22
670.558	294.63
670.889	312.7
671.22	317.22
671.551	244.91
671.881	278.81
672.212	303.66
672.542	326.26
672.873	287.85
673.203	328.52
673.533	301.4
673.864	265.25
674.194	287.85
674.524	249.43
674.855	330.78
675.185	283.33
675.515	296.88
675.845	292.37
676.175	292.37
676.505	265.25
676.835	256.21
677.165	303.66
677.494	265.25
677.824	278.81
678.154	362.42
678.484	299.14
678.813	247.17
679.143	328.52
679.472	272.03
679.802	344.34
680.131	283.33
680.461	283.33
680.79	321.74
681.119	339.82
681.449	285.59
681.778	281.07
682.107	287.85
682.436	301.4
682.765	328.52
683.094	274.29
683.423	269.77
683.752	308.18
684.081	269.77
684.41	267.51
684.739	303.66
685.067	274.29
685.396	308.18
685.725	301.4
686.053	278.81
686.382	238.13
686.711	251.69
687.039	301.4
687.367	281.07
687.696	290.11
688.024	324
688.352	269.77
688.681	285.59
689.009	296.88
689.337	301.4
689.665	240.39
689.993	253.95
690.321	362.42
690.649	312.7
690.977	312.7
691.305	251.69
691.633	287.85
691.961	299.14
692.288	314.96
692.616	183.89
692.944	317.22
693.271	249.43
693.599	294.63
693.926	292.37
694.254	292.37
694.581	231.35
694.908	272.03
695.236	324
695.563	292.37
695.89	278.81
696.217	310.44
696.544	242.65
696.871	269.77
697.198	281.07
697.525	290.11
697.852	292.37
698.179	312.7
698.506	238.13
698.833	265.25
699.16	269.77
699.486	251.69
699.813	258.47
700.139	224.57
700.466	321.74
700.793	287.85
701.119	308.18
701.445	265.25
701.772	292.37
702.098	324
702.424	310.44
702.75	303.66
703.077	272.03
703.403	278.81
703.729	294.63
704.055	276.55
704.381	312.7
704.707	251.69
705.033	287.85
705.358	326.26
705.684	294.63
706.01	272.03
706.336	342.08
706.661	330.78
706.987	328.52
707.312	274.29
707.638	290.11
707.963	217.79
708.289	251.69
708.614	231.35
708.939	251.69
709.265	269.77
709.59	321.74
709.915	256.21
710.24	294.63
710.565	292.37
710.89	276.55
711.215	362.42
711.54	274.29
711.865	240.39
712.19	299.14
712.515	208.75
712.839	312.7
713.164	355.64
713.489	274.29
713.813	305.92
714.138	292.37
714.462	324
714.787	292.37
715.111	292.37
715.435	274.29
715.76	269.77
716.084	294.63
716.408	290.11
716.732	283.33
717.056	287.85
717.381	324
717.705	355.64
718.028	294.63
718.352	294.63
718.676	314.96
719	272.03
719.324	296.88
719.648	317.22
719.971	285.59
720.295	299.14
720.618	267.51
720.942	324
721.265	278.81
721.589	285.59
721.912	262.99
722.236	308.18
722.559	290.11
722.882	265.25
723.205	317.22
723.528	260.73
723.851	287.85
724.174	281.07
724.497	283.33
724.82	244.91
725.143	235.87
725.466	235.87
725.789	308.18
726.112	229.09
726.434	292.37
726.757	283.33
727.08	267.51
727.402	296.88
727.725	274.29
728.047	324
728.37	215.53
728.692	324
729.014	294.63
729.336	258.47
729.659	258.47
729.981	346.6
730.303	296.88
730.625	301.4
730.947	296.88
731.269	285.59
731.591	265.25
731.913	321.74
732.235	285.59
732.556	314.96
732.878	242.65
733.2	262.99
733.521	208.75
733.843	242.65
734.164	326.26
734.486	256.21
734.807	301.4
735.129	260.73
735.45	283.33
735.771	308.18
736.092	301.4
736.414	217.79
736.735	272.03
737.056	217.79
737.377	262.99
737.698	262.99
738.019	267.51
738.34	244.91
738.66	247.17
738.981	215.53
739.302	253.95
739.623	272.03
739.943	272.03
740.264	305.92
740.584	328.52
740.905	262.99
741.225	330.78
741.546	283.33
741.866	258.47
742.186	287.85
742.507	208.75
742.827	281.07
743.147	249.43
743.467	276.55
743.787	244.91
744.107	292.37
744.427	215.53
744.747	299.14
745.067	276.55
745.386	269.77
745.706	265.25
746.026	328.52
746.346	260.73
746.665	229.09
746.985	308.18
747.304	272.03
747.624	269.77
747.943	242.65
748.262	220.05
748.582	269.77
748.901	308.18
749.22	299.14
749.539	274.29
749.858	292.37
750.177	281.07
750.496	324
750.815	296.88
751.134	285.59
751.453	281.07
751.772	240.39
752.09	233.61
752.409	238.13
752.728	299.14
753.046	301.4
753.365	231.35
753.683	240.39
754.002	283.33
754.32	326.26
754.639	269.77
754.957	337.56
755.275	344.34
755.593	276.55
755.911	260.73
756.23	317.22
756.548	326.26
756.866	290.11
757.183	249.43
757.501	224.57
757.819	235.87
758.137	256.21
758.455	319.48
758.772	299.14
759.09	328.52
759.408	333.04
759.725	292.37
760.043	222.31
760.36	244.91
760.677	296.88
760.995	283.33
761.312	328.52
761.629	226.83
761.946	303.66
762.264	258.47
762.581	296.88
762.898	305.92
763.215	308.18
763.532	226.83
763.848	269.77
764.165	328.52
764.482	283.33
764.799	247.17
765.115	281.07
765.432	235.87
765.749	229.09
766.065	319.48
766.382	312.7
766.698	301.4
767.014	265.25
767.331	278.81
767.647	321.74
767.963	249.43
768.279	233.61
768.595	335.3
768.911	269.77
769.227	299.14
769.543	301.4
769.859	249.43
770.175	251.69
770.491	296.88
770.807	253.95
771.122	299.14
771.438	276.55
771.754	314.96
772.069	301.4
772.385	283.33
772.7	314.96
773.016	308.18
773.331	303.66
773.646	319.48
773.961	339.82
774.277	287.85
774.592	314.96
774.907	244.91
775.222	278.81
775.537	308.18
775.852	278.81
776.167	244.91
776.482	296.88
776.796	272.03
777.111	310.44
777.426	321.74
777.74	310.44
778.055	267.51
778.369	287.85
778.684	281.07
778.998	278.81
779.313	292.37
779.627	366.94
779.941	342.08
780.255	220.05
780.57	256.21
780.884	253.95
781.198	269.77
781.512	251.69
781.826	294.63
782.14	276.55
782.453	267.51
782.767	310.44
783.081	238.13
783.395	242.65
783.708	249.43
784.022	299.14
784.335	283.33
784.649	235.87
784.962	305.92
785.276	281.07
785.589	251.69
785.902	292.37
786.216	249.43
786.529	233.61
786.842	256.21
787.155	281.07
787.468	262.99
787.781	319.48
788.094	296.88
788.407	247.17
788.72	276.55
789.032	247.17
789.345	296.88
789.658	342.08
789.97	324
790.283	296.88
790.595	240.39
790.908	247.17
791.22	276.55
791.532	335.3
791.845	256.21
792.157	299.14
792.469	281.07
792.781	265.25
793.093	290.11
793.405	274.29
793.717	312.7
794.029	258.47
794.341	287.85
794.653	260.73
794.965	269.77
795.276	276.55
795.588	290.11
795.9	249.43
796.211	301.4
796.523	251.69
796.834	253.95
797.146	240.39
797.457	283.33
797.768	251.69
798.08	272.03
798.391	357.9
798.702	276.55
799.013	305.92
799.324	258.47
799.635	296.88
799.946	258.47
800.257	274.29
800.568	278.81
800.878	290.11
801.189	290.11
801.5	294.63
801.81	342.08
802.121	247.17
802.431	249.43
802.742	317.22
803.052	276.55
803.363	251.69
803.673	310.44
803.983	247.17
804.293	211.01
804.603	251.69
804.913	324
805.224	256.21
805.533	272.03
805.843	265.25
806.153	283.33
806.463	292.37
806.773	333.04
807.083	251.69
807.392	247.17
807.702	269.77
808.011	321.74
808.321	278.81
808.63	319.48
808.94	274.29
809.249	262.99
809.558	324
809.868	294.63
810.177	267.51
810.486	312.7
810.795	233.61
811.104	269.77
811.413	249.43
811.722	294.63
812.031	308.18
812.339	220.05
812.648	269.77
812.957	294.63
813.266	287.85
813.574	163.56
813.883	285.59
814.191	249.43
814.5	287.85
814.808	292.37
815.116	314.96
815.424	310.44
815.733	251.69
816.041	262.99
816.349	260.73
816.657	244.91
816.965	265.25
817.273	262.99
817.581	290.11
817.889	272.03
818.196	294.63
818.504	301.4
818.812	301.4
819.12	233.61
819.427	251.69
819.735	283.33
820.042	305.92
820.349	222.31
820.657	258.47
820.964	305.92
821.271	253.95
821.579	253.95
821.886	326.26
822.193	290.11
822.5	226.83
822.807	337.56
823.114	274.29
823.421	285.59
823.727	290.11
824.034	220.05
824.341	229.09
824.648	272.03
824.954	244.91
825.261	292.37
825.567	294.63
825.874	294.63
826.18	240.39
826.486	355.64
826.793	281.07
827.099	342.08
827.405	360.16
827.711	324
828.017	321.74
828.323	328.52
828.629	296.88
828.935	258.47
829.241	276.55
829.547	226.83
829.852	217.79
830.158	206.49
830.464	183.89
830.769	262.99
831.075	321.74
831.38	265.25
831.686	326.26
831.991	312.7
832.296	233.61
832.601	301.4
832.907	281.07
833.212	294.63
833.517	267.51
833.822	321.74
834.127	269.77
834.432	294.63
834.737	244.91
835.041	301.4
835.346	272.03
835.651	301.4
835.956	285.59
836.26	224.57
836.565	251.69
836.869	296.88
837.174	267.51
837.478	296.88
837.782	265.25
838.087	281.07
838.391	287.85
838.695	226.83
838.999	262.99
839.303	269.77
839.607	201.97
839.911	290.11
840.215	278.81
840.519	310.44
840.822	314.96
841.126	326.26
841.43	278.81
841.733	299.14
842.037	274.29
842.34	274.29
842.644	294.63
842.947	321.74
843.251	262.99
843.554	319.48
843.857	272.03
844.16	208.75
844.463	267.51
844.766	319.48
845.069	287.85
845.372	269.77
845.675	283.33
845.978	281.07
846.281	285.59
846.583	222.31
846.886	224.57
847.189	337.56
847.491	247.17
847.794	314.96
848.096	267.51
848.399	251.69
848.701	258.47
849.003	292.37
849.305	244.91
849.608	249.43
849.91	258.47
850.212	326.26
850.514	292.37
850.816	360.16
851.118	287.85
851.419	371.46
851.721	296.88
852.023	287.85
852.325	278.81
852.626	324
852.928	287.85
853.229	310.44
853.531	262.99
853.832	229.09
854.133	211.01
854.435	283.33
854.736	281.07
855.037	274.29
855.338	274.29
855.639	283.33
855.94	274.29
856.241	333.04
856.542	292.37
856.843	310.44
857.144	242.65
857.444	294.63
857.745	283.33
858.046	296.88
858.346	258.47
858.647	305.92
858.947	265.25
859.247	260.73
859.548	285.59
859.848	305.92
860.148	240.39
860.448	247.17
860.749	265.25
861.049	310.44
861.349	265.25
861.648	265.25
861.948	251.69
862.248	265.25
862.548	283.33
862.848	269.77
863.147	247.17
863.447	287.85
863.746	260.73
864.046	265.25
864.345	215.53
864.645	247.17
864.944	324
865.243	301.4
865.542	305.92
865.842	310.44
866.141	256.21
866.44	249.43
866.739	274.29
867.038	276.55
867.337	355.64
867.635	326.26
867.934	265.25
868.233	276.55
868.531	294.63
868.83	333.04
869.129	247.17
869.427	274.29
869.725	260.73
870.024	224.57
870.322	235.87
870.62	251.69
870.918	305.92
871.217	312.7
871.515	272.03
871.813	211.01
872.111	231.35
872.409	267.51
872.706	310.44
873.004	326.26
873.302	267.51
873.6	278.81
873.897	253.95
874.195	256.21
874.492	299.14
874.79	276.55
875.087	285.59
875.384	267.51
875.682	265.25
875.979	267.51
876.276	308.18
876.573	258.47
876.87	258.47
877.167	258.47
