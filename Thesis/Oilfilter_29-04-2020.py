#Importing packages
import matplotlib.pyplot as plt
import numpy as np
import os
import math
import pandas as pd

# Histogram function myhistogram
#This function makes a histogram of input data and plots and saves it
#inputs:
#       data: the array to be histogrammed
#       wavelength: at which wavelength should the data be looked at
#       binsize: fixed at 10, but can be changed
#       name is for the name in the file the plots will be saved at
#output:
#       histx, histy: the x and y values of the histogramm of the input data
def myhistogram(data, wavelength, binsize=10, name=''):
    print(data)
    ix_of_wavelength = np.argmax(wavelength < data[0][0])
    peakval = []
    for x, y in data:
        peakval.append(y[ix_of_wavelength])
    histy, histx = np.histogram(peakval, bins=np.arange(np.min(peakval), np.max(peakval), binsize))
    fig_peakval, ax = plt.subplots()
    ax.plot(peakval, 'bo', markersize=0.8)
    ax.set_ylabel('Counts')
    ax.set_xlabel('Intensity at max. Abs/PL')
    fig_peakval.tight_layout()
    fig_peakval.savefig(directory[:-1] + '_Intensity distribution_' + name)
    fig_hist, bx = plt.subplots()
    bx.plot(histx[1:], histy)
    bx.set_ylabel('Counts')
    bx.set_xlabel('Intensity at max. Abs/PL')
    fig_hist.tight_layout()
    fig_hist.savefig(directory[:-1] + '_Histogram_' + name)
    return histx, histy

# Variables for the three dfferent data types
header_PL = 17
header_Abs = 17
header_ref = 17
# Variables for PL
p_pl = 0.1
max_pl = 600
cutoff_pl = 500
binsize_pl = 10
# Variables for Abs
Abs_max = 489
binsize_abs = 10
p_abs = 0.05

# Declaring the folder to look at for data import
base = '/Users/andres/Documents/PycharmProjects/Thesis/Data/2020-12-15/data2/4/'

# Reading in the data from the text files
# Firstly declaring three empty arrays for PL data, absorption data, reference data
dataPL = []
dataAbs = []
ref = []
# Importing the data via a Pandas Dataframe and then converting it into a numpy array
for directory, dirs, files in os.walk(base):
    #directory = os.path.join(base, folder)
    for file in files:
        if file.endswith('.txt'):
            filepath = os.path.join(directory, file)
            if file.startswith('P'):
                header = header_PL
                #These two lines are reading in the data with pandas and then converting it into numpy arrays, because it is faster
                #The lines below woul dod the same for a numpy array, which is sloweer but works jsut as well
                spectrum_df = pd.read_csv(filepath, dtype=np.float64, header=header_PL, delimiter='\t').T
                spectrum = spectrum_df.to_numpy()
                dataPL.append(spectrum)
            #spectrum = np.genfromtxt(directory + file, skip_header=header, skip_footer=1, unpack=True)
            elif file.startswith('water'):
                header = header_ref
                spectrum_df = pd.read_csv(filepath, delimiter='\t', dtype=np.float64).T
                spectrum = spectrum_df.to_numpy()
                ref.append(spectrum)
            elif file.startswith('fitC'):
                header = header_Abs
                spectrum_df = pd.read_csv(filepath, delimiter='\t', dtype=np.float64).T
                spectrum = spectrum_df.to_numpy()
                dataAbs.append(spectrum)

# Analysing the data
# find the index of where the excitation line x-value is
# Cutoff the data at this index and see where the maximum in the y-values is
# define the thresholds a bit below the lower one to include some scattering and a bit more below the second one to surely not include those
histx, histy = myhistogram(dataPL, max_pl, binsize_pl, 'PL') #using the histogram function defined above for PL data
ix_of_cutoff = np.argmax(histx > cutoff_pl) #defining an index for cutting of the data, since there is usually an excitation peak we want to cut
peakindex = np.argmax(histy[ix_of_cutoff:]) #declaring the peakindex, index of the peak
threshold_low = histx[peakindex] * (1 - p_pl) #declaring the thresholds, while only the higher one will be used for PL
threshold_high = histx[peakindex] * (1 + p_pl)

#Separating the oil and the droplet values
dropletspectra_PL = [] #empty array, that will later be filled with the data points that are left over after filtering
for x, y in dataPL:
   if y[(np.argmax(max_pl < dataPL[0][0]))] > threshold_high: #separating values below the threshold out and only appending those above the threshold
       dropletspectra_PL.append(y)
#Averaging the data points in the above made array dropletspectra_PL
y = np.mean(dropletspectra_PL, axis=0)
x_new = x[np.argmax(x > cutoff_pl):] #removing the data points below the cutoff that was defined above
y_new = y[np.argmax(x > cutoff_pl):]
y_new2 = []
for element in y_new:
  y_new2.append(math.log10(element)) #plotting the logarithm
#Saving the file
with open(directory[:-1] + 'PLspectrum.dat','w') as file2:
   for x_el, y_el in zip(x_new,y_new2):
       file2.write('{}\t{}\n'.format(x_el,y_el))
#Plotting the file
fig, dx = plt.subplots()
dx.set_ylabel('Photoluminescence (a.u.)')
dx.plot(x_new, y_new2, color = 'red')
dx.set_xlabel('Wavelength (nm)')
fig.tight_layout()
fig.savefig(directory[:-1] + 'PL_Spectrum')
# Absorption data
# print(dataAbs)
# index_of_max_absorption = np.argmax(dataAbs[0][0] > Abs_max)
# count = 0
# for datensatz in (dataAbs, ref): #doing the same for the refernence data and the absorption data, thus importing data, filtering and averaging
#     if count == 0:
#         name = 'Abs'
#     elif count == 1:
#         name = 'ref'
#     histx, histy = myhistogram(datensatz, Abs_max, binsize_abs, name)
#     #Find the index of where the middle x-value is
#     ix_middle = np.argmax(histx > np.mean(histx))
#     #Apply that index to the y-value and that way split the data and see where the two maximas in the two halfs are for the y-values
#     iy_peak = np.argmax(histy[:ix_middle])
#     #define the thresholds a bit below the lower one to include some scattering and a bit more below the second one to surely not include those
#     threshold_low = histx[iy_peak] * (1 - p_abs)
#     threshold_high = histx[iy_peak] * (1 + p_abs)
#     dropletspectra = []
#     for x,y in datensatz:
#         if y[index_of_max_absorption] < threshold_high and y[index_of_max_absorption] > threshold_low:
#             dropletspectra.append((x, y))
#     if datensatz is dataAbs:
#         x_data, y_data = np.mean(dropletspectra, axis=0)
#     elif datensatz is ref:
#         x_ref, y_ref = np.mean(dropletspectra, axis=0)
#     count += 1
# # Taking reference and plotting data
# y_new = y_ref/y_data
# y_new2 = []
# for element in y_new:
#     y_new2.append(math.log10(element))
# # Saving the file
# with open('Absspectrum.dat','w') as file3:
#     for x_el, y_el in zip(x,y):
#         file3.write('{}\t{}\n'.format(x_el,y_el))
# #Part of the plotting
# figi, dx = plt.subplots()
# dx.set_ylabel('Absorbance (a.u.)')
# dx.plot(x_data, y_new2, color = 'red')
# dx.set_xlabel('Wavelength (nm)')
# figi.tight_layout()
# figi.savefig('Absorptionspectrum')
# plt.show()