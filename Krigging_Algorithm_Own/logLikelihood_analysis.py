"""
deMello Group

Author: Andres Rocha Tapia
Date: 2603.2020

In this script, we analyze the behavior of the logarithmic function of the krige model in order to test whether a
minimum is findable by a minimisation algorithm.

The analysis was simply done via plotting the surveyed variable space and checking whether there is a minimum,
and then subsequently running an optimizer (other script) and comparing wheter the solutions found are in agreement.

"""

# Standard Packages
import pandas as pd
import numpy as np

# Plotting Packages
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt

# Model Packages
import Krige as kr


# Importing Data
data = pd.read_csv('./Data/_stats.csv')
S = data[['Temperature', 'Base:acid']].to_numpy()
Y = data['Median / nm'].to_numpy().reshape((-1, 1))

#######################################################################################################################

# In this section of the code I am creating a theta vector in order to see how the LogLikelyhood
# behaves as a function of it.
# Since we are dealing with a 2D problem, I must create grid vectors such that for each parameter x, y is varied
# from (a,b) as x is varied from (c, d).
# In this case I simply varied both parameters of theta in the same range using np.linspace
n = 50
theta_xy = np.linspace(0.1, 100, n).reshape((-1, 1))
theta = np.reshape(np.meshgrid(theta_xy, theta_xy), (2, n * n)).T
print(theta.shape)
print(theta[0])

# Creating the aforementioned grid
xx, yy = np.meshgrid(theta_xy, theta_xy)

# Create vector to store likelihood solutions
L = np.zeros([n*n, 1])

# Calculating the - log(likelihood)
for i in range(n*n):
    L[i] = - kr.loglike(kr.GaussModel(S, theta[i]), Y, N=1)

print('\nThe shape of L is: ', L.shape)

#######################################################################################################################

# In this section I take the computed values of likelihood and reshape them into a grid matrix so that they may be
# plotted as a surface

# Creating grid to store the values of likelihood
L_grid = np.zeros((n, n))
print('The shape of L_grid is:', L_grid.shape)

# Assigning values to the grid
k = 0
for i in np.arange(np.size(xx, 0)):
    for j in np.arange(np.size(yy, 0)):
        L_grid[i, j] = L[k]
        k += 1

# Plotting the data
fig = plt.figure(1, figsize=(9, 9))
ax = plt.axes(projection='3d')
im1 = ax.plot_surface(xx, yy, L_grid, rstride=1, cstride=1, cmap='viridis', edgecolor='none')

# Formatting
ax.view_init(azim=25, elev=1)
fig.colorbar(im1, shrink=0.45)
plt.xlabel(r'$\theta$ along dimension x')
plt.ylabel(r'$\theta$ along dimension y')
plt.title('z = -LogLikelihood(\u03B8)')

# Plotting data as heat map
fig2 = plt.figure(2, figsize=(6, 6))
plt.contourf(xx, yy, L_grid, 15)

# Formatting
plt.xlabel(r'$\theta$ along dimension x')
plt.ylabel(r'$\theta$ along dimension y')
plt.title('z = -LogLikelihood(\u03B8)')
plt.colorbar(cmap='viridis')

# Visualisation and Saving of graphs
plt.show()
fig.savefig('./Graphs/' + 'Evolution_LogLikelihood_3d')
fig2.savefig('./Graphs/' + 'Evolution_LogLikelihood_contour')
