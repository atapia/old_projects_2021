"""
deMello Group

Author: Andres Rocha Tapia
Date: 26.03.2020

In this script, we use the Krige model to interpolate a data space and derive a reaction surface.
The employed data if from Perovskite research provided by Shangkun/Phil

"""

from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
from datetime import datetime
import numpy as np
import Krige as kr
from matplotlib import cm
import matplotlib.pyplot as plt

# Here we read the data using a pandas data frame
data = pd.read_csv('./Data/_stats.csv')
S = data[['Temperature', 'Base:acid']]
S = S.to_numpy()
column = 'Median / nm'
Y = data[column].to_numpy()
Y = np.reshape(Y, [len(Y), 1])

# We create the linear space to predict
# Mind that if we use a vale of n, we effectively are probing a surface with n^2 points
# Think of speed of calculation, n=100 can take a few minutes!!!!
# Also kep in mid krigging can only effectively predict within (or slightly outside) of the probed surface
n = 50
T_pred = np.linspace(90, 190, n).reshape((-1, 1))
X_pred = np.linspace(.7, 1.2, n).reshape((-1, 1))
xx, yy = np.meshgrid(T_pred, X_pred)

# Turning the matrix grid into two column vectors v_x, v_y in order for it to be iterable inside the krigging function
v_x, v_y = [], []
for i in np.arange(len(T_pred)):
    v_x = np.concatenate([v_x, xx[i, :]])
    v_y = np.concatenate([v_y, yy[i, :]])

v_x = np.reshape(v_x, [len(v_x), 1])
v_y = np.reshape(v_y, [len(v_y), 1])

# Packing sample vectors into a single k-column matrix
S_pred = np.concatenate([v_x, v_y], 1)
print('S_pred shape:', S_pred.shape)

# Running the krigging model
y_pred, y_ss = kr.blup(S, Y, S_pred)

#######################################################################################################################

# In this section I take the computed values and reshape them into a grid matrix so that they may be
# plotted as a surface

# Creating grid to store the values of likelihood
L_grid = np.zeros([np.size(xx, 0), np.size(yy, 0)])
print('\nL_grid shape: ', L_grid.shape)

# Assigning values ot grid
k = 0
for i in np.arange(np.size(xx, 0)):
    for j in np.arange(np.size(yy, 0)):
        L_grid[i, j] = y_pred[k]
        k += 1

# Plotting krigged surface
fig = plt.figure(2)
ax2 = plt.axes(projection='3d')
ax2.plot_surface(xx, yy, L_grid, rstride=1, cstride=1, cmap='viridis',
                edgecolor='none')
ax2.view_init(azim=-135, elev=22)
plt.xlabel('Temperature (°C)')
plt.ylabel('Acid Ratio')
ax2.set_zlabel(column)
plt.title('Krigged(X, Y)')

#######################################################################################################################

# In this section I plot the experimental data onto the surface for comparison

# Preparing the data to plot as a scatter in a 3D plot
T = np.arange(90, 200, 10).reshape((-1, 1))
X = np.arange(0.7, 1.25, 0.05).reshape((-1, 1))

# Cretaing a grid for sampled points
mT, mX = np.meshgrid(T, np.flip(X, 0))
print('\nSample grid shape: ', mT.shape)

# Creating a grid to assing the corresponding values of the Y vector
Med = np.zeros([np.size(mT, 0), np.size(mX, 0)])
k = 0
for i in np.arange(np.size(mT, 0)):
    for j in np.arange(np.size(mX, 0)):
        Med[j, i] = Y[k]
        k += 1

# Plotting the actual data
ax2.scatter(mT, mX, Med, color='k')

directory = './Graphs/' + 'krigged_surface_n' + str(n) + '_' + datetime.today().strftime('%Y-%m-%d')
fig.savefig(directory)
print('\nFigure has been saved to: ', directory)
#######################################################################################################################

# In this section I write all the krigged values into a data frame to then save them as a '.csv' file

points = np.concatenate([S_pred[:, 0].reshape((-1, 1)), S_pred[:, 1].reshape((-1, 1)), y_pred.reshape((-1, 1)), y_ss.reshape((-1, 1))], 1)
columns = ['Temperature (°C)', 'Acid Ratio', 'Prediction', 'Estimated Variance']

data_raw = pd.DataFrame(data=points, columns=columns)

directory = str('./Data/' + 'krigged_data_n' + str(n) + '_' + datetime.today().strftime('%Y-%m-%d') + '.csv')
data_raw.to_csv(directory, index=False)

print('\nData has been saved to: ', directory)



