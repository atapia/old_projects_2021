import numpy as np
from ssb_optimize import optimizer as opt
from scipy import optimize


def weightedDist(xi, xj, w, p):
    """
    This function calculates the distance between two vectors using the formula d = w * (xi[k] - xj[k]) ^ p

    :param xi: first input vector (1xk)
    :param xj: second input vector
    :param w: weighting parameter for each k dimension (1xk)
    :param p: determines the power law used (1x1)
    :return: returns the weighted distance of two column k-dimensional vectors
    :rtype: returns a single value (1x1)
    """
    q = (xi - xj) ** p
    return np.sqrt((w * q * q).sum())


def GaussModel(S, theta, X=np.array([]), p=2, predict=False):
    """

    :param S: n-vector of sampled variables in the k-dimmensional space (nxk)
    :param theta: k-vector associated to the correlation of each point as a function of distance (1xk)
    :param p: defines the smoothness of the data, by default 2 (gaussian like distribution)
    :param X: 1xk vector of desired prediction point, by default empty so to use only when predictions are needed
    :param predict: activates whether an X point is to be interpolated (must provide X)
    :return: R = correlation matrix (nxn) whose (i, j) entries are Corre[x(i), x(j)]
            or R, r = correlation matrix (nxn) and correlation correlation vector (nx1)
    """

    # Normalisation of data
    # In the future the vector S, should be normalized in order to make the theta values more comparable
    # to all input parameters.
    S_norm = (S - S.mean()) / S.std()

    # Computing Computation Matrix
    R = np.zeros([len(S), len(S)])

    for i in np.arange(len(S)):
        for j in np.arange(len(S)):
            R[i, j] = np.exp(-weightedDist(np.array(S_norm[i]), np.array(S_norm[j]), theta, p))

    if predict:
        X_norm = (X - S.mean()) / S.std()
        r = np.zeros([len(S), 1])
        for i in np.arange(len(S)):
            r[i] = np.exp(-weightedDist(np.array(S[i]), X_norm, theta, p))
        return R, r
    else:
        return R


def loglike(r, Y, N=1, allparam=Falsem):
    """
    Ca

    :param allparam: determines whether the function returns the full calculated parameters (mu, ss and llh) or only the Likelihood
    :param r: nxn correlation matrix obtained from the GaussModel function
    :param Y: n-vector of values of sampled points
    :return: LogLikelyhood (llh), estimated mean (mu) and estimated variance (ss)
    """

    n = len(Y)
    ones = np.ones([n, 1])

    r_inv = np.linalg.inv(r)

    if N:
        Y = (Y - Y.mean()) / Y.std()

    mu = tripleP(ones.T, r_inv, Y) / tripleP(ones.T, r_inv, ones)

    y_sub = np.subtract(Y, ones * mu)
    ss = tripleP(y_sub.T, r_inv, ones) / n

    llh = -0.5 * n * np.log(ss) - 0.5 * np.log(np.linalg.det(r))

    if allparam:
        return llh, mu, ss
    else:
        return llh


def optimizer(S, Y):
    """
    This function will get for us the optimal values of theta for a given sampled points.
    This is done by performing a maximisation of the likelyhood estimator as a function of theta.

    :param S: n Sampled points vector in the k-dimensional space (nxk)
    :param Y:
    :return:
    """
    # Initial Values and Bounds
    k = np.size(S, 1)  # This get the dimension we are working on

    theta0 = [11, ] * k  # list type
    # theta0 = np.ones([k, 1])
    bounds = ((0.01, 20,),) * k

    def fun(theta):
        return -loglike(GaussModel(S, np.array(theta)), Y)

    # return optimize.minimize(fun, theta0, method='Nelder-Mead')
    # options={'disp': True, 'maxiter': 100, 'maxfev': 100}
    # Y                        )
    # return optimize.fminbound(fun, lb, up)
    # return optimize.fmin_l_bfgs_b(fun, np.array(theta0), approx_grad=True, bounds=bounds,
    # maxfun=100, maxiter=20,
    #                            disp=1)

    theta_opt = opt.nelder_mead(theta0, fun)
    return np.reshape(theta_opt, [1, np.size(S, 1)])


def tripleP(a, b, c):
    return np.matmul(a, np.matmul(b, c))


def model(R, r, mu, sig_sq, Y):
    n = np.size(Y, 0)
    ones = np.ones([n, 1])

    R_inv = np.linalg.inv(R)
    y_sub = np.subtract(Y, np.multiply(ones, mu))

    # y = mu + np.matmul(r.T, np.matmul(R_inv, np.subtract(Y, np.multiply(ones, mu))))
    y = mu + tripleP(r.T, R_inv, y_sub)

    y_ss = sig_sq * (1 + tripleP(r.T, R_inv, r) +
                     (1 - tripleP(ones.T, R_inv, r)) ** 2 / tripleP(ones.T, R_inv, ones))

    return y, y_ss


def blup(S, Y, X):
    # Get theta from sampled points
    theta = optimizer(S, Y)
    print('Optimized Value for Theta is: ', theta)

    # Get model constants
    R = GaussModel(S, Y, np.array(theta))
    L, mu, ss = loglike(R, Y, allparam=True)

    # calculate predictions
    y = np.zeros(np.size(X, 0))
    ss_y = np.copy(y)

    n = np.size(Y, 0)
    ones = np.ones([n])

    for i in np.arange(np.size(X, 0)):
        R, r = GaussModel(S, np.array(theta), X=X[i], predict=True)
        y[i], ss_y[i] = model(R, r, mu, ss, Y)

    return y, ss_y
