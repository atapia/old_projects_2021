import numpy as np


def weightedDist(xi, xj, w, p):
    """

    :rtype: returns the weighted distance of two column k-dimensional vectors
    """
    q = (xi - xj) ** p
    return np.sqrt((w * q * q).sum())
