import numpy as np
import Krige as kr
from numpy import random
import matplotlib.pyplot as plt

# Try testing model using generated data
# Generate Data
# Input
S = np.array([0.03, 0.07, 0.17, 0.20, 0.26, 0.30, 0.38, 0.48,
              0.51, 0.6, 0.82, 0.93, 0.98])
S = np.reshape(S, [len(S), 1])


# Output
def fun(x):  # function
    return (x / 3 - 1) ** 2 * np.cos(12 * x)


# Randomisation of data
random.seed(99)
Y = fun(S)
Y = Y + random.randn(len(S), 1) * 0.1

# Generating the prediction points
X = np.linspace(0, 1, 300)
X = X.reshape(len(X), 1)

# Running predictor
y, ss_y = kr.blup(S, Y, X)
s_y = np.sqrt(ss_y)

# Generating values for actual function to compare with prediction
kx = np.linspace(0, 1, 100)
ky = fun(kx)

# plotting data
fig = plt.figure(1)
line, = plt.plot(X, y, color='b', linewidth=0.8)
lin2, = plt.plot(kx, ky, color='r', linewidth=0.8, linestyle='-.')
plt.scatter(S, Y, linewidths=0.5, )
plt.axis([0, 1, -1, 1])
plt.xlabel('X')
plt.ylabel('f(X)')
plt.title('Comparison of Krigging Prediction Model')
plt.legend(['Prediction', 'Real Function', 'Sampled Points'])

fig.savefig('./Graphs/' + '1D_Test')
